export const initialState = {
    // init은 새로고침을 했는지 안했는지에 대한 여부
    init: false,
    language: "ko"
};

export const CHANGE_LANGUAGE = "language/CHANGE_LANGUAGE";

export const changeLanguage = (language) => ({
    type: CHANGE_LANGUAGE,
    payload : {
        language : language
    }
});

export default function LanguageReducer(state = initialState, { type, payload}) {
    switch (type) {
        case CHANGE_LANGUAGE:
            return {
                language: payload.language,
                init: true
            };
        default:
            return state;
    }
}