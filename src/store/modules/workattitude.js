import {createAction, handleActions} from "redux-actions";

import {Map, List, fromJS} from "immutable";
import {pender} from "redux-pender";
import {  message } from 'antd';
import * as api from "lib/api";

const INITIALIZE = "workattitude/INITIALIZE";
const CHANGE_INPUT = "workattitude/CHANGE_INPUT";
const WRITE_ADD = "workattitude/WRITE_ADD";
const WORK_ATTITUDE_LIST = 'workattitude/WORK_ATTITUDE_LIST';
const WORK_ATTITUDE_DATA = 'workattitude/WORK_ATTITUDE_DATA';
const EDIT_WORK_ATTITUDE_DATA = 'workattitude/EDIT_WORK_ATTITUDE_DATA';
const REMOVE_WORK_ATTITUDE_DATA = 'workattitude/REMOVE_WORK_ATTITUDE_DATA';
const WORK_ATTITUDE_ALARM_LIST = 'workattitude/WORK_ATTITUDE_ALARM_LIST';
const UPDATE_READ = "workattitude/UPDATE_READ"; 

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeAdd = createAction(WRITE_ADD, api.writeAdd);
export const getWorkAttitudeList = createAction(WORK_ATTITUDE_LIST, api.getWorkAttitudeList);
export const getWorkAttitudeData = createAction(WORK_ATTITUDE_DATA, api.getWorkAttitudeData);
export const editWorkAttitudeData = createAction(EDIT_WORK_ATTITUDE_DATA, api.editWorkAttitudeData);
export const removeWorkAttitudeData = createAction(REMOVE_WORK_ATTITUDE_DATA, api.removeWorkAttitudeData);
export const getWorkAttitudeAlarmList = createAction(WORK_ATTITUDE_ALARM_LIST, api.getWorkAttitudeAlarmList);
export const updateRead = createAction(UPDATE_READ, api.updateRead);

const initialState = Map({
    // searchFromDate의 기본 값 -> locale.searchFromDate[sessionStorage.getItem("language")]로 변경해야 함
    searchFromDate: '',
    // searchFromDate의 기본 값 -> locale.searchFromDate[sessionStorage.getItem("language")]로 변경해야 함
    searchToDate: '',
    searchUserNo: '',
    searchUserName: '',
    // 전체 -> locale.All[sessionStorage.getItem("language")]로 변경해야 함
    searchWorkAttitude: '전체',
    userNo: "",
    no: 0,
    name: "",
    startDay: "",
    endDay: "",
    title: "",
    titleEn: "",
    gubun: "출장",
    workAttitudeList:"",

    content: "",
    contentEn: '',
    tables: List(),
    badge: '',
    changeView: 'table',
    modal: false,
    editDisabled: true,
    workAttitudeAddModal:false,
    add:"출장"
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const {name, value} = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WORK_ATTITUDE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            const {data: tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: WORK_ATTITUDE_DATA,
        onSuccess: (state, action) => {

            const{ name, title, titleEn, startDay, endDay, content, contentEn, workAttitudeList } = action.payload.data.data;

            return state.set('name', name)
                        .set('title', title)
                        .set('titleEn', titleEn)
                        .set('startDay', startDay)
                        .set('endDay', endDay)
                        .set('content', content)
                        .set('contentEn', contentEn)
                        .set('workAttitudeList', workAttitudeList);
        }
    }),
    ...pender({
        type: EDIT_WORK_ATTITUDE_DATA,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);

            return state.set('changeView', state.workattitude.changeView);
        }
    }),
    ...pender({
        type: REMOVE_WORK_ATTITUDE_DATA,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info("삭제하였습니다.");

            return state.set('changeView', state.workattitude.changeView);
        }
    }),
    ...pender({
        type: WORK_ATTITUDE_ALARM_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            const {data: tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: UPDATE_READ,
        onSuccess: (state, action) => {
            return state;
        }
    }),
    ...pender({
        type: WRITE_ADD,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
           // alert(action.payload.data.data);

            return state;
        }
    })

}, initialState);
