import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';
import {  message } from 'antd';
import * as api from 'lib/api';

const INITIALIZE = 'breaktime/INITIALIZE';
const CHANGE_INPUT = 'breaktime/CHANGE_INPUT';
const WRITE_BREAK_TIME = 'breaktime/WRITE_BREAK_TIME';
const BREAK_TIME_LIST = 'breaktime/BREAK_TIME_LIST';
const GET_BREAK_TIME_DATA = 'breaktime/GET_BREAK_TIME_DATA';
const EDIT_BREAK_TIME_DATA = 'breaktime/EDIT_BREAK_TIME_DATA';
const REMOVE_BREAK_TIME_DATA = 'breaktime/REMOVE_BREAK_TIME_DATA';
const BREAK_TIME_USE_LIST = 'breaktime/BREAK_TIME_USE_LIST';
export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeBreakTime = createAction(WRITE_BREAK_TIME, api.writeBreakTime);
export const getBreakTimeList = createAction(BREAK_TIME_LIST, api.getBreakTimeList);
export const getBreakTimeUseList = createAction(BREAK_TIME_USE_LIST, api.getBreakTimeUseList);
export const getBreakTimeData = createAction(GET_BREAK_TIME_DATA, api.getBreakTimeData);
export const editBreakTimeData = createAction(EDIT_BREAK_TIME_DATA, api.editBreakTimeData);
export const removeBreakTimeData = createAction(REMOVE_BREAK_TIME_DATA, api.removeBreakTimeData);

const initialState = Map({
    start: '00:00',
    end: '00:00',
    use: 'true',
    no: 0,
    description: '',
    descriptionEn: '',
    newStart: '00:00',
    newEnd: '00:00',
    newUse: 'true',
    newDescription: '',
    tables: List(),
    modal: false,
    editDisabled: true,
    changeView: 'table'
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_BREAK_TIME,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            const { start, end, use, description } = action.payload.data;
            return state.set('start', start)
                        .set('end', end)
                        .set('use', use)
                        .set('description', description);
        }
    }),
    ...pender({
        type: BREAK_TIME_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: GET_BREAK_TIME_DATA,
        onSuccess: (state, action) => {
            const { start, end, use, description, descriptionEn } = action.payload.data.data;

            return state.set('start', start)
                        .set('end', end)
                        .set('use', use)
                        .set('description', description)
                        .set('descriptionEn', descriptionEn);
        }
    }),
    ...pender({
        type: EDIT_BREAK_TIME_DATA,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);

            return state.set('changeView', state.breaktime.changeView);
        }
    }),
    ...pender({
        type: REMOVE_BREAK_TIME_DATA,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            return state.set('changeView', state.breaktime.changeView);
        }
    })
}, initialState);