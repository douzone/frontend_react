import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'record/INITIALIZE';
const CHANGE_INPUT = 'record/CHANGE_INPUT';
const RECORD_LIST = 'record/RECORD_LIST';
const SEARCH_RECORD_LIST = 'record/SEARCH_RECORD_LIST';
const GET_RECORD_DATA = 'record/GET_RECORD_DATA';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const getRecordList = createAction(RECORD_LIST, api.getRecordList);
export const getSearchRecordList = createAction(SEARCH_RECORD_LIST, api.getSearchRecordList);
export const getRecordData = createAction(GET_RECORD_DATA, api.getRecordData);

const initialState = Map({
    no: 0,
    day: '',
    actor: '',
    id: '',
    recordType: '전체',
    recordTypeEn: '',
    content: '',
    contentEn: '',
    read: '전체',
    insertUserId: '',
    tables: List(),
    startdate: '',
    enddate: '',
    name: '',
    badge: '',
    select: false,
    changeView: 'table',
    modal: false,
    newDay: '',
    newActor: '',
    newId: '',
    newRecordType: '전체',
    newContent: '',
    newRead: '전체',
    subStartdate: '',
    subEnddate: '',
    subType: '전체',
    subRead: '전체',
    subContent: '',
    subSelect: false
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const {name, value} = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: RECORD_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            const { data : tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: SEARCH_RECORD_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            const {data : tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: GET_RECORD_DATA,
        onSuccess: (state, action) => {
            const {day, actor, id, recordType, recordTypeEn, content, contentEn, read, insertUserId} = action.payload.data.data;

            return state.set('day', day)
                        .set('actor', actor)
                        .set('id', id)
                        .set('recordType', recordType)
                        .set('recordTypeEn', recordTypeEn)
                        .set('content', content)
                        .set('contentEn', contentEn)
                        .set('read', read)
                        .set('insertUserId', insertUserId);
        }
    })
}, initialState);

