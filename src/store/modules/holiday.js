import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';
import {  message } from 'antd';

import * as api from 'lib/api';

const INITIALIZE = 'holiday/INITIALIZE';
const CHANGE_INPUT = 'holiday/CHANGE_INPUT';
const WRITE_HOLI_DAY = 'holiday/WRITE_HOLI_DAY';
const HOLI_DAY_LIST = 'holiday/HOLI_DAY_LIST';
const GET_HOLI_DAY_DATA = 'holiday/GET_HOLI_DAY_DATA';
const EDIT_HOLI_DAY_DATA = 'holiday/EDIT_HOLI_DAY_DATA';
const REMOVE_HOLI_DAY_DATA = 'holiday/REMOVE_HOLI_DAY_DATA';
const GET_PRE_HOLI_DAY_DATA = 'holiday/GET_PRE_HOLI_DAY_DATA';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeHoliDay = createAction(WRITE_HOLI_DAY, api.writeHoliDay);
export const getHoliDayList = createAction(HOLI_DAY_LIST, api.getHoliDayList);
export const getHoliDayData = createAction(GET_HOLI_DAY_DATA, api.getHoliDayData);
export const editHoliDayData = createAction(EDIT_HOLI_DAY_DATA, api.editHoliDayData);
export const removeHoliDayData = createAction(REMOVE_HOLI_DAY_DATA, api.removeHoliDayData);
export const getPreHoliDayData = createAction(GET_PRE_HOLI_DAY_DATA, api.getPreHolidayData);

const initialState = Map({
    day: '',
    use: 'true',
    description: '',
    descriptionEn: '',
    no: 0,
    newDay: '',
    newUse: 'true',
    newDescription: '',
    tables: List(),
    changeView: 'table',
    modal: false,
    editDisabled: true,
    preHoliDay: false
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_HOLI_DAY,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            
            return state.set('changeView', state.holiday.changeView);
        }
    }),
    ...pender({
        type: HOLI_DAY_LIST,
        onSuccess: (state, action) => {
            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: GET_HOLI_DAY_DATA,
        onSuccess: (state, action) => {
            const {day, use, description, descriptionEn} = action.payload.data.data;

            return state.set('day', day)
                        .set('use', use)
                        .set('description', description)
                        .set('descriptionEn', descriptionEn);
        }
    }),
    ...pender({
        type: EDIT_HOLI_DAY_DATA,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info(action.payload.data.data);

            return state.set('changeView', state.holiday.changeView);
        }
    }),
    ...pender({
        type: REMOVE_HOLI_DAY_DATA,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info(action.payload.data.data);

            return state.set('changeView', state.holiday.changeView);
        }
    }),
    ...pender({
        type: GET_PRE_HOLI_DAY_DATA,
        onSuccess: (state, action) => {
            return state.set('preHoliDay', action.payload.data.data);
        }
    })
}, initialState);