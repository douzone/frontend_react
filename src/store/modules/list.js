import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';
import { message } from 'antd';

import * as api from 'lib/api';

const INITIALIZE = 'list/INITIALIZE';
const CHANGE_INPUT = 'list/CHANGE_INPUT';
const TABLE_LIST = 'list/TABLE_LIST';
const FULL_TABLE_LIST = 'list/FULL_TABLE_LIST';
const CLICK = 'list/CLICK';
const CHANGE_VIEW = 'list/CHANGE_VIEW';
const GET_COMMUTE_BETWEEN = 'list/GET_COMMUTE_BETWEEN';
const EDIT_COMMUTE_BETWEEN = 'list/EDIT_COMMUTE_BETWEEN';
const REMOVE_COMMUTE_BETWEEN = 'list/REMOVE_COMMUTE_BETWEEN';
const UPDATE_READ = "list/UPDATE_READ";

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const getTableList = createAction(TABLE_LIST, api.getTableList);
export const getFullTableList = createAction(FULL_TABLE_LIST, api.getFullTableList);
export const click = createAction(CLICK);
export const changeView = createAction(CHANGE_VIEW);
export const getCommuteBetween = createAction(GET_COMMUTE_BETWEEN, api.getCommuteBetween);
export const editCommuteBetween = createAction(EDIT_COMMUTE_BETWEEN, api.editCommuteBetween);
export const removeCommuteBetween = createAction(REMOVE_COMMUTE_BETWEEN, api.removeCommuteBetween);
export const updateRead = createAction(UPDATE_READ, api.updateRead);

const initialState = Map({
    searchFromDate: '',
    searchToDate: '',
    searchUserNo: '',
    searchState: '전체',
    startdate: '',
    enddate: '',
    name: '',
    no: 0,
    fromDate: '',
    toDate: '',
    state: '전체',
    tables: List(),
    lastPage: null,
    changeView: 'table',
    modal: false,
    startNo: '',
    endNo: '',
    starttime: '',
    endtime: '',
    editDisabled: true,
    startCommute: '',
    endCommute: '',
    userNo: 0,
    groupNo: 0,
    select: false,
    userName: '',
    subStartdate: '',
    subEnddate: '',
    subCalendardate: '',
    subState: '',
    subSelect: false
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value} = action.payload;
        //console.log("CHANGE_INPUT" + name + " " + value)
        return state.set(name, value);
    },
    [CLICK]: (state, action) => {
        const value = action.payload;
        return state.set('searchUserNo', value);
    },
    ...pender({
        type: TABLE_LIST,
        onSuccess: (state, action) => {
          //  //console.log(action.payload);
            const { data: tables } = action.payload.data;
            
            const lastPage = action.payload.headers['last-page'];
            return state.set('tables', fromJS(tables))
                        .set('lastpage', parseInt(lastPage, 10));
        }
    }),
    ...pender({
        type: FULL_TABLE_LIST,
        onSuccess: (state, action) => {
          //  //console.log(action.payload);
            const{data: tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: GET_COMMUTE_BETWEEN,
        onSuccess: (state, action) => {
            const{startDate, startTime, endDate, endTime, startCommute, endCommute, userNo, groupNo } = action.payload.data.data;

            return state.set('startdate', startDate)
                        .set('starttime', startTime)
                        .set('enddate', endDate)
                        .set('endtime', endTime)
                        .set('startCommute', startCommute)
                        .set('endCommute', endCommute)
                        .set('groupNo', groupNo);
        }
    }),
    ...pender({
        type: EDIT_COMMUTE_BETWEEN,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info("성공적으로 수정하였습니다.");

            return state.set('changeView', state.list.changeView);
        }
    }),
    ...pender({
        type: REMOVE_COMMUTE_BETWEEN,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info("삭제하였습니다.");

            return state.set('changeView', state.list.changeView);
        }
    }),
    ...pender({
        type: UPDATE_READ,
        onSuccess: (state, action) => {
            return state;
        }
    })
}, initialState);