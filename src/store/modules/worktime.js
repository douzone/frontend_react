import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';
import {  message } from 'antd';
const INITIALIZE = 'worktime/INITIALIZE';
const CHANGE_INPUT = 'worktime/CHANGE_INPUT';
const WRITE_WORK_TIME = 'worktime/WRITE_WORK_TIME';
const WORK_TIME_LIST = 'worktime/WORK_TIME_LIST';
const GET_WORK_TIME_DATA = 'worktime/GET_WORK_TIME_DATA';
const EDIT_WORK_TIME_DATA = 'worktime/EDIT_WORK_TIME_DATA';
const REMOVE_WORK_TIME_DATA = 'worktime/REMOVE_WORK_TIME_DATA';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const writeWorkTime = createAction(WRITE_WORK_TIME, api.writeWorkTime);
export const getWorkTimeList = createAction(WORK_TIME_LIST, api.getWorkTimeList);
export const getWorkTimeData = createAction(GET_WORK_TIME_DATA, api.getWorkTimeData);
export const editWorkTimeData = createAction(EDIT_WORK_TIME_DATA, api.editWorkTimeData);
export const removeWorkTimeData = createAction(REMOVE_WORK_TIME_DATA, api.removeWorkTimeData);

const initialState = Map({
    start: '00:00',
    end: '00:00',
    use: 'true',
    newStart: '00:00',
    newEnd: '00:00',
    newUse: 'true',
    no: '',
    tables: List(),
    modal: false,
    editDisabled: true,
    changeView: 'table'
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: WRITE_WORK_TIME,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            const { start, end, use } = action.payload.data;
            return state.set('start', start)
                        .set('end', end)
                        .set('use', use);
        }
    }),
    ...pender({
        type: WORK_TIME_LIST,
        onSuccess: (state, action) => {
         //   //console.log(action.payload);

            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: GET_WORK_TIME_DATA,
        onSuccess: (state, action) => {
            const { start, end, use } = action.payload.data.data;
            return state.set('start', start)
                        .set('end', end)
                        .set('use', use);
        }
    }),
    ...pender({
        type: EDIT_WORK_TIME_DATA,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            return state.set('changeView', state.worktime.changeView);
        }
    }),
    ...pender({
        type: REMOVE_WORK_TIME_DATA,
        onSuccess: (state, action) => {
            message.config( {top: 100, duration: 2});
            message.info(action.payload.data.data);
            return state.set('changeView', state.worktime.changeView);
        }
    })
}, initialState);