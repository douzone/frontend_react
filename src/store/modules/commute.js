import {createAction, handleActions} from 'redux-actions';

import {Map} from 'immutable';
import {pender} from 'redux-pender';
import {  message } from 'antd';
import * as api from '../../lib/api';


const GO_TO = 'commute/GO_TO';
const GO_OFF = 'commute/GO_OFF';
const STATE_CHANGE = 'commute/STATE_CHANGE';
const CHANGE_INPUT = 'commute/CHANGE_INPUT';
const GET_END_TIME = 'commute/GET_END_TIME';
const GET_GOTO_DATA = 'commute/GET_GOTO_DATA';
const GET_GOOFF_DATA = 'commute/GET_GOOFF_DATA';
const GET_PRE_GOTO_DATA = 'commute/GET_PRE_GOTO_DATA';
const GET_PRE_GOOFF_DATA = 'commute/GET_PRE_GOOFF_DATA';
const PRE_GO_TO = 'commute/PRE_GO_TO';
const PRE_GO_OFF = 'commute/PRE_GO_OFF';

export const changeInput = createAction(CHANGE_INPUT);
export const goTo = createAction(GO_TO, api.goTo);
export const goOff = createAction(GO_OFF, api.goOff);
export const stateChange = createAction(STATE_CHANGE);
export const getEndTime = createAction(GET_END_TIME, api.getEndTime);
export const getGoToData = createAction(GET_GOTO_DATA, api.goToData);
export const getGoOffData = createAction(GET_GOOFF_DATA, api.goOffData);
export const getPreGoToData = createAction(GET_PRE_GOTO_DATA, api.goToData);
export const getPreGoOffData = createAction(GET_PRE_GOOFF_DATA, api.preGoOffData);
export const getPreGoTo = createAction(PRE_GO_TO, api.goTo);
export const getPreGoOff = createAction(PRE_GO_OFF, api.goOff);

const initialState = Map({
    goto: false,
    gooff: false,
    isGoOff: false,
    endTime: '',
    time: '',
    isNight: false,
    preGoTo: false,
    preGoOff: false,
    cnt:'',
    totalWorkTime:"00:00:00"
});

export default handleActions({
    [STATE_CHANGE]: (state, action) => {
        const {name, value} = action.payload;

        //console.log("STATE_CHANGE : " + name + " " + value);

        return state.set(name, value);
    },
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        //console.log("bbb");
        //console.log(name+"="+value);
        return state.set(name, value);
    },
    ...pender({
        type: GO_TO,
        onSuccess: (state, action) => {
            //console.log("GO_TO : " + action.payload);

            if(action.payload.data.data !== false && action.payload.data.data !== true){
                message.config( {top: 100, duration: 2});
                message.info("이미 출근하셨습니다.");
            }
            //console.log("1");
            return state.set('goto', true);
            //             .set('gooff', false);
        }
    }),
    ...pender({
        type: GO_OFF,
        onSuccess: (state, action) => {
            //console.log("GO_OFF : " + action.payload);

            if(action.payload.data.data !== false && action.payload.data.data !== true) {
                message.config( {top: 100, duration: 2});
                message.info("이미 퇴근하셨습니다.");
            }
            //console.log("2");
            return state.set('gooff', true);
            //             .set('goto', false);
        }
    }),
    ...pender({
        type: PRE_GO_TO,
        onSuccess: (state, action) => {
            return state.set('preGoTo', action.payload.data.data);
        }
    }),
    ...pender({
        type: PRE_GO_OFF,
        onSuccess: (state, action) => {
            return state.set('preGoOff', action.payload.data.data);
        }
    }),
    ...pender({
        type: GET_END_TIME,
        onSuccess: (state, action) => {
            //console.log("5");
            return state.set('endTime', action.payload.data.data);
        }
    }),
    ...pender({
        type: GET_GOTO_DATA,
        onSuccess: (state, action) => {
            //console.log("GET_GOTO_DATA : " + action.payload);
            //console.log("6");

            return state.set('goto', action.payload.data.data);
        }
    }),
    ...pender({
        type: GET_GOOFF_DATA,
        onSuccess: (state, action) => {
            //console.log("GET_GOOFF_DATA : " + action.payload);
            //console.log("7");
            return state.set('gooff', action.payload.data.data);
        }
    }),
    ...pender({
        type: GET_PRE_GOTO_DATA,
        onSuccess: (state, action) => {
            //console.log("aaaaaa");
            //console.log(action.payload);
            //console.log("GET_PRE_GOTO_DATA : " + action.payload);
            //console.log("8");
            return state.set('preGoTo', action.payload.data.data);
        }
    }),
    ...pender({
        type: GET_PRE_GOOFF_DATA,
        onSuccess: (state, action) => {
            //console.log("GET_PRE_GOOFF_DATA : " + action.payload);
            //console.log("9");
            return state.set('preGoOff', action.payload.data.data);
        }
    })
}, initialState);

