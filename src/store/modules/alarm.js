import { createAction, handleActions } from 'redux-actions';

import { Map, List, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';

const INITIALIZE = 'alarm/INITIALIZE';
const ALARM_RECORD_LIST = 'alarm/ALARM_RECORD_LIST';
const CLICK = 'alarm/CLICK';

export const initialize = createAction(INITIALIZE);
export const getRecordTotalAlarm = createAction(ALARM_RECORD_LIST, api.getRecordTotalAlarm);
export const click = createAction(CLICK);

const initialState = Map({
    no: '',
    day: '',
    actor: '',
    recordType: '',
    read: '',
    tables: List(),
    isClick: null
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CLICK]: (state, action) => {
        const value = action.payload;
        return state.set('isClick', value);
    },
    ...pender({
        type: ALARM_RECORD_LIST,
        onSuccess: (state, action) => {
            const { data : tables } = action.payload.data;
            return state.set('tables', fromJS(tables));
        }
    })
}, initialState);