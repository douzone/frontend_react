import { createAction, handleActions } from "redux-actions";
import { Map } from "immutable";
import { pender } from "redux-pender";
import * as api from "../../lib/api";

const INITIALIZE = 'time/INITIALIZE';
const TIME_LIST = "time/TIME_LIST";
const START_TIME = "time/START_TIME";
const TOTAL_WORKTIME = "time/TOTAL_WORKTIME";
const END_TIME = "time/END_TIME";
const CHANGE_INPUT = 'time/CHANGE_INPUT';
const INIT_TIME = "time/INIT_TIME";

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const getTime = createAction(TIME_LIST, api.getTime);
export const getStartTime = createAction(START_TIME, api.getStartTime);
export const getTotalWorkTime = createAction(TOTAL_WORKTIME,api.getTotalTime); 
export const getTodayCommuteEndTime = createAction(END_TIME,api.getTodayCommuteEndTime); 
export const inittime = createAction(INIT_TIME);

const initialState = Map({
  time: "",
  state: "",
  starttime: "",
  start: "",
  end: "",
  totalWorkTime: "",
  time1:'',
  endtime:"",
  goto:false,
  breakTimeList:""
});

export default handleActions(
  {
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
      const { name, value} = action.payload;

      //console.log(action.payload);

      return state.set(name, value);
  },
    ...pender({
      type: END_TIME,
      onSuccess: (state, action) => {
        let setTime = null;
        if (action.payload.data.data.day != null) {
          let time = action.payload.data.data.day.split(" ");
          setTime = time[1].substring(0,8);

        //console.log("endtime 가져옴");
        //console.log(setTime);
        
          return state.set("endtime",setTime)
                  .set("goto",true);
        }
        
      }
    }),
    ...pender({
      type: TIME_LIST,
      onSuccess: (state, action) => {
        return state.set("time", action.payload.data.data.start);
      }
    }),
    ...pender({
      type: START_TIME,
      onSuccess: (state, action) => {

        const { data: starttime } = action.payload.data.data;
        let setTime = null;
        let setTime2 = null;
        //console.log(action.payload);
        let data = action.payload.data.data;
        if (action.payload.data.data.date != null) {
         
          let time = action.payload.data.data.date.split(" ");
          setTime = time[1].substring(0, 8);
        }
        
        //console.log("starttime가져옴");
        //console.log(setTime);
        console.log(data.breakTimeList);
        return state.set("starttime",setTime)
               .set("start",data.start)
               .set("end",data.end)
               .set("state",data.state)
               .set("time1",data.time)
               .set("breakTimeList",data.breakTimeList);
      }
    }),
    ...pender({
      type: TOTAL_WORKTIME,
      onSuccess: (state, action) => {
         //console.log("totaltime가져옴");
         let data = action.payload.data.data;
         //console.log(data.totalWorkTime);   
          let setTime = data.totalWorkTime.substring(0, 5);   
        return state.set("totalWorkTime",setTime);
      }
    }),
    [INIT_TIME]: (state, action) => {
      return state.set("time", "")
              .set("state", "")
              .set("statrtime", "")
              .set("start", "")
              .set("end", "")
              .set("totalWorkTime", "")
              .set("time1", "")
              .set("endtime", "")
    }
  },
  initialState
);
