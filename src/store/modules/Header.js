import { createAction, handleActions } from "redux-actions";
import { Map} from "immutable";

const CHANGE_INPUT = 'Header/CHANGE_INPUT'; 

export const changeInput = createAction(CHANGE_INPUT);

const initialState = Map({ 
    timeset : ''
})

export default handleActions({
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        return state.set(name, value);
    }
}, initialState);