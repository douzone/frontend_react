import { createAction, handleActions } from 'redux-actions';

import { Map, List} from 'immutable';
import { pender } from 'redux-pender';
import { message } from 'antd';

import * as api from '../../lib/api';


const Calendar_LIST = 'list/CALENDAR_LIST';
const CHANGE_INPUT = 'list/CAL_LIST_MODAL_CHANGE_INPUT';
const EDIT_COMMUTE_BETWEEN = 'list/CAL_LIST_MODAL_EDIT_COMMUTE_BETWEEN';
const REMOVE_COMMUTE_BETWEEN = 'list/CAL_LIST_MODAL_REMOVE_COMMUTE_BETWEEN';
const UPDATE_READ = "list/UPDATE_READ";

export const changeInput = createAction(CHANGE_INPUT);
export const getList = createAction(Calendar_LIST, api.getCalendarList);
export const editCommuteBetween = createAction(EDIT_COMMUTE_BETWEEN, api.editCommuteBetween);
export const removeCommuteBetween = createAction(REMOVE_COMMUTE_BETWEEN, api.removeCommuteBetween);
export const updateRead = createAction(UPDATE_READ, api.updateRead);

const initialState = Map({
    tables: List(),
    fromDate: '',
    toDate: '',
    modal: false,
    startNo: '',
    endNo: '',
    startTime: '',
    endTime: '',
    editDisabled: true,
    startCommute: '',
    endCommute: '',
    commuteNo:0,
    search:{},
    select:false,
    userName:""
});

export default handleActions({
    [CHANGE_INPUT]: (state, action) => {

    const { name, value} = action.payload;
        return state.set(name, value);
    },
    ...pender({
        type: Calendar_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload.data);
            return state.set('calList', action.payload.data);
        }
    }),
    ...pender({
        type: UPDATE_READ,
        onSuccess: (state, action) => {
            return state;
        }
    }),
    ...pender({
        type: EDIT_COMMUTE_BETWEEN,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info("성공적으로 수정하였습니다.");

            return;
        }
    }),
    ...pender({
        type: REMOVE_COMMUTE_BETWEEN,
        onSuccess: (state, action) => {
            message.config({top: 100, duration: 2});
            message.info("삭제하였습니다.");

            return;
        }
    })
}, initialState);