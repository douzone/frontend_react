import { createAction, handleActions } from 'redux-actions';

import { Map, List} from 'immutable';
import { pender } from 'redux-pender';

import * as api from '../../lib/api';

const TABLE_LIST ='workattitude/CALENDAR_LIST';
const CHANGE_INPUT = 'workattitude/CAL_LIST_MODAL_CHANGE_INPUT';
export const getList = createAction(TABLE_LIST, api.getWorkAttitudeCalendarList);

export const changeInput = createAction(CHANGE_INPUT);


const initialState = Map({
    tables: List(),
    modal: false,
    editDisabled: true,
    name:"",
    title:"",
    startDay:"",
    endDay:"",
    workAttitudeList:"",
    content:"",
    no:0,
    search:{},
    select:false,
    userName:""
});

export default handleActions({
    [CHANGE_INPUT]: (state, action) => {

        const { name, value} = action.payload;
            return state.set(name, value);
        },
    ...pender({
        type: TABLE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload.data);
            return state.set('calList', action.payload.data);
        }
    })
}, initialState);