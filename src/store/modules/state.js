import { createAction, handleActions } from 'redux-actions';

import { Map, fromJS } from 'immutable';
import { pender } from 'redux-pender';

import * as api from 'lib/api';


const INITIALIZE = 'state/INITIALIZE';
const CHANGE_INPUT = 'state/CHANGE_INPUT';
const COMMUTE_STATE_LIST = 'state/COMMUTE_STATE_LIST';
const WORK_ATTITUDE_STATE_LIST = 'state/WORK_ATTITUDE_STATE_LIST';
const RECORD_STATE_LIST = 'state/RECORD_STATE_LIST';
const COMMUTE_SEARCH_STATE_LIST = 'state/COMMUTE_SEARCH_STATE_LIST';
const WORK_ATTITUDE_SEARCH_STATE_LIST = 'state/WORK_ATTITUDE_SEARCH_STATE_LIST';
const COMMUTE_CALENDAR_STATE_LIST = 'state/COMMUTE_CALENDAR_STATE_LIST';
const WORK_ATTITUDE_CALENDAR_STATE_LIST = 'state/WORK_ATTITUDE_CALENDAR_STATE_LIST';
const RECORD_SEARCH_STATE_LIST = 'state/RECORD_SEARCH_STATE_LIST';

export const initialize = createAction(INITIALIZE);
export const changeInput = createAction(CHANGE_INPUT);
export const getCommuteStateList = createAction(COMMUTE_STATE_LIST, api.getCommuteStateList);
export const getWorkAttitudeStateList = createAction(WORK_ATTITUDE_STATE_LIST, api.getWorkAttitudeStateList);
export const getRecordStateList = createAction(RECORD_STATE_LIST, api.getRecordStateList);
export const getCommuteSearchStateList = createAction(COMMUTE_SEARCH_STATE_LIST, api.getCommuteSearchStateList);
export const getWorkAttitudeSearchStateList = createAction(WORK_ATTITUDE_SEARCH_STATE_LIST, api.getWorkAttitudeSearchStateList);
export const getCommuteCalendarStateList = createAction(COMMUTE_CALENDAR_STATE_LIST, api.getCommuteCalendarStateList);
export const getWorkAttitudeCalendarStateList = createAction(WORK_ATTITUDE_CALENDAR_STATE_LIST, api.getWorkAttitudeCalendarStateList);
export const getRecordSearchStateList = createAction(RECORD_SEARCH_STATE_LIST, api.getRecordSearchStateList);


const initialState = Map({
    tables: Map(),
    calendarTables: Map(),
    changeView: 'table'
});

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    [CHANGE_INPUT]: (state, action) => {
        const { name, value} = action.payload;

        //console.log(action.payload);

        return state.set(name, value);
    },
    ...pender({
        type: COMMUTE_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: WORK_ATTITUDE_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const { data: tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: RECORD_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const { data : tables } = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: COMMUTE_SEARCH_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const {data : tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: WORK_ATTITUDE_SEARCH_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const {data : tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: RECORD_SEARCH_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            const {data: tables} = action.payload.data;

            return state.set('tables', fromJS(tables));
        }
    }),
    ...pender({
        type: COMMUTE_CALENDAR_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);
            //const {data : calendarTables} = action.payload.data;

            return state.set('calendarTables', fromJS(action.payload.data.data));
        }
    }),
    ...pender({
        type: WORK_ATTITUDE_CALENDAR_STATE_LIST,
        onSuccess: (state, action) => {
            //console.log(action.payload);

            //console.log(action.payload.data.data);

            return state.set('calendarTables', fromJS(action.payload.data.data));
        }
    })
}, initialState);