import { createAction, handleActions } from 'redux-actions';
import { Map } from 'immutable';
import { pender } from 'redux-pender';
import * as api from 'lib/api';
import moment from 'moment';


const CHANGE_INPUT = 'dashboard/CHANGE_INPUT';
const GET_DASHBOARD_DATA = 'dashboard/GET_DASHBOARD_DATA';
const INITIALIZE = 'dashboard/INITIALIZE';
let date = moment(new Date()).format('YYYY-MM-DD');

const initialState = Map({
    searchDate:date
});

export const getDashBoardData = createAction(GET_DASHBOARD_DATA, api.getDashBoardData);
export const changeInput = createAction(CHANGE_INPUT);

export default handleActions({
    [INITIALIZE]: (state, action) => initialState,
    
    [CHANGE_INPUT]: (state, action) => {
        const { name, value } = action.payload;
        //console.log(name);
        //console.log(value);
        return state.set(name, value);
    },
    ...pender({
        type: GET_DASHBOARD_DATA,
        onSuccess: (state, action) => {
            //console.log(action.payload.data.data);
           return state.set("totalUserCount",action.payload.data.data.today.totalUserCount)
           .set("gotoWorkCount",action.payload.data.data.today.gotoWorkCount)
           .set("tardyCount",action.payload.data.data.today.tardyCount)
           .set("businessTripCount",action.payload.data.data.today.businessTripCount)
           .set("workOutsideCount",action.payload.data.data.today.workOutsideCount)
           .set("annualLeaveCount",action.payload.data.data.today.annualLeaveCount)
           .set("educationCount",action.payload.data.data.today.educationCount)
           .set("chartList",action.payload.data.data.chartList);
        }
    }),
  
}, initialState);