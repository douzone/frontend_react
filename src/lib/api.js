import axios from 'axios';
import queryString from 'query-string';

//const address = 'http://localhost:8076';
const address = 'http://218.39.221.104:8080/smartchecker';
//const address = 'http://218.39.221.76:8076';

//await axios.get("http://localhost:8076/commute/search")
//await axios.get("http://localhost:8076/commute/list/1")
export const getFullTableList = (page, token) => axios.get(address+"/commute/list/" + page, {headers : {'Authorization' : 'Bearer ' + token}});
//export const getTableList = (search, page) => axios.get('http://jsonplaceholder.typicode.com/posts/1');
export const getTableList = ({searchFromDate, searchToDate, searchUserNo, userName, searchState, keyNo}, page, select, token, locale) => axios.get(address+`/commute/search?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, userName, searchState, page, select, locale, keyNo})}`, {headers : {'Authorization' : 'Bearer ' + token}});

export const getCommuteBetween = ({startNo, endNo}, token) => axios.get(address+`/commute/search/` + startNo + `/` + endNo, {headers : {'Authorization' : 'Bearer ' + token}});
export const editCommuteBetween = ({startNo, endNo, startDate, startTime, endDate, endTime, startCommute, endCommute, userNo, groupNo}, token) => axios.put(address+`/commute/${startNo}/${endNo}`, {startDate, startTime, endDate, endTime, startCommute, endCommute, userNo, groupNo}, {headers : {'Authorization' : 'Bearer ' + token}});
export const removeCommuteBetween = (startNo, endNo, token) => axios.delete(address+`/commute/${startNo}/${endNo}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getCalendarList =(search, token,select)=> axios.get(address+"/commute/calendar/"+search.searchUserNo+"?month="+search.searchFromDate+"&state="+search.searchState+"&select="+select, {headers : {'Authorization' : 'Bearer ' + token}});
export const getSearchUserList = (name, token) => axios.get(address+"/user/"+name, {headers : {'Authorization' : 'Bearer ' + token}});

export const getPage = (token) => axios.get(address+"/commute/page", {headers : {'Authorization' : 'Bearer ' + token}});
export const getSearchPage = ({searchFromDate, searchToDate, searchUserNo, searchState, select, userName, keyNo}, token) => axios.get(address+`/commute/searchpage?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, searchState, select, userName, keyNo})}`, {headers : {'Authorization' : 'Bearer ' + token}});

export const writeWorkTime = ({start, end, use}, token,language) => axios.post(address+"/worktime/new/?locale="+language, {start, end, use}, {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkTimeList = (page, token) => axios.get(address+"/worktime/list/" + page, {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkTimeTotal = (token) => axios.get(address+"/worktime/page", {headers : {'Authorization' : 'Bearer ' + token}});

export const writeBreakTime = ({start, end, description, use}, token,language) => axios.post(address+"/breaktime/new?locale="+language, {start, end, description, use}, {headers : {'Authorization' : 'Bearer ' + token}});
export const getBreakTimeList = (page, token) => axios.get(address+"/breaktime/list/" + page, {headers : {'Authorization' : 'Bearer ' + token}});
export const getBreakTimeTotal = (token) => axios.get(address+"/breaktime/page", {headers : {'Authorization' : 'Bearer ' + token}});

export const goTo = (commute, recordType,token) => axios.post(address+"/work/goto", {commute, recordType}, {headers : {'Authorization' : 'Bearer ' + token}});
export const goOff = (commute, recordType, totalWorkTime,token) => axios.post(address+"/work/gooff", {commute, recordType,totalWorkTime}, {headers : {'Authorization' : 'Bearer ' + token}});
export const getEndTime = (token) => axios.get(address+'/worktime/endtime', {headers: {'Authorization' : 'Bearer ' + token}});
export const goToData = (isToday, token) => axios.get(address+'/work/gotodata/' + isToday, {headers: {'Authorization' : 'Bearer ' + token}});
export const goOffData = (isToday, token) => axios.get(address+'/work/gooffdata/' + isToday, {headers: {'Authorization' : "Bearer " + token}});
export const preGoOffData = (token) => axios.get(address+'/work/pregooffdata', {headers: {'Authorization' : "Bearer " + token}});

export const writeHoliDay = ({day, description, use}, token,language) => axios.post(address+"/holiday/new?locale="+language, {day, description, use}, {headers : {'Authorization' : 'Bearer ' + token}});
export const getHoliDayList = (page, token) => axios.get(address+"/holiday/list/" + page, {headers : {'Authorization' : 'Bearer ' + token}});
export const getHoliDayTotal = (token) => axios.get(address+"/holiday/page", {headers : {'Authorization' : 'Bearer ' + token}});
export const getPreHolidayData = (token) => axios.get(address+"/holiday/use", {headers: {'Authorization' : 'Bearer ' + token}});

export const writeAdd = ({no,name,startDay,endDay,title,state,workAttitudeList,content,userNo,userId}, token,language) => axios.post(address+"/workattitude/new?locale="+language,{no,name,startDay,endDay,title,state,workAttitudeList,content,userNo,userId}, {headers : {'Authorization' : 'Bearer ' + token}});

export const getWorkAttitudeList = ({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo}, page, token, locale) => axios.get(address+`/workattitude/list?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo, page, locale})}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkAttitudeTotal = ({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo}, token) => axios.get(address+`/workattitude/page?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo})}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkAttitudeData = (no, token, locale) => axios.get(address+`/workattitude/search/${no}/${locale}`, {headers: {'Authorization' : 'Bearer ' + token}});
export const editWorkAttitudeData = ({no, name, title, startDay, endDay, workAttitudeList, content}, token, locale) => axios.put(address+`/workattitude/${no}?locale=${locale}`, {name, title, startDay, endDay, workAttitudeList, content}, {headers : {'Authorization' : 'Bearer ' + token}});
export const removeWorkAttitudeData = (no, token) => axios.delete(address+`/workattitude/${no}`, {headers: {'Authorization' : 'Bearer ' + token}});

export const getRecordTotalAlarm = (auth, token) => axios.get(address+"/record/alarm/" + auth, {headers : {'Authorization' : 'Bearer ' + token}});
export const getRecordList = (page, token) => axios.get(address+"/record/list/" + page, {headers : {'Authorization' : 'Bearer ' + token}});
export const getRecordTotal = (token) => axios.get(address+"/record/page", {headers : {'Authorization' : 'Bearer ' + token}});
export const getSearchRecordList = (page, startdate, enddate, name, content, recordType, select, searchUserNo, read, token, language) => axios.get(address+"/record/search/" + page + "?start=" + startdate + "&end=" + enddate + "&name=" + name + "&content=" + content + "&recordType=" + recordType + "&select=" + select + "&searchUserNo=" + searchUserNo + "&read=" + read + "&locale=" + language, {headers : {'Authorization' : 'Bearer ' + token}});
export const getSearchRecordPage = (startdate, enddate, name, content, recordType, select, searchUserNo, read, token, language) => axios.get(address+`/record/searchpage?${queryString.stringify({startdate, enddate, name, content, recordType, select, searchUserNo, read, language})}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getUserListByName = (token) => axios.get(address+'/user/distinct', {headers : {'Authorization' : 'Bearer ' + token}});
export const getRecordData = (no, token) => axios.get(address+'/record/data/' + no, {headers: {'Authorization' : 'Bearer ' + token}});

export const getCommuteStateList = (token) => axios.get(address+'/commute/state', {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkAttitudeStateList = (token) => axios.get(address+'/workattitude/state', {headers : {'Authorization' : 'Bearer ' + token}});
export const getRecordStateList = (token) => axios.get(address+'/record/state', {headers: {'Authorization' : 'Bearer ' + token}});
export const getCommuteCalendarStateList = (searchFromDate, searchUserNo, userName, searchState, select, token) => axios.get(address+`/commute/calendarstate?${queryString.stringify({searchFromDate, searchUserNo, userName, searchState, select})}`, {headers: {"Authorization" : "Bearer " + token}});
export const getWorkAttitudeCalendarStateList = (searchFromDate, searchUserNo, searchUserName, searchWorkAttitude, select, token) => axios.get(address+`/workattitude/calendarstate?${queryString.stringify({searchFromDate, searchUserNo, searchUserName, searchWorkAttitude, select})}`, {headers : {'Authorization' : 'Bearer ' + token}});

export const getCommuteSearchStateList = ({searchFromDate, searchToDate, searchUserNo, searchState, select, userName, keyNo}, token) => axios.get(address+`/commute/searchstate?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, searchState, select, userName, keyNo})}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getWorkAttitudeSearchStateList = ({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo}, token) => axios.get(address+`/workattitude/searchstate?${queryString.stringify({searchFromDate, searchToDate, searchUserNo, searchUserName, searchWorkAttitude, keyNo})}`, {headers : {'Authorization' : 'Bearer ' + token}});
export const getRecordSearchStateList = (startdate, enddate, name, content, recordType, select, searchUserNo, read, token, language) => axios.get(address+`/record/searchstate?${queryString.stringify({startdate, enddate, name, content, recordType, select, searchUserNo, read, language})}`, {headers : {'Authorization' : 'Bearer ' + token}});

export const getWorkTimeData = (no, token) => axios.get(address+'/worktime/search/' + no, {headers: {'Authorization' : 'Bearer ' + token}});
export const editWorkTimeData = (no, start, end, use, token,language) => axios.put(address+'/worktime/edit/' + no+"?locale="+language, {start, end, use}, {headers : {'Authorization' : 'Bearer ' + token}});
export const removeWorkTimeData = (no, token,language) => axios.delete(address+'/worktime/remove/' + no+'?locale='+language, {headers: {'Authorization' : 'Bearer ' + token}});

export const getBreakTimeData = (no, token) => axios.get(address+'/breaktime/search/' + no, {headers: {'Authorization' : 'Bearer ' + token}});
export const editBreakTimeData = (no, start, end, use, description, token,language) => axios.put(address+"/breaktime/edit/" + no+"?locale="+language, {start, end, use, description}, {headers : {'Authorization' : 'Bearer ' + token}});
export const removeBreakTimeData = (no, token,language) => axios.delete(address+'/breaktime/remove/' + no+"?locale="+language, {headers: {'Authorization' : 'Bearer ' + token}});

export const getHoliDayData = (no, token) => axios.get(address+'/holiday/search/' + no, {headers: {'Authorization' : 'Bearer ' + token}});
export const editHoliDayData = (no, day, use, description, token,language) => axios.put(address+'/holiday/edit/' + no+'?locale='+language, {day, use, description}, {headers: {'Authorization' : 'Bearer ' + token}});
export const removeHoliDayData = (no, token,language) => axios.delete(address+'/holiday/remove/' + no+"?locale="+language, {headers: {'Authorization' : 'Bearer ' + token}});

export const getWorkAttitudeAlarmList = (keyNo, token) => axios.get(address+"/workattitude/alarm/list/"+ keyNo, {headers: {'Authorization' : 'Bearer ' + token}});

export const getWorkAttitudeCalendarList = (search, token,select)=> axios.get(address+"/workattitude/calendar/"+search.searchUserNo+"?month="+search.searchFromDate+"&workattitude="+search.searchWorkAttitude+"&select="+select, {headers : {'Authorization' : 'Bearer ' + token}});

export const getTime = ( token) => axios.get(address+'/time/use', {headers : {'Authorization' : 'Bearer ' + token}});
export const getStartTime = ( no,token) => axios.get(address+'/time/'+no, {headers : {'Authorization' : 'Bearer ' + token}});
export const getTotalTime = ( no,token) => axios.get(address+'/time/total/'+no, {headers : {'Authorization' : 'Bearer ' + token}});

export const getTodayCommuteEndTime = ( no,token) => axios.get(address+'/time/leaveWork/'+no, {headers : {'Authorization' : 'Bearer ' + token}});

export const updateRead = (key_no, auth, recordType, token) => axios.get(address+'/record/alarm/update/' + key_no + "/" + auth + "/" + recordType, {headers: {'Authorization' : 'Bearer ' + token}});


export const getDashBoardData=(token,searchDate)=> axios.get(address+'/dashboard/'+searchDate, {headers: {'Authorization' : 'Bearer ' + token}});

export const getBreakTimeUseList=(token)=> axios.get(address+'/breaktime/use', {headers: {'Authorization' : 'Bearer ' + token}});
