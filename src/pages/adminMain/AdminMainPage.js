import React from 'react';
import AdminMainContainer from 'containers/adminMain/AdminMainContainer';
import AdminTitleContainer from "containers/adminMain/AdminTitleContainer";

const AdminMainPage = () => {
    return (
        <div>
            <AdminTitleContainer />
            <AdminMainContainer />
        </div>
    );
};


export default AdminMainPage;