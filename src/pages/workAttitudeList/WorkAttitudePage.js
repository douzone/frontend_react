import React from 'react';
import WorkAttitudeSearchContainer from 'containers/workAttitude/WorkAttitudeSearchContainer';
import WorkAttitudeListContainer from 'containers/workAttitude/WorkAttitudeListContainer';
import WorkAttitudePaginationContainer from 'containers/workAttitude/WorkAttitudePaginationContainer';
import CalendarSearchContainer from 'containers/workAttitude/CalendarSearchContainer';
import WorkAttitudeStateContainer from 'containers/workAttitude/WorkAttitudeStateContainer';
import WorkAttitudeCalendarStateContainer from 'containers/workAttitude/WorkAttitudeCalendarStateContainer';
import WorkAttitudeModalContainer from "containers/modal/WorkAttitudeModalContainer";
import CalendarWorkAttitudeModalContainer from "containers/modal/CalendarWorkAttitudeModalContainer";
import WorkAttitudeTitleContainer from "containers/workAttitude/WorkAttitudeTitleContainer";

const WorkAttitudePage = () => {
    return (
        <div>
            <WorkAttitudeTitleContainer />
            <WorkAttitudeSearchContainer/>
            <WorkAttitudeStateContainer/>
            <WorkAttitudeCalendarStateContainer/>
            <WorkAttitudeListContainer/>
            <WorkAttitudePaginationContainer/>
            <CalendarSearchContainer/>
            <WorkAttitudeModalContainer/>
            <CalendarWorkAttitudeModalContainer/>
        </div>
    );
};

export default WorkAttitudePage;