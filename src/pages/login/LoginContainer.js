import { compose, withState, withHandlers, lifecycle } from "recompose";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

import LoginView from "./LoginView";
import { loginUser, resetError } from "./LoginState";

import locale from 'locale';

export default compose(
  connect(
    state => ({
      isLoading: state.login.isLoading,
      isAuthenticated: state.login.isAuthenticated,
      language: state.language.language,
      error: state.login.error,
      errorMessage: state.login.errorMessage
    }),
    { loginUser, resetError }
  ),
  withRouter,
  withState("nameValue", "setNameValue", ""),
  withState("loginValue", "setLoginValue", ""),
  withState("passwordValue", "setPasswordValue", ""),
  withHandlers({
    handleInput: props => (e, input = "login") => {
      if (props.error) {
        props.resetError();
      }

      if (input === "login") {
        props.setLoginValue(e.target.value);
      } else if (input === "password") {
        props.setPasswordValue(e.target.value);
      } else if (input === "name") {
        props.setNameValue(e.target.value);
      }
    },
    handleLoginButtonClick: props => () => {
      props.loginUser(props.loginValue, props.passwordValue);
    },
    handleKeyPress: props => (e) => {
      if(e.charCode === 13) {
        props.loginUser(props.loginValue, props.passwordValue);
      }
    }
  }),
  lifecycle({
    componentDidMount() {
      document.title = locale.loginTitle[this.props.language];
    },
    componentWillReceiveProps(nextProps) {
      if (!this.props.error && nextProps.error) {
        this.props.setPasswordValue("");
      }
    }
  })
)(LoginView);
