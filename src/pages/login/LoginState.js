import Axios from 'axios';
import RSA from 'node-rsa';
export const initialState = {
  isLoading: false,         // 로그인 요청
  isLogging: false,         // 현재 로그인 상태
  isLogouting: false,       // 로그아웃 요청
  isSignout: false,         // 로그아웃 상태 
  isAuthenticated: !!sessionStorage.getItem("id_token"),   // login 및 getUser 유무
  token: false,             // token
  name: false,              // 사용자 name
  id: false,                // 사용자 id
  no: false,                // 사용자 no
  auth: false,              // 권한
  error: null,              // 에러 발생 유무
  errorMessage: null        // 에러 응답 코드
};

export const START_LOGIN = "Login/START_LOGIN";
export const LOGIN_SUCCESS = "Login/LOGIN_SUCCESS";
export const LOGIN_FAILURE = "Login/LOGIN_FAILURE";

export const START_GET_USER = "Get/START_GET_USER";
export const GET_USER_SUCCESS = "Get/GET_USER_SUCCESS";
export const GET_USER_FAILURE = "Get/GET_USER_FAILURE";

export const RESET_ERROR = "Login/RESET_ERROR";
export const LOGIN_USER = "Login/LOGIN_USER";

export const START_LOGOUT = "Logout/START_LOGOUT";
export const SIGN_OUT_SUCCESS = "Logout/SIGN_OUT_SUCCESS";

export const startLogin = () => ({
  type: START_LOGIN
});

export const loginSuccess = (token) => ({
  type: LOGIN_SUCCESS,
  payload : {
    token : token
  }
});

export const loginFailure = (message) => ({
  type: LOGIN_FAILURE,
  payload : {
    message : message
  }
});

export const resetError = () => ({
  type: RESET_ERROR
});

export const startGetUser = () => ({
  type: START_GET_USER
});

export const getUserSuccess = (response, token) => ({
  type: GET_USER_SUCCESS,
  payload : {
    response : response,
    token : token
  }
});

export const getUserFailure = () => ({
  type: GET_USER_FAILURE
})

export const startLogout = () => ({
  type: START_LOGOUT
})

export const signOutSuccess = () => ({
  type: SIGN_OUT_SUCCESS
});

export const loginUser = (username, password) =>{
  return async (dispatch) => {
      // 로그인이 시작됨을 알리는 action 객체를 reducer에게 전달
      dispatch(startLogin());

      let key='-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDVdmwYWqL0Dy7BGoYSHBxuBcgj7btuTRGrWduSwKasiPMhO4tfAttfXP1ajI4cSN0PKg3T0obIOLvU4knFjhdXlvcQifjoQHziiW1QTyWr96kRRH0QW2G35cMcqvw9eFmTzXoXEc/1f2sAJ6eDcrq9YrLwbG8QQT3pNddgT6dkFQIDAQAB\n-----END PUBLIC KEY-----'
      const publicKey = new RSA(key);
      const encrypted = publicKey.encrypt(password, 'base64');
      password=encrypted;
      // backend와 통신
      await Axios.post('http://218.39.221.104:8080/smartchecker/user/auth', 
      { username, password })
      .then((response) => {
          // 로그인 성공시 성공을 알리는 action 객체를 reducer에게 전달
          if(response.data.result === 'fail') {
              dispatch(loginFailure(response.data.message));
          } 
          else if (response.data.token){
              sessionStorage.setItem("id_token", response.data.token);
              sessionStorage.setItem("listkeyNo", "define");
              sessionStorage.setItem("statekeyNo", "define");
              sessionStorage.setItem("pagekeyNo", "define");
              dispatch(loginSuccess(response.data.token));
          } 
          else {
              //console.log(response);
              dispatch(loginFailure(response.data.message));
          }
      }).catch((error) => {
          // 로그인 실패시 실패를 알리는 action 객체를 reducer에게 전달
          dispatch(loginFailure(error.message));
      });
  };
}

export const getUser = (token) => {
  return async (dispatch) => {
    dispatch(startGetUser());
    
    // 다국어(언어) server에 넘길시 'Accept-Language'에 언어 설정
    await Axios.get('http://218.39.221.104:8080/smartchecker/user', {
      headers : {
        'Authorization' : 'Bearer ' + token
      }
    })
    .then((response) => {
      console.log(response);
      dispatch(getUserSuccess(response, token));
    })
    .catch((error) => {
      dispatch(getUserFailure());
    })
  }
}

export const signOut = (token) => {
  return async(dispatch) => {
    dispatch(startLogout());
    await Axios.post('http://218.39.221.104:8080/smartchecker/user/logout', { token }, {
      headers : {
        'Authorization' : 'Bearer ' + token
      }
    })
    .then((response) => {
      // Storage 데이터들 전부 삭제
      localStorage.clear();
      // id_token 삭제
      sessionStorage.removeItem("id_token");
      // key_No 삭제
      sessionStorage.removeItem("listkeyNo");
      sessionStorage.removeItem("statekeyNo");
      sessionStorage.removeItem("pagekeyNo");
      // alarm 개수 삭제
      sessionStorage.removeItem("alarmnum");
  
      // token이 sessionStorage에서 완전히 삭제되었을 경우 로그아웃 액션 실행
      if(!sessionStorage.getItem("id_token")) {
          dispatch(signOutSuccess());
        }
    })
    .catch((error) => {
      //console.log("로그아웃 에러 응답");
      //console.log(error);
    })
  }
};

export default function LoginReducer(state = initialState, { type, payload }) {
  switch (type) {
    case START_LOGIN:
      return {
        ...state,
        isLoading: true
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuthenticated: true,
        token: payload.token,
        error: null
      };
    case LOGIN_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: true,
        errorMessage: payload.message
      };
    case START_GET_USER:
      return {
        ...state,
        isLoading: true
      };
    case GET_USER_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isLogging: true,
        token: payload.token,
        name: payload.response.data.name,
        no: payload.response.data.no,
        auth: payload.response.data.authorities[0].authority,
        id: payload.response.data.username,
        error: null
      };
    case GET_USER_FAILURE:
      return {
        ...state,
        isLoading: false,
        error: true
      };
    case RESET_ERROR:
      return {
        error: false
      };
    case START_LOGOUT:
      return {
        ...state,
        isLogouting: true
      };
    case SIGN_OUT_SUCCESS:
      return {
        isLoading: false,
        isLogging: false,
        isLogouting: false,
        isSignout: true,
        isAuthenticated: false,
        token: false,
        name: false,
        id: false,
        no: false,
        auth: false,
        error: null
      };
    default:
      return state;
  }
}
