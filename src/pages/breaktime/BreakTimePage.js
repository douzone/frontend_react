import React from 'react';
import BreakTimeContainer from 'containers/service/BreakTimeContainer';
import BreakTimeListContainer from 'containers/service/BreakTimeListContainer';
import BreakTimePaginationContainer from 'containers/service/BreakTimePaginationContainer';
import BreakTimeModalContainer from 'containers/modal/BreakTimeModalContainer';
import ServiceTitleContainer from "containers/service/ServiceTitleContainer";

const BreakTimePage = () => {
    return (
        <React.Fragment>
            <ServiceTitleContainer />
            <BreakTimeContainer/>
            <BreakTimeListContainer/>
            <BreakTimePaginationContainer/>
            <BreakTimeModalContainer/>
        </React.Fragment>
    );
};

export default BreakTimePage;