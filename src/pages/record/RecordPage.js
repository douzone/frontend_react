import React from 'react';
import RecordSearchContainer from 'containers/record/RecordSearchContainer';
import RecordListContainer from 'containers/record/RecordListContainer';
import RecordPaginationContainer from 'containers/record/RecordPaginationContainer';
import RecordStateContainer from 'containers/record/RecordStateContainer';
import RecordTitleContainer from 'containers/record/RecordTitleContainer';
import RecordModalContainer from 'containers/modal/RecordModalContainer';


const RecordPage = () => {
    return(
        <div>
            <RecordTitleContainer/>
            <RecordSearchContainer/>
            <RecordStateContainer/>
            <RecordListContainer/>
            <RecordPaginationContainer/>
            <RecordModalContainer/>
        </div>
    );
}

export default RecordPage;