import React from 'react';
import WorkTimeContainer from 'containers/service/WorkTimeContainer';
import WorkTimeListContainer from 'containers/service/WorkTimeListContainer';
import WorkTimePaginationContainer from 'containers/service/WorkTimePaginationContainer';
import WorkTimeModalContainer from 'containers/modal/WorkTimeModalContainer';
import ServiceTitleContainer from "containers/service/ServiceTitleContainer";

const WorkTimePage = () => {
    return (
        <React.Fragment>
            <ServiceTitleContainer />
            <WorkTimeContainer/>
            <WorkTimeListContainer/>
            <WorkTimePaginationContainer/>
            <WorkTimeModalContainer/>
        </React.Fragment>
    );
};

export default WorkTimePage;