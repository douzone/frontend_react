import "./ListView.css"
import React, { Component } from "react";
import LIstTableContainer from "containers/list/LIstTableContainer";
import PaginationContainer from "containers/list/PaginationContainer";
import ListSearchContainer from "containers/list/ListSearchContainer";
import CalendarContainer from "containers/list/CalendarSearchContainer";
import CommuteModalContainer from "containers/modal/CommuteModalContainer";
import CommuteStateContainer from "containers/list/CommuteStateContainer";
import CommuteCalendarStateContainer from "containers/list/CommuteCalendarStateContainer";
import CalendarCommuteModalContainer from "containers/modal/CalendarCommuteModalContainer";
import ListTitleContainer from "containers/list/ListTitleContainer";

class ListView extends Component {

    render(){

        return (
            <React.Fragment>
                <ListTitleContainer/>
                <ListSearchContainer/>
                <CommuteStateContainer/>
                <CommuteCalendarStateContainer/>
                <CalendarContainer/>
                <LIstTableContainer/>
                <PaginationContainer/>
                <CommuteModalContainer/>
                <CalendarCommuteModalContainer/>
            </React.Fragment>
        );
    }
};

export default ListView;