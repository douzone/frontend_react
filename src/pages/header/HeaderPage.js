import React from 'react';
import HeaderTimeContainer from 'components/Header/HeaderTimeContainer';
import HeaderContainer from 'components/Header/HeaderContainer';
import TimeContainer from 'components/time/HeaderContainer';
const HoliDayPage = () => {
    return (
        <div>
            <HeaderTimeContainer/>
            <HeaderContainer/>
        </div>
    );
};

export default HoliDayPage;