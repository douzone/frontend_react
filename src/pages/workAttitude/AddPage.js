import React from "react";
import AddContainer from "containers/workAttitude/AddContainer";
import WorkAttitudeTitleContainer from "containers/workAttitude/WorkAttitudeTitleContainer";

const AddPage = () => {
  return (
    <div>
      <WorkAttitudeTitleContainer />
      <AddContainer />
    </div>
  );
};

export default AddPage;
