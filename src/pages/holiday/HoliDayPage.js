import React from 'react';
import HoliDayContainer from 'containers/service/HoliDayContainer';
import HoliDayListContainer from 'containers/service/HoliDayListContainer';
import HoliDayPaginationContainer from 'containers/service/HoliDayPaginationContainer';
import HoliDayModalContainer from 'containers/modal/HoliDayModalContainer';
import ServiceTitleContainer from "containers/service/ServiceTitleContainer";

const HoliDayPage = () => {
    return (
        <React.Fragment>
            <ServiceTitleContainer />
            <HoliDayContainer/>
            <HoliDayListContainer/>
            <HoliDayPaginationContainer/>
            <HoliDayModalContainer/>
        </React.Fragment>
    );
};

export default HoliDayPage;