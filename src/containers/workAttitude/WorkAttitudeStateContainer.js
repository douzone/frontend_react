import React, { Component } from 'react';
import WorkAttitudeState from 'components/workAttitude/WorkAttitudeState';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as stateActions from 'store/modules/state';

class WorkAttitudeStateContainer extends Component {

    getStateList = async() => {
        const {StateActions} = this.props;
        const {token, auth, loginUserNo} = this.props;
        let keyNo = null;
        if(sessionStorage.getItem("statekeyNo") !== "define") {
            keyNo = sessionStorage.getItem("statekeyNo");
            sessionStorage.removeItem("statekeyNo");
            sessionStorage.setItem("statekeyNo", "define");
        }
        try {
            if(auth === "ROLE_USER") {
                const searchUserNo = loginUserNo;
                const search = {searchUserNo};

                //재성아 알림시 막아야함
                if(keyNo === null) {
                    await StateActions.getWorkAttitudeSearchStateList(search, token);
                }
            }
            else {
                await StateActions.getWorkAttitudeStateList(token);
            }
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getStateList();
    }

    render() {
        const { tables, loading, changeView } =this.props;

        if(changeView !== 'table') return null;
        if(loading) return null;

        return (
            <div>
                <WorkAttitudeState tables={tables}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.state.get('tables'),
        changeView: state.state.get('changeView'),
        loading: state.pender.pending['state/WORK_ATTITUDE_STATE_LIST'],
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no
    }),
    (dispatch) => ({
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(WorkAttitudeStateContainer);