import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Pagination from 'components/list/Pagination';
import * as paginationActions from 'store/modules/pagination';
import * as workAttitudeActions from 'store/modules/workattitude';
import * as listActions from 'store/modules/workattitudecalendarlist';


class BreakTimePaginationContainer extends Component {

    handlePageChange = (pageNumber) => {
        const { activePage, PaginationActions } = this.props;

        PaginationActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handleList = async(pageNumber) => {

        const { WorkAttitudeActions, searchFromDate, searchToDate, searchUserNo, name, searchWorkAttitude } = this.props;
        const { token, language } = this.props;
        if(searchFromDate === '' && searchToDate === '' && searchUserNo === '' && searchWorkAttitude === '') {
            try{
                await WorkAttitudeActions.getWorkAttitudeList('', pageNumber, token, language);
            } catch(e){
                console.log(e);
            }
        }
        else{
            try{
                const search = {
                    searchWorkAttitude, searchFromDate, searchToDate, searchUserNo
                }

                await WorkAttitudeActions.getWorkAttitudeList(search, pageNumber, token, language);
            } catch(e){
                console.log(e);
            }
        }

        
    }

    handlePage = async() => {
        const { PaginationActions, auth, token } = this.props;
        let keyNo = null;
        if(sessionStorage.getItem("pagekeyNo") !== "define") {
            keyNo = sessionStorage.getItem("pagekeyNo");
            sessionStorage.removeItem("pagekeyNo");
            sessionStorage.setItem("pagekeyNo", "define");
        }
        try{
            // KJS: 알림 리스트 클릭시 Storage의 "keyNo"값이 "define"이 아닌 다른 값으로 변경됩니다.
            if(keyNo === null && auth === "ROLE_ADMIN") {
                await PaginationActions.getWorkAttitudeTotal('', token);
            }
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { PaginationActions } = this.props;

        PaginationActions.initialize();
    }

    render() {
        const { activePage, totalCount,changeView } = this.props;
        if(changeView !== 'table') return null;
        return (
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        totalCount: state.pagination.get("totalCount"),
        changeView: state.workattitude.get('changeView'),
        searchWorkAttitude: state.workattitude.get('searchWorkAttitude'),
        searchFromDate: state.workattitude.get('searchFromDate'),
        searchToDate: state.workattitude.get('searchToDate'),
        searchUserNo: state.workattitude.get('searchUserNo'),
        token: state.login.token,
        auth: state.login.auth,
        language: state.language.language
    }),
    (dispatch) => ({
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        WorkAttitudeActions: bindActionCreators(workAttitudeActions, dispatch),
        ListActions: bindActionCreators(listActions, dispatch)
    })
)(BreakTimePaginationContainer);