import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import Calendar from 'components/workAttitude/calendar/Calendar';
import * as workattitudecalendarlist from 'store/modules/workattitudecalendarlist';
import * as workattitude from 'store/modules/workattitude';

class CalendarSeachContainer extends Component {
    handleChangeInput = ({name, value}) => {
        
        const { Workattitudecalendarlist } = this.props;
        Workattitudecalendarlist.changeInput({name, value});
    }
    render() {
        const { calList, loading, language, changeView, search, token, workattitude, auth } =this.props;
        if(changeView !== 'calendar') return null;
        if(loading) return null;
        return (
            <Calendar
                calList={calList} language={language} handleChangeInput={this.handleChangeInput}
                search={search} token={token} workattitude={workattitude} auth={auth}
            />
        );
    }
}


export default connect(
    (state) => (
        {
        calList: state.workattitudecalendarlist.get('calList'),
        changeView: state.workattitude.get('changeView'),
        loading: state.pender.pending['workattitude/CALENDAR_LIST'],
        language: state.language.language,
        search:state.workattitudecalendarlist.get('search'),
        token: state.login.token,
        auth: state.login.auth
    }),
    (dispatch) => ({
        Workattitudecalendarlist: bindActionCreators(workattitudecalendarlist, dispatch),
        workattitude: bindActionCreators(workattitude, dispatch)
    })
)(CalendarSeachContainer);