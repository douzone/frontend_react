import React, {Component} from 'react';
import WorkAttitudeSearch from 'components/workAttitude/WorkAttitudeSearch/WorkAttitudeSearch';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as paginationActions from 'store/modules/pagination';
import CalendarTableSelect from 'components/workAttitude/CalendarTableSelect';
import * as workAttitudeActions from 'store/modules/workattitude';
import * as getCalList from 'store/modules/workattitudecalendarlist';
import * as stateActions from 'store/modules/state';
import locale from 'locale';


class WorkAttitudeSearchContainer extends Component {

    handleChangeInput = ({name, value}) => {

        const {WorkAttitudeActions} = this.props;
        WorkAttitudeActions.changeInput({name, value});
    }

    handleChangeRadio = ({name, value}) => {
        const {WorkAttitudeActions, StateActions} = this.props;
        WorkAttitudeActions.changeInput({name, value});

        if (value === 'calendar') {
            StateActions.changeInput({name: 'changeView', value: 'calendar'});
        }

        if (value === 'table') {
            StateActions.changeInput({name: 'changeView', value: 'table'});
        }
    }
    
    componentDidMount() {
        const {WorkAttitudeActions, language} = this.props;

        WorkAttitudeActions.initialize();
    }

    handleSubmit = async (searchUserNo = 0, select, searchUserName) => {
        // //console.log(searchUserNo);
        // //console.log(select);
        // //console.log(searchUserName);

        const {searchFromDate, searchToDate, activePage, WorkAttitudeActions, GetCalList, PaginationActions,
             searchWorkAttitude, token, changeView, StateActions,auth,loginUserNo, language} = this.props;

        if(auth==="ROLE_USER"){
            select=true;
            searchUserNo=loginUserNo
        }
        
        const search = {
            searchFromDate,
            searchToDate,
            searchUserName,
            searchUserNo,
            searchWorkAttitude
        }

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            // //console.log(searchUserNo);
            // //console.log(select);
            // //console.log(searchUserName);
            // //console.log(searchWorkAttitude);

            if (changeView === "table") {
                await StateActions.getWorkAttitudeSearchStateList(search, token);
                await PaginationActions.getWorkAttitudeTotal(search, token);
                await WorkAttitudeActions.getWorkAttitudeList(search, pageNumber, token, language);
                let name="searchFromDate"
                let value=searchFromDate;
                workAttitudeActions.changeInput({name, value});
  
                 name="searchToDate"
                 value=searchToDate;
                 workAttitudeActions.changeInput({name, value});
                name="searchUserName"
                 value=searchUserName;
                 workAttitudeActions.changeInput({name, value});
                name="searchUserNo"
                value=searchUserNo;
                workAttitudeActions.changeInput({name, value});
               name="searchUserNo"
               value=searchUserNo;
               workAttitudeActions.changeInput({name, value});
              name="searchWorkAttitude"
              value=searchWorkAttitude;
              workAttitudeActions.changeInput({name, value});
            }
            else {
                if (search.searchUserNo == null)
                    search.searchUserNo = 0;
                if (!select)
                    search.searchUserNo = searchUserName;

                await StateActions.getWorkAttitudeCalendarStateList(searchFromDate, searchUserNo, searchUserName, searchWorkAttitude, select, token);

                await GetCalList.getList(search, token, select);
                GetCalList.changeInput({
                    name: "search",
                    value: search
                })
                GetCalList.changeInput({
                    name: "select",
                    value: select
                })
                GetCalList.changeInput({
                    name: "userName",
                    value: searchUserName
                })

            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const {language, searchWorkAttitude, searchFromDate, searchToDate, searchUserNo, changeView} = this.props;
        const {handleSubmit} = this;
        const {startdate, enddate, name, content, gubun, token,auth} = this.props;

        return (
            <div>
                <WorkAttitudeSearch searchWorkAttitude={searchWorkAttitude} searchFromDate={searchFromDate}
                                    searchToDate={searchToDate} token={token}
                                    searchUserNo={searchUserNo} onChangeInput={this.handleChangeInput}
                                    onSubmit={handleSubmit} changeView={changeView}
                                    language={language} auth={auth}/>
                <CalendarTableSelect language={language} onChangeInput={this.handleChangeRadio} changeView={changeView}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        searchWorkAttitude: state.workattitude.get('searchWorkAttitude'),
        searchFromDate: state.workattitude.get('searchFromDate'),
        searchToDate: state.workattitude.get('searchToDate'),
        searchUserNo: state.workattitude.get('searchUserNo'),
        searchUserName: state.workattitude.get('searchUserName'),
        changeView: state.workattitude.get('changeView'),
        token: state.login.token,
        language: state.language.language,
        auth: state.login.auth,
        loginUserNo:state.login.no
    }),
    (dispatch) => ({
        WorkAttitudeActions: bindActionCreators(workAttitudeActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(WorkAttitudeSearchContainer);