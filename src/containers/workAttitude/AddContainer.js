import React, { Component } from "react";
import Add from "components/workAttitude/WorkAttitudeAdd";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import * as addActions from "store/modules/workattitude";
import locale from 'locale';
import * as lanActions from "store/modules/language";

class AddContainer extends Component {
  componentDidMount() {
    const { AddActions, language } = this.props;
    document.title = locale.workAttitudeNewTitle[language];
    AddActions.initialize(); // 에디터를 초기화합니다.
  }

  shouldComponentUpdate(nextProps, nextState) {
    document.title = locale.workAttitudeNewTitle[nextProps.language];
    return true;
  }
  
  handleChangeInput = ({ name, value }) => {
    const { AddActions } = this.props;
    AddActions.changeInput({ name, value });
  };

  handleSubmit = async () => {

    const {
      name,
      startDay,
      endDay,
      title,
      add,
      content,
      AddActions,
      language
    } = this.props;
    //const userId = "aabb";
    const param = {
      name,
      startDay,
      endDay,
      title,
      state: '정상',
      workAttitudeList: add,
      userNo: localStorage.getItem('no'),
      userId: localStorage.getItem('id'),
      content,
      no: 1
    };
    const { token } = this.props;
    try {
      await AddActions.writeAdd(param, token,language);
      AddActions.initialize(); // 에디터를 초기화합니다.
    } catch (e) {
      console.log(e);
    }
  };

  render() {
    const { handleChangeInput, handleSubmit } = this;
    const { name, startDay, endDay, title, gubun, content, loginName, language,workAttitudeAddModal,LanActions,add } = this.props;
    return (
      <Add
        name={name}
        loginName={loginName}
        startDay={startDay}
        endDay={endDay}
        title={title}
        gubun={gubun}
        content={content}
        onChangeInput={handleChangeInput}
        onSubmit={handleSubmit}
        language={language}
        workAttitudeAddModal={workAttitudeAddModal}
        LanActions={LanActions}
        add={add}
      />
    );
  }
}

export default connect(
  state => ({
    name: state.workattitude.get("name"),
    startDay: state.workattitude.get("startDay"),
    endDay: state.workattitude.get("endDay"),
    title: state.workattitude.get("title"),
    gubun: state.workattitude.get("gubun"),
    content: state.workattitude.get("content"),
    workAttitudeAddModal:state.workattitude.get("workAttitudeAddModal"),
    token: state.login.token,
    loginName: state.login.name,
    language: state.language.language,
    add: state.workattitude.get("add")
  }),
  dispatch => ({
    AddActions: bindActionCreators(addActions, dispatch),
    LanActions: bindActionCreators(lanActions, dispatch)
  })
)(AddContainer);
