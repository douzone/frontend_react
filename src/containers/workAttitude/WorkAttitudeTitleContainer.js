import React, {Component} from 'react';
import {connect} from 'react-redux';
import Title from 'components/workAttitude/Title';

class WorkAttitudeTitleContainer extends Component {
    render() {
        const { language, auth } = this.props;
        return(
            <Title 
                language={language} auth={auth}
            />
        );
    }
}

export default connect(
    (state) => ({
        language: state.language.language,
        auth: state.login.auth
    })
)(WorkAttitudeTitleContainer)