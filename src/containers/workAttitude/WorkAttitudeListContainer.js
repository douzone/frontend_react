import React, {Component} from 'react';
import WorkAttitudeList from 'components/workAttitude/WorkAttitudeList';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as listActions from 'store/modules/workattitudecalendarlist';
import * as workAttitudeActions from 'store/modules/workattitude';
import * as paginationActions from 'store/modules/pagination';
import * as getCalList from 'store/modules/workattitudecalendarlist';
import * as stateActions from 'store/modules/state';
import locale from 'locale';


class WorkAttitudeListContainer extends Component{

    getList = async() => {
        const { WorkAttitudeActions, activePage, language } = this.props;
        const { token ,auth,loginUserNo,GetCalList, PaginationActions} = this.props;

        try{if(auth==="ROLE_ADMIN"){
            await WorkAttitudeActions.getWorkAttitudeList('', activePage, token, language);
        }
        else{
            let date = new Date();
            let dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                                .toISOString()
                                .split("T")[0].substring(0,7);


                const search = {
                    searchFromDate:dateString,
                    searchUserNo:loginUserNo,
                    searchWorkAttitude:"전체"
                }

                const pageNumber = 1;

                //리스트
                await PaginationActions.getWorkAttitudeTotal(search, token);
                await WorkAttitudeActions.getWorkAttitudeList(search, pageNumber, token, language);
                //달력
                await GetCalList.getList(search, token, true);
        }
        } catch(e){
            console.log(e);
        }
    }

    getAlarmSearchList = async(keyNo) => {
        const { WorkAttitudeActions, PaginationActions, token, language, StateActions } = this.props;
        const pageNumber = 1

        const search = {
            keyNo
        }

        try {
            await StateActions.getWorkAttitudeSearchStateList(search, token);
            await PaginationActions.getWorkAttitudeTotal(search, token);
            await WorkAttitudeActions.getWorkAttitudeList(search, pageNumber, token, language);
        } catch(e) {
            console.log(e);
        }
    }

    componentDidMount() {
        let keyNo = null;
        const {language} = this.props;
        if(sessionStorage.getItem("listkeyNo") !== "define") {
            keyNo = sessionStorage.getItem("listkeyNo");
            sessionStorage.removeItem("listkeyNo");
            sessionStorage.setItem("listkeyNo", "define");
        }
        if(keyNo !== null) {
            this.getAlarmSearchList(keyNo);
        } else {
            this.getList();
        }
        document.title = locale.workAttitudeListTitle[language];
    }
  
    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.workAttitudeListTitle[nextProps.language];
        return true;
    }

    // (list, calendar)리스트 클릭시 updateRead액션 발생
    // MongoDB의 key_no와 no가 일치하면 read true로 update
    updateRead = (no) => {
        const { token, auth, WorkAttitudeActions } = this.props;
        const recordType = "근태";
        WorkAttitudeActions.updateRead(no, auth, recordType, token);
    }
    
    handleChangeInput = ({name, value}) => {
        if(name === 'no') {
            this.updateRead(value);
        }
        const { WorkAttitudeActions } = this.props;
        WorkAttitudeActions.changeInput({name, value});
    }

    render() {
        const { tables, loading, badge,changeView, language } = this.props;
        const { handleChangeInput } = this;
        
        if(loading && tables <= 0) return null;
        if(changeView !== 'table') return null;
        return (
            <div>
                <WorkAttitudeList tables={tables} badge={badge} language={language} handleChangeInput={handleChangeInput}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        changeView: state.workattitude.get('changeView'),
        no: state.workattitude.get('no'),
        tables: state.workattitude.get('tables'),
        badge: state.workattitude.get('badge'),
        loading: state.pender.pending['workattitude/WORK_ATTITUDE_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language,
        auth: state.login.auth,
        loginUserNo: state.login.no
    }), 
    (dispatch) => ({
        WorkAttitudeActions: bindActionCreators(workAttitudeActions, dispatch),
        ListActions: bindActionCreators(listActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(WorkAttitudeListContainer);