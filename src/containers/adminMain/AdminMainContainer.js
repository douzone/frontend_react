import React, { Component } from 'react';
import AdminMain from 'components/adminMain/AdminMain';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as dashboardActions from "store/modules/dashboard";
import moment from 'moment';
import locale from 'locale';

class AdminMainContainer extends Component {

    changeInput(name,value){
        const {DashboardActions,token} = this.props;
        //DashboardActions.changeInput({name,value});
        //console.log(value);
        DashboardActions.getDashBoardData(token,value);
    }

    componentWillMount(){
        //console.log("윌마운트")
        const {token,DashboardActions,searchDate} = this.props;
         DashboardActions.getDashBoardData(token,searchDate);
    }
    render(){
        const { language,totalUserCount,gotoWorkCount,tardyCount,chartList,businessTripCount,
            workOutsideCount,annualLeaveCount,educationCount,searchDate} = this.props;

        let parm = {
            language,totalUserCount,gotoWorkCount,tardyCount,chartList,workOutsideCount,businessTripCount,
            annualLeaveCount,educationCount,searchDate
        }
        return(
            <div>
                <AdminMain parm={parm} language={language} changeInput={(name,value)=>this.changeInput(name,value)}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        token: state.login.token,
        language: state.language.language,
        totalUserCount: state.dashboard.get("totalUserCount"),
        gotoWorkCount: state.dashboard.get("gotoWorkCount"),
        tardyCount: state.dashboard.get("tardyCount"),
        chartList:state.dashboard.get("chartList"),
        educationCount:state.dashboard.get("educationCount"),
        businessTripCount:state.dashboard.get("businessTripCount"),
        workOutsideCount:state.dashboard.get("workOutsideCount"),
        annualLeaveCount:state.dashboard.get("annualLeaveCount"),
        searchDate:state.dashboard.get("searchDate")
    }),
    (dispatch) => ({
        DashboardActions: bindActionCreators(dashboardActions, dispatch),

    })
)(AdminMainContainer);