import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as listActions from 'store/modules/commutecalendarlist';
import Calendar from 'components/list/calendar/Calendar';

class CalendarSeachContainer extends Component {

    handleChangeInput = ({name, value}) => {
        
        const { ListActions } = this.props;
        ListActions.changeInput({name, value});
    }
    
    render() {
        const { calList, token, ListActions, loading, changeView, search, language, auth } =this.props;
        if(changeView !== 'calendar') return null;
        if(loading) return null;
        return (
            <Calendar
                language={language} 
                calList={calList} 
                token={token}
                handleChangeInput={this.handleChangeInput} 
                search={search}
                ListActions={ListActions}
                auth={auth}
            />
        );
    }
}


export default connect(
    (state) => (
        {
        calList: state.commutecalendarlist.get('calList'),
        changeView: state.list.get('changeView'),
        loading: state.pender.pending['list/GET_CALENDAR_LIST'],
        search: state.commutecalendarlist.get('search'),
        language: state.language.language,
        token: state.login.token,
        auth: state.login.auth
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch)
    })
)(CalendarSeachContainer);