import React, { Component } from 'react';
import CalendarState from 'components/list/CalendarState';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as stateActions from 'store/modules/state';


class CommuteCalendarStateContainer extends Component {

    getStateList = async() => {
        const {StateActions} = this.props;
        const {token, auth, loginUserNo} = this.props;

        if(auth === "ROLE_USER"){
            try{
                await StateActions.getCommuteCalendarStateList(null, loginUserNo, null, null, true, token);
            }
            catch(e){
                console.log(e);
            }
        }
     }

    componentWillMount() {
        this.getStateList();
    }

    componentDidMount() {
        const {StateActions} = this.props;

        StateActions.initialize();
    }


    render() {
        const { calendarTables, loading, changeView, language } =this.props;

        if(changeView !== 'calendar') return null;
        if(loading) return null;

        return (
            <div>
                <CalendarState language={language} calendarTables={calendarTables}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        calendarTables: state.state.get('calendarTables'),
        changeView: state.state.get('changeView'),
        loading: state.pender.pending['state/COMMUTE_CALENDAR_STATE_LIST'],
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(CommuteCalendarStateContainer);