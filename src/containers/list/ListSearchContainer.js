import React, {Component} from 'react';
import Search from 'components/list/Search';
import CalendarTableSelect from 'components/list/CalendarTableSelect';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as listActions from 'store/modules/list';
import * as getCalList from 'store/modules/commutecalendarlist';
import * as paginationList from 'store/modules/pagination';
import * as stateActions from 'store/modules/state';


class ListSearchContainer extends Component {

    handleClick = (value) => {
        const {ListActions} = this.props;
        ListActions.click(value);
    }

    handleChangeInput = ({name, value}) => {
        const {ListActions} = this.props;
        ListActions.changeInput({name, value});
    }

    handleChangeRadio = ({name, value}) => {
        const {ListActions, StateActions} = this.props;
        ListActions.changeInput({name, value});

        if (value === 'calendar') {
            StateActions.changeInput({name: 'changeView', value: 'calendar'});
        }

        if (value === 'table') {
            StateActions.changeInput({name: 'changeView', value: 'table'});
        }
    }

    handleSubmit = async (searchUserNo = 0, select, userName) => {

        const {searchFromDate, searchToDate, activePage, ListActions, GetCalList, PaginationActions, searchState, changeView, StateActions} = this.props;
        const {token, auth, loginUserNo, language} = this.props;

        if (auth === "ROLE_USER") {
            select = true;
            searchUserNo = loginUserNo
        }
        const search = {
            searchFromDate,
            searchToDate,
            userName,
            searchUserNo,
            searchState,
            select
        }

        //console.log("@@@@@@")
        //console.log(searchUserNo)
        //console.log(select)
        //console.log(userName);
        //console.log(search);
        //console.log("@@@@@@");

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});


        try {
            if (changeView === "table") {
                await StateActions.getCommuteSearchStateList(search, token);
                await PaginationActions.getSearchPage(search, token);
                await ListActions.getTableList(search, pageNumber, select, token, language);
                
                let name="searchFromDate"
                let value=searchFromDate;
                ListActions.changeInput({name, value});
                
                name="subStartdate"
                ListActions.changeInput({name, value});

                name="searchToDate"
                value=searchToDate;
                ListActions.changeInput({name, value});

                name="subEnddate"
                ListActions.changeInput({name, value});

                name="userName"
                value=userName;
                ListActions.changeInput({name, value});

                name="searchUserNo"
                value=searchUserNo;
                ListActions.changeInput({name, value});

                name="searchState"
                value=searchState;
                ListActions.changeInput({name, value});

                name="subState"
                ListActions.changeInput({name, value});

                name="select"
                value=select;
                ListActions.changeInput({name, value});
                
                name="subSelect"
                ListActions.changeInput({name, value});
            } else {
                if (search.searchUserNo == null)
                    search.searchUserNo = 0;

                if (!select)
                    search.searchUserNo = userName;

                await StateActions.getCommuteCalendarStateList(searchFromDate, searchUserNo, userName, searchState, select, token);
                await GetCalList.getList(search, token, select);
                GetCalList.changeInput({
                    name: "search",
                    value: search
                })
                GetCalList.changeInput({
                    name: "select",
                    value: select
                })
                GetCalList.changeInput({
                    name: "userName",
                    value: userName
                })
                GetCalList.changeInput({
                    name: "userName",
                    value: userName
                })
                ListActions.changeInput({
                    name: "subCalendardate",
                    value: searchFromDate
                })
            }


        } catch
            (e) {
            console.log(e);
        }
    }

    componentDidMount() {
        const {ListActions} = this.props;

        ListActions.initialize();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if (nextProps.select !== this.props.select) {
            this.handleChangeInput({name: 'select', value: nextProps.select});
        }

        if (nextProps.username !== this.props.userName) {
            this.handleChangeInput(({name: 'userName', value: nextProps.userName}));
        }

        if (nextProps.no !== this.props.no) {
            this.handleChangeInput({name: 'no', value: nextProps.no});
        }

        return true;
    }

    render() {
        const {searchFromDate, searchToDate, subStartdate, subEnddate, name, searchUserNo, show, 
                searchState, changeView, token, language, auth, userName, 
                loginUserName, state, subState, tables, select, subSelect, subCalendardate} = this.props;
        const {handleChangeInput, handleSubmit, handleChangeRadio} = this;

        return (
            <div>
                <Search
                    searchFromDate={searchFromDate}
                    searchToDate={searchToDate}
                    name={name}
                    onChangeInput={handleChangeInput}
                    onSubmit={handleSubmit}
                    onClick={this.handleClick}
                    searchUserNo={searchUserNo}
                    show={show}
                    searchState={searchState}
                    changeView={changeView}
                    token={token}
                    auth={auth}
                    language={language}
                />
                <CalendarTableSelect 
                language={language} searchFromDate={subStartdate} searchToDate={subEnddate} name={userName}
                onChangeInput={handleChangeRadio} changeView={changeView}
                auth={auth} loginUserName={loginUserName} state={state} subState={subState} tables={tables} select={select} 
                subSelect={subSelect} subCalendardate={subCalendardate}/>
            </div>
        );
    }
}

//구독
export default connect(
    (state) => ({
        searchFromDate: state.list.get('searchFromDate'),
        searchToDate: state.list.get('searchToDate'),
        searchState: state.list.get('searchState'),
        searchUserNo: state.list.get('searchUserNo'),
        startdate: state.list.get('startdate'),
        enddate: state.list.get('enddate'),
        name: state.list.get('name'),
        no: state.list.get('no'),
        userName: state.list.get('userName'),
        select: state.list.get('select'),
        state: state.list.get('state'),
        changeView: state.list.get('changeView'),
        activePage: state.pagination.get('activePage'),
        tables: state.list.get('tables'),
        subStartdate: state.list.get('subStartdate'),
        subEnddate: state.list.get('subEnddate'),
        subCalendardate: state.list.get('subCalendardate'),
        subState: state.list.get('subState'),
        subSelect: state.list.get('subSelect'),
        token: state.login.token,
        language: state.language.language,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        loginUserName: state.login.name
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(ListSearchContainer);