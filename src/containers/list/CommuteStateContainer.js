import React, { Component } from 'react';
import State from 'components/list/State';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as stateActions from 'store/modules/state';

class CommuteStateContainer extends Component {

    getStateList = async() => {
        const {StateActions} = this.props;
        const {token, auth, loginUserNo} = this.props;
        let keyNo = null;
        if(sessionStorage.getItem("statekeyNo") !== "define") {
            keyNo = sessionStorage.getItem("statekeyNo");
            sessionStorage.removeItem("statekeyNo");
            sessionStorage.setItem("statekeyNo", "define");
        }

        try {
            if(auth === "ROLE_ADMIN") {
                await StateActions.getCommuteStateList(token);
            }
            else{
                const search = {
                    searchUserNo: loginUserNo,
                    select: true
                }
                if(keyNo === null) {
                    await StateActions.getCommuteSearchStateList(search, token);
                }
            }
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getStateList();
    }

    render() {
        const { tables, loading, changeView, language} =this.props;

        if(changeView !== 'table') return null;
        if(loading) return null;

        return (
            <div>
                <State language={language} tables={tables}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.state.get('tables'),
        changeView: state.state.get('changeView'),
        loading: state.pender.pending['state/COMMUTE_STATE_LIST'],
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(CommuteStateContainer);