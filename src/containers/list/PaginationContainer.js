import React, {Component} from 'react';
import Pagination from 'components/list/Pagination';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as listActions from 'store/modules/pagination';
import * as tableActions from 'store/modules/list';


class PaginationContainer extends Component {

    handlePageChange = (pageNumber) =>{
        const {activePage, ListActions} = this.props;

        ListActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handlePage = async() => {
        const {ListActions, auth, token, loginUserNo } = this.props;
        let keyNo = null;
        if(sessionStorage.getItem("pagekeyNo") !== "define") {
            keyNo = sessionStorage.getItem("pagekeyNo");
            sessionStorage.removeItem("pagekeyNo");
            sessionStorage.setItem("pagekeyNo", "define");
        }
        try {
            
            if(auth==="ROLE_ADMIN" && keyNo === null){
                await ListActions.getPage(token);
            }

        } catch (e) {
            console.log(e);
        }
    }

    handleList = async(pageNumber) => {
        const {TableActions, searchFromDate, searchToDate, userName, searchUserNo, no, searchState, select, language} = this.props;
        const { token } = this.props;
        const paramNo = no;

        if(searchFromDate === '' && searchToDate === '' && userName === '' && searchUserNo === '' && searchState === '') {
            try {
                //console.log("change page : " + pageNumber);
                //console.log("select : " + select);

                await TableActions.getTableList('', pageNumber, select, token, language);
            } catch (e) {
                console.log(e);
            }
        }
        else{
            try{
                const search = {searchFromDate, searchToDate, userName, paramNo, searchState};

                await TableActions.getTableList(search, pageNumber, select, token, language);
            }
            catch(e){
                console.log(e);
            }
        }
    }

    componentWillMount() {
        this.handlePage();
    }

    componentDidMount() {
        const { ListActions } = this.props;

        ListActions.initialize();
    }

    render(){
        const {activePage, totalCount, changeView} = this.props;
        if(changeView !== 'table') return null;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get("activePage"),
        totalCount: state.pagination.get("totalCount"),
        searchFromDate: state.list.get("searchFromDate"),
        searchToDate: state.list.get("searchToDate"),
        userName: state.list.get("userName"),
        searchState: state.list.get("searchState"),
        searchUserNo: state.list.get("searchUserNo"),
        no: state.list.get("no"),
        select: state.list.get('select'),
        tables: state.list.get("tables"),
        changeView: state.list.get('changeView'),
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        TableActions: bindActionCreators(tableActions, dispatch)
    })
)(PaginationContainer);