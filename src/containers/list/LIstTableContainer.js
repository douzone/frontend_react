import React, {Component} from 'react';
import TableList from 'components/list/TableList';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as listActions from 'store/modules/list';
import * as getCalList from 'store/modules/commutecalendarlist';
import * as stateActions from 'store/modules/state';
import * as paginationList from 'store/modules/pagination';
import locale from 'locale';


class LIstTableContainer extends Component {

    /*
    getPostList = () => {
        // 페이지와 태그 값을 부모로부터 받아 옵니다.
        const { activePage, ListActions } = this.props;
        
        const search = {
            startdate : '2019-04-25',
            enddate : '2019-04-25',
            name : '박성혜'
        }
        const page = activePage;
        //console.log(search);
        ListActions.getTableList(search, page);
      }
    
    componentDidMount() {
    this.getPostList();
    }
    */

    getFullList = async() => {
        const { activePage, ListActions, select, GetCalList, PaginationActions, language } = this.props;
        const { token,auth,loginUserNo } = this.props;
        
        try {

            if (auth === "ROLE_ADMIN") {
                await ListActions.getTableList('', activePage, select, token, language);
            } else {

                let date = new Date();
                let dateString = new Date(date.getTime() - (date.getTimezoneOffset() * 60000 ))
                                        .toISOString()
                                        .split("T")[0].substring(0,7);

                const search = {
                    searchFromDate: dateString,
                    searchUserNo: loginUserNo,
                    searchState: "전체"
                }

                const listSearch = {
                    searchUserNo:loginUserNo,
                    select:true
                }
                
                const pageNumber = 1;
                await PaginationActions.getSearchPage(listSearch, token);
                await ListActions.getTableList(listSearch, pageNumber, listSearch.select, token, language);
                //달력
                await GetCalList.getList(search, token, true);
            }
        } catch (e) {
            console.log(e);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.commuteListTitle[nextProps.language];

        if (nextProps.userNo !== this.props.userNo) {
            this.handleChangeInput({name: 'userNo', value: nextProps.userNo});
        }

        return true;
    }

    componentDidMount() {
        const { language } = this.props;
        document.title = locale.commuteListTitle[language];
    }

    /* 
    주영돈 : WorkAttitudeListContainer.js의 getAlarmSearchList처럼
    keyNo에 대한 데이터 가져오는 코드 작성하시면 됩니다.
    */
    getAlarmSearchList = async(keyNo) => {
        const { token, language, PaginationActions, ListActions, StateActions } = this.props;
        const pageNumber = 1;

        const search = {
            keyNo
            ,select: false
        }

        try {
            await StateActions.getCommuteSearchStateList(search, token);
            await PaginationActions.getSearchPage(search, token);
            await ListActions.getTableList(search, pageNumber, search.select, token, language);
        } catch(e) {
            console.log(e);
        }
    }

    componentWillMount() {
        const {auth} = this.props;
        let keyNo = null;
        if(sessionStorage.getItem("listkeyNo") !== "define") {
            keyNo = sessionStorage.getItem("listkeyNo");
            sessionStorage.removeItem("listkeyNo");
            sessionStorage.setItem("listkeyNo", "define");
        }
        // USER가 알림(출퇴근 수정) 클릭했을 경우
        if(keyNo !== null && auth === 'ROLE_USER') {
            this.getAlarmSearchList(keyNo);
        } else {
            this.getFullList();
        }
    }

    // (list, calendar)리스트 클릭시 updateRead액션 발생
    // MongoDB의 key_no와 startNo가 일치하면 read true로 update
    updateRead = (no) => {
        const { token, auth, ListActions } = this.props;
        const recordType = "출퇴근"
        ListActions.updateRead(no, auth, recordType, token);
    }

    handleChangeInput = ({name, value}) => {
        const {ListActions, auth} = this.props;
        // 권한이 유저일 경우에 출퇴근 칼럼 클릭시 read update
        if(name === 'startNo' && auth === 'ROLE_USER') {
            this.updateRead(value);
        }
        ListActions.changeInput({name, value});
    }

    render() {
        const {tables, loading, changeView, language} = this.props;
        const {handleChangeInput} = this;

        if (changeView !== 'table') return null;
        if (loading && tables <= 0) return null;

        return (
            <div>
                <TableList language={language} tables={tables} handleChangeInput={handleChangeInput}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.list.get('tables'),
        changeView: state.list.get('changeView'),
        startNo: state.list.get('startNo'),
        endNo: state.list.get('endNo'),
        select: state.list.get('select'),
        userNo: state.list.get('userNo'),
        loading: state.pender.pending['list/FULL_TABLE_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
    })
)(LIstTableContainer);