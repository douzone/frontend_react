import React, {Component} from 'react';
import WorkTime from 'components/service/WorkTime';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as worktimeActions from 'store/modules/worktime';
import * as paginationActions from 'store/modules/pagination';
import locale from 'locale';

class WorkTimeContainer extends Component {

    componentDidMount() {
        const {WorkTimeActions, language} = this.props;
        document.title = locale.workTimeTitle[language];
        //WorkTimeActions.initialize(); // 에디터를 초기화합니다.

    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.workTimeTitle[nextProps.language];
        return true;
    }

    handleChangeInput = ({name, value}) => {
        const {WorkTimeActions} = this.props;
        WorkTimeActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {start, end, use, activePage, WorkTimeActions, PaginationActions} = this.props;
        const { token,language } = this.props;
        const worktime = {
            start,
            end,
            use
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await WorkTimeActions.writeWorkTime(worktime, token,language);
            await WorkTimeActions.getWorkTimeList(pageNumber, token);
            //WorkTimeActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('newStart', '00:00');
        this.handleState('newEnd', '00:00');
        this.handleState('newUse', 'true');
    }

    handleState(name, value){
        const { WorkTimeActions } = this.props;

        WorkTimeActions.changeInput({name, value});
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {start, end, use, language} = this.props;
        return (
            <WorkTime
                newStart={start}
                newEnd={end}
                newUse={use}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
                language={language}
            />
        );
    }
}

export default connect(
    (state) => ({
        start: state.worktime.get('newStart'),
        end: state.worktime.get('newEnd'),
        use: state.worktime.get('newUse'),
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(WorkTimeContainer);