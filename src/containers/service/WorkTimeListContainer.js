import React, { Component } from 'react';
import WorkTimeList from 'components/service/WorkTimeList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as worktimeActions from 'store/modules/worktime';


class WorkTimeListContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const {WorkTimeActions} = this.props;

        WorkTimeActions.changeInput({name, value});
    }

    getList = async() => {
        const { WorkTimeActions, activePage } = this.props;
        const { token } = this.props;

        try{
            await WorkTimeActions.getWorkTimeList(activePage, token);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    render() {
        const { tables, loading, changeView, language } =this.props;

        if(changeView !== 'table') return null;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <WorkTimeList language={language} tables={tables} handleChangeInput={this.handleChangeInput}/>
            </div>
        );
    }''
}

export default connect(
    (state) => ({
        tables: state.worktime.get('tables'),
        changeView: state.worktime.get('changeView'),
        no: state.worktime.get('no'),
        loading: state.pender.pending['worktime/WORK_TIME_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch)
    })
)(WorkTimeListContainer);