import React, { Component } from 'react';
import HoliDayList from 'components/service/HoliDayList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
//import * as listActions from 'store/modules/list';
import * as holidayActions from 'store/modules/holiday';

class HoliDayListContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const { HoliDayActions } = this.props;

        HoliDayActions.changeInput({name, value});
    }

    getList = async() => {
        const { HoliDayActions, activePage } = this.props;
        const { token } = this.props;

        try{
            await HoliDayActions.getHoliDayList(activePage, token);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.no != this.props.no) {
            this.handleGetModal(nextProps.no);
        }

        return true;
    }

    handleGetModal = async(no) => {
        const {HoliDayActions, token} = this.props;

        try{
            await HoliDayActions.getHoliDayData(no, token);
        }
        catch(e){
            console.log(e);
        }
    }

    render() {
        const { tables, loading, language, changeView } =this.props;

        if(changeView !== 'table') return null;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <HoliDayList language={language} tables={tables} handleChangeInput={this.handleChangeInput}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.holiday.get('tables'),
        changeView: state.holiday.get('changeView'),
        no: state.holiday.get('no'),
        loading: state.pender.pending['holiday/HOLI_DAY_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        HoliDayActions: bindActionCreators(holidayActions, dispatch)
    })
)(HoliDayListContainer);