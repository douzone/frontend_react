import React, {Component} from 'react';
import HoliDay from 'components/service/HoliDay';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as holidayActions from 'store/modules/holiday';
import * as paginationActions from 'store/modules/pagination';
import locale from 'locale';

class HoliDayContainer extends Component {

    componentDidMount() {
        const {HoliDayActions, language} = this.props;
        document.title = locale.holidayTitle[language];
        // HoliDayActions.initialize(); // 에디터를 초기화합니다.
    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.holidayTitle[nextProps.language];
        return true;
    }

    componenetWillMount() {
    }

    handleChangeInput = ({name, value}) => {
        const {HoliDayActions} = this.props;
        HoliDayActions.changeInput({name, value});
    }

    handleState(name, value){
        const { HoliDayActions } = this.props;

        HoliDayActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {day, use, description, activePage, HoliDayActions, PaginationActions,language} = this.props;
        const { token } = this.props;
        const holiday = {
            day,
            use,
            description
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await HoliDayActions.writeHoliDay(holiday, token,language);
            await HoliDayActions.getHoliDayList(pageNumber, token);
            // HoliDayActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('newDay', '');
        this.handleState('newDescription', '');
        this.handleState('newUse', 'true');
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {day, use, description, language} = this.props;

        return (
            <HoliDay
                day={day}
                use={use}
                description={description}
                language={language}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
            />
        );
    }
}

export default connect(
    (state) => ({
        day: state.holiday.get('newDay'),
        use: state.holiday.get('newUse'),
        description: state.holiday.get('newDescription'),
        activePage: state.pagination.get("activePage"),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        HoliDayActions: bindActionCreators(holidayActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(HoliDayContainer);