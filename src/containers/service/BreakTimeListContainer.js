import React, { Component } from 'react';
import BreakTimeList from 'components/service/BreakTimeList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as breaktimeActions from 'store/modules/breaktime';


class BreakTimeListContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const {BreakTimeActions} = this.props;

        BreakTimeActions.changeInput({name, value});
    }

    getList = async() => {
        const { BreakTimeActions, activePage } = this.props;
        const { token } = this.props;

        try{
            await BreakTimeActions.getBreakTimeList(activePage, token);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }

    render() {
        const { tables, loading, changeView, language } =this.props;

        if(changeView !== 'table') return null;
        if(loading && tables <= 0) return null;

        return (
            <div>
                <BreakTimeList language={language} tables={tables} handleChangeInput={this.handleChangeInput}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.breaktime.get('tables'),
        changeView: state.breaktime.get('changeView'),
        no: state.breaktime.get('no'),
        loading: state.pender.pending['breaktime/BREAK_TIME_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        BreakTimeActions: bindActionCreators(breaktimeActions, dispatch)
    })
)(BreakTimeListContainer);