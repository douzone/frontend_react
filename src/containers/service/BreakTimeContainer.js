import React, {Component} from 'react';
import BreakTime from 'components/service/BreakTime';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import * as breaktimeActions from 'store/modules/breaktime';
import * as paginationActions from 'store/modules/pagination';
import locale from 'locale';

class BreakTimeContainer extends Component {

    componentDidMount() {
        const {BreakTimeActions, language} = this.props;
        document.title = locale.breakTimeTitle[language];
        // BreakTimeActions.initialize(); // 에디터를 초기화합니다.
    }

    componenetWillMount() {
    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.breakTimeTitle[nextProps.language];

        return true;
    }

    handleChangeInput = ({name, value}) => {
        const {BreakTimeActions} = this.props;
        BreakTimeActions.changeInput({name, value});
    }

    handleState(name, value){
        const { BreakTimeActions } = this.props;

        BreakTimeActions.changeInput({name, value});
    }

    handleSubmit = async () => {

        const {start, end, use, description, activePage, BreakTimeActions, PaginationActions} = this.props;
        const { token,language } = this.props;
        const breaktime = {
            start,
            end,
            use,
            description
        }

        const pageNumber = 1;

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await BreakTimeActions.writeBreakTime(breaktime, token,language);
            await BreakTimeActions.getBreakTimeList(pageNumber, token);
            // BreakTimeActions.initialize(); // 에디터를 초기화합니다.
        } catch (e) {
            console.log(e);
        }

        this.handleState('newStart', '00:00');
        this.handleState('newEnd', '00:00');
        this.handleState('newDescription', '');
        this.handleState('newUse', 'true');
    }

    render() {
        const {handleChangeInput, handleSubmit} = this;
        const {start, end, use, description, language} = this.props;
        return (
            <BreakTime
                start={start}
                end={end}
                use={use}
                description={description}
                onChangeInput={handleChangeInput}
                onSubmit={handleSubmit}
                language={language}
            />
        );
    }
}

export default connect(
    (state) => ({
        start: state.breaktime.get('newStart'),
        end: state.breaktime.get('newEnd'),
        use: state.breaktime.get('newUse'),
        description: state.breaktime.get('newDescription'),
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language
    }),
    (dispatch) => ({
        BreakTimeActions: bindActionCreators(breaktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(BreakTimeContainer);