import React, {Component} from 'react';
import {connect} from 'react-redux';
import Title from 'components/service/Title';

class ServiceTitleContainer extends Component {
    render() {
        const { language } = this.props;
        return(
            <Title 
                language={language}
            />
        );
    }
}

export default connect(
    (state) => ({
        language: state.language.language,
        auth: state.login.auth
    })
)(ServiceTitleContainer)