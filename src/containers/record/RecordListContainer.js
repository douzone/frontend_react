import React, { Component } from 'react';
import RecordList from 'components/record/RecordList/RecordList';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recordActions from 'store/modules/record';
import * as paginationActions from 'store/modules/pagination';
import locale from 'locale';


class RecordListContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const {RecordActions} = this.props;

        RecordActions.changeInput({name, value});
    }

    getList = async() => {
        const { RecordActions, activePage, PaginationActions } = this.props;
        const { token, auth, loginUserNo, language } = this.props;

        //console.log(loginUserNo);
        try{
            if(auth === "ROLE_ADMIN") {
                await RecordActions.getRecordList(activePage, token);
            }
            else{
                await PaginationActions.getSearchRecordPage('', '', '', '', '', 'true', String(loginUserNo), '', token, language);
                await RecordActions.getSearchRecordList(activePage, '', '', '', '', '', 'true', String(loginUserNo), '', token, language);
            }
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getList();
    }
    
    componentDidMount() {
        const {language} = this.props;
        document.title = locale.activeRecordTitle[language];
    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.activeRecordTitle[nextProps.language];

        return true;
    }

    render(){
        const { tables, loading, badge, changeView, language, subSelect, subStartdate, subEnddate,
                subType, subRead, subContent, auth, loginUserName} = this.props;

        if(changeView !== 'table') return null;
        if(loading && tables <= 0) return null;
        
        return(
            <div>
                {
                    <RecordList language={language} tables={tables} badge={badge} handleChangeInput={this.handleChangeInput} loginUserName={loginUserName}
                                subSelect={subSelect} subStartdate={subStartdate} subEnddate={subEnddate} 
                                subType={subType} subRead={subRead} subContent={subContent} auth={auth}/>
                }
            </div>
        );
    }
}


export default connect(
    (state) => ({
        tables: state.record.get('tables'),
        changeView: state.record.get('changeView'),
        no: state.record.get('no'),
        badge: state.record.get('badge'),
        loading: state.pender.pending['record/RECORD_LIST'],
        activePage: state.pagination.get('activePage'),
        token: state.login.token,
        language: state.language.language,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        loginUserName: state.login.name,
        subStartdate: state.record.get('subStartdate'),
        subEnddate: state.record.get('subEnddate'),
        subSelect: state.record.get('subSelect'),
        subType: state.record.get('subType'),
        subRead: state.record.get('subRead'),
        subContent: state.record.get('subContent')
    }),
    (dispatch) => ({
        RecordActions: bindActionCreators(recordActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch)
    })
)(RecordListContainer);