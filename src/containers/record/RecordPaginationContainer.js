import React, { Component } from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import Pagination from 'components/list/Pagination';
import * as paginationActions from 'store/modules/pagination';
import * as recordActions from 'store/modules/record';


class RecordPaginationContainer extends Component {

    handlePageChange = (pageNumber) => {
        const { activePage, PaginationActions } = this.props;

        PaginationActions.pageChange({activePage, pageNumber});
        this.handleList(pageNumber);
    }

    handleList = async(pageNumber) => {
        const { RecordActions, startdate, enddate, name, content, recordType, select, no, read } = this.props;
        const { token, auth, loginUserNo, language } = this.props;

        let searchSelect;
        let searchNo;

        if(auth === "ROLE_USER") {
            searchSelect = true;
            searchNo = loginUserNo;
        }
        else{
            searchSelect = select;
            searchNo = no;
        }

        if(startdate === "" && enddate === "" && name === "" && content === "" && recordType === "전체" && read === "전체" && searchSelect === false) {
            try {
                //console.log("change page : " + pageNumber);
                //console.log("searchSelect : " + searchSelect);

                await RecordActions.getRecordList(pageNumber, token);
            } catch (e) {
                console.log(e);
            }
        }
        else {
            try{
                //console.log("startdate : " + startdate);
                //console.log("enddate : " + enddate);
                //console.log("name : " + name);
                //console.log("content : " + content);
                //console.log("recordType : " + recordType);
                //console.log("searchNo : " + searchNo);
                //console.log("read : " + read);
                //console.log("searchSelect : " + searchSelect);

                await RecordActions.getSearchRecordList(pageNumber, startdate, enddate, name, content, recordType, searchSelect, searchNo, read, token, language);
            } catch(e){
                console.log(e);
            }
        }
    }

    handlePage = async() => {
        const { PaginationActions } = this.props;
        const { token } = this.props;

        try{
            await PaginationActions.getRecordTotal(token);
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        const { auth } = this.props;

        if(auth === "ROLE_ADMIN"){
            this.handlePage();
        }
        else{
            this.handleList(1);
        }
    }

    componentDidMount() {
        const { PaginationActions } = this.props;

        PaginationActions.initialize();
    }

    render() {
        const {activePage, totalCount} = this.props;

        return(
            <div>
                <Pagination activePage={activePage} totalCount={totalCount} onChange={this.handlePageChange}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        activePage: state.pagination.get('activePage'),
        totalCount: state.pagination.get('totalCount'),
        startdate: state.record.get('startdate'),
        enddate: state.record.get('enddate'),
        name: state.record.get('name'),
        content: state.record.get('newContent'),
        select: state.record.get('select'),
        no: state.record.get('no'),
        read: state.record.get('newRead'),
        recordType: state.record.get('newRecordType'),
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        RecordActions: bindActionCreators(recordActions, dispatch)
    })
)(RecordPaginationContainer);