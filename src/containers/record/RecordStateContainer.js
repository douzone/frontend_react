import React, { Component } from 'react';
import RecordState from 'components/record/RecordState';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as stateActions from 'store/modules/state';

class RecordStateContainer extends Component {

    getStateList = async() => {
        const {StateActions, language} = this.props;
        const { token, auth, loginUserNo } = this.props;

        try {
            if(auth === "ROLE_ADMIN") {
                await StateActions.getRecordStateList(token);
            }
            else{
                await StateActions.getRecordSearchStateList(null, null, null, null, null, true, loginUserNo, null, token, language);
            }
        } catch(e){
            console.log(e);
        }
    }

    componentWillMount() {
        this.getStateList();
    }

    render() {
        const { tables, loading, language, loginUserName, select,
                subStartdate, subEnddate, subSelect, subType, subRead, subContent, auth } =this.props;

        if(loading) return null;
        return (
            <div>
                <RecordState language={language} tables={tables} loginUserName={loginUserName}
                            select={select} subSelect={subSelect} subStartdate={subStartdate} subEnddate={subEnddate} 
                            subType={subType} subRead={subRead} subContent={subContent} auth={auth}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        tables: state.state.get('tables'),
        loading: state.pender.pending['state/RECORD_STATE_LIST'],
        subStartdate: state.record.get('subStartdate'),
        subEnddate: state.record.get('subEnddate'),
        subSelect: state.record.get('subSelect'),
        subType: state.record.get('subType'),
        subRead: state.record.get('subRead'),
        subContent: state.record.get('subContent'),
        select: state.record.get('select'),
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language,
        loginUserName: state.login.name
    }),
    (dispatch) => ({
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(RecordStateContainer);