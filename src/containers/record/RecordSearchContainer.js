import React, {Component} from 'react';
import RecordSearch from 'components/record/RecordSearch/RecordSearch';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as recordActions from 'store/modules/record';
import * as paginationActions from 'store/modules/pagination';
import * as stateActions from 'store/modules/state';


class RecordSearchContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const {RecordActions} = this.props;

        RecordActions.changeInput({name, value});
    }

    handleState(name, value) {
        const {RecordActions} = this.props;

        RecordActions.changeInput({name, value});
    }

    handleSubmit = async(searchUserNo = '',select,userName) => {
        const {startdate, enddate, content, name, no, recordType, read, activePage, RecordActions, PaginationActions, StateActions} = this.props;
        const { token, auth, loginUserNo, language } = this.props;
        const pageNumber = 1;

        if(auth === "ROLE_USER"){
            select = true;
            searchUserNo = loginUserNo;
        }

        //console.log(searchUserNo);
        //console.log(select);
        //console.log(userName);
        //console.log(no);
        //console.log(name);
        //console.log(read);
        //console.log(language);

        PaginationActions.pageChange({activePage, pageNumber});
        try{
            if(startdate === "" && enddate === "" && userName === "" && content === "" && recordType === "전체" && read === '전체' && select === false){
                await PaginationActions.getRecordTotal(token);
            }
            else {
                await PaginationActions.getSearchRecordPage(startdate, enddate, userName, content, recordType, select, searchUserNo, read, token, language);
                RecordActions.changeInput({
                    name: "subStartdate", value: startdate
                });
                RecordActions.changeInput({
                    name: "subEnddate", value: enddate
                });
                RecordActions.changeInput({
                    name: "subType", value: recordType
                });
                RecordActions.changeInput({
                    name: "subRead", value: read
                });
                RecordActions.changeInput({
                    name: "subContent", value: content
                });
                RecordActions.changeInput({
                    name: "subSelect", value: true
                });
            }
            await StateActions.getRecordSearchStateList(startdate, enddate, userName, content, recordType, select, searchUserNo, read, token, language);
            await RecordActions.getSearchRecordList(pageNumber, startdate, enddate, userName, content, recordType, select, searchUserNo, read, token, language);
        } catch(e){
            console.log(e);
        }
    }

    shouldComponentUpdate(nextProps, nextState) {
        if(nextProps.no !== this.props.no){
            this.handleChangeInput({name: 'no', value: nextProps.no});
        }

        if(nextProps.select !== this.props.select){
            this.handleChangeInput({name: 'select', value: nextProps.select});
        }

        if(nextProps.name !== this.props.name){
            this.handleChangeInput({name: 'name', value: nextProps.name});
        }

        return true;
    }

    componentDidMount() {
        const {RecordActions} = this.props;

        RecordActions.initialize();
    }

    render(){
        const {startdate, enddate, name, content, recordType, token, read, select, no, auth, language} = this.props;

        return(
            <div>
                <RecordSearch startdate={startdate} enddate={enddate} name={name} content={content} recordType={recordType} read={read} select={select} no={no}
                              onSubmit={this.handleSubmit} onChangeInput={this.handleChangeInput} token={token} auth={auth} language={language}/>
            </div>
        );
    }
}

export default connect(
    (state) => ({
        startdate: state.record.get("startdate"),
        enddate: state.record.get("enddate"),
        name: state.record.get("name"),
        content: state.record.get("newContent"),
        recordType: state.record.get('newRecordType'),
        read: state.record.get('newRead'),
        select: state.record.get('select'),
        no: state.record.get('no'),
        activePage: state.pagination.get("activePage"),
        token: state.login.token,
        auth: state.login.auth,
        loginUserNo: state.login.no,
        language: state.language.language
    }),
    (dispatch) => ({
        RecordActions: bindActionCreators(recordActions, dispatch),
        PaginationActions: bindActionCreators(paginationActions, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
})
)(RecordSearchContainer);