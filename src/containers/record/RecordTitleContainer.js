import React, {Component} from 'react';
import {connect} from 'react-redux';
import Title from 'components/record/Title';

class RecordTitleContainer extends Component {
    render() {
        const { language } = this.props;
        
        return(
            <Title 
                language={language}
            />
        );
    }
}

export default connect(
    (state) => ({
        language: state.language.language
    })
)(RecordTitleContainer)