import React, { Component } from "react";
import Time from "components/time/Time";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as timeActions from "store/modules/commute";
import moment from "moment";

class TimeContainer extends Component {
  state = {
    timeset: ""
  };
  getTime = () => {
    
    const hour = moment().format("HH");
    const minute = moment().format("mm");
    const second = moment().format("ss");
    const timeset = `${hour > 9 ? hour : `0${hour}`}:${minute > 9 ? minute : `0${minute}`}:${second > 9 ? second : `0${second}`}`;
    this.setState({
      timeset
    });
  };
  componentDidMount = async () => {
    //   //setInterval(this.getTime, 1);
    //   const { TimeActions } = this.props;
      
    //   try {
    //     let { no, token,lasttime } = this.props;
    //     //console.log(no);
    //     await TimeActions.getTimeList(no, token);
    //   } catch (e) {
    //     console.log(e);
    //   }
    // };
    };
  render() {
    let {lasttime ,language} = this.props;
    return (
      <div>
        <Time time={lasttime} language = {language} />

      </div>
    );
  };
};
export default connect(
  state => ({
    time: state.time.get("time"),
    no: state.login.no,
    token: state.login.token,
    lasttime:state.commute.get("lasttime"),
    start : state.time.get("start"),
    end : state.time.get("end"),
    totalWorkTime : state.time.get("totalWorkTime"),
    starttime : state.time.get("starttime"),
    language: state.language.language,
  }),
  dispatch => ({
    TimeActions: bindActionCreators(timeActions, dispatch)
  })
)(TimeContainer);
