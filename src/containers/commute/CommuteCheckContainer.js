import React, { Component } from 'react';
import Check from 'components/commute/Check';
import {connect} from "react-redux";
import {bindActionCreators} from "redux";
import * as commuteActions from 'store/modules/commute';
import * as timeActions from "store/modules/commute";
import * as listActions from 'store/modules/list';
import * as getCalList from 'store/modules/commutecalendarlist';
import * as paginationList from 'store/modules/pagination';
import * as stateActions from 'store/modules/state';
import * as holidayActions from 'store/modules/holiday';
import moment from 'moment';
import locale from 'locale';

class CommuteCheckContainer extends Component {

    handleCommute = async() => {
        const {searchFromDate, searchToDate, activePage, ListActions, GetCalList, PaginationActions, searchState, changeView, StateActions} = this.props;
        const {token, loginUserNo, language} = this.props;

        const search = {
            searchFromDate,
            searchToDate,
            userName: "",
            searchUserNo: loginUserNo,
            searchState,
            select: true
        }

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try{
            await ListActions.getTableList(search, pageNumber, true, token, language);
            await GetCalList.getList(search, token, true);

            if (changeView === "table") {
                await StateActions.getCommuteSearchStateList(search, token);
                await PaginationActions.getSearchPage(search, token);
            }
            else{
                await StateActions.getCommuteCalendarStateList(searchFromDate, loginUserNo, "", searchState, true, token);
            }
        }
        catch(e){
            console.log(e);
        }
    }

    handleGoTo = async(preGoTo) => {
        const { CommuteActions } = this.props;
        const { token } = this.props;

        try{
            if(preGoTo) {
                await CommuteActions.goTo('출근', '출근 입력', token);
                
            }
            else{
                await CommuteActions.getPreGoTo('출근', '출근 입력', token);
            }

            this.handleCommute();
        } catch(e){
            console.log(e);
        }
    }

    handleGoOff = async(preGoOff) => {
        const { CommuteActions,TimeActions } = this.props;
        const { token,loginUserNo,totalWorkTime } = this.props;

        try{
            if(preGoOff) {
                await CommuteActions.goOff('퇴근', '퇴근 입력',totalWorkTime, token);

            }
            else{

                await CommuteActions.getPreGoOff('퇴근', '퇴근 입력',totalWorkTime, token);
            }

            this.handleCommute();
        } catch(e){
            console.log(e);
        }
    }

    handleState = async(preGoTo, preGoOff) => {
        const {token, CommuteActions, HoliDayActions} = this.props;

        try{
            await CommuteActions.getPreGoOffData(token);
            await CommuteActions.getPreGoToData(preGoTo, token);

            await HoliDayActions.getPreHoliDayData(token);

            await CommuteActions.getGoToData(true, token);
            await CommuteActions.getGoOffData(true, token);
        }
        catch(e){
            console.log(e);
        }
    }

    handleEndTime = async() => {
        const {CommuteActions, token} = this.props;

        try{
            await CommuteActions.getEndTime(token);
        }
        catch(e){
            console.log(e);
        }
    }

    convertState(name, value){
        const { CommuteActions } = this.props;

        CommuteActions.stateChange({name, value});
    }

    componentDidMount() {
        const {endTime, language} = this.props;
        document.title = locale.commuteCheckTitle[language];

        const {CommuteActions} = this.props;
        let cnt =0;
        // this.timerID = setInterval(() => {
        //     cnt++;
        //     CommuteActions.changeInput({name:'cnt', value:cnt});
        //     this.tick();
        // }, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    shouldComponentUpdate(nextProps, nextState) {
        document.title = locale.commuteCheckTitle[nextProps.language];
        
        if(nextProps.endTime !== this.props.endTime){
            this.convertState('endTime', nextProps.endTime);
        }

        if(nextProps.isGoOff !== this.props.isGoOff){
            this.convertState('isGoOff', nextProps.isGoOff);
        }

        return true;
    }

    componentWillMount() {
        this.handleState(this.props.preGoTo, this.props.preGoOff);
        this.handleEndTime();
    }

    render(){
        const { language, starttime, endtime, goto, gooff, isGoOff, loading, endTime, visible, time, isNight, preGoTo, preGoOff, preHoliDay } = this.props;

        if(loading) return null;

        return(
                <Check starttime={starttime} endtime={endtime} language = {language} isNight={isNight} time={time} visible={visible} endTime={endTime}
                       isGoOff={isGoOff} goto={goto} gooff={gooff} preGoTo={preGoTo} preGoOff={preGoOff} preHoliDay={preHoliDay}
                       onGoTo={this.handleGoTo} onGoOff={this.handleGoOff} onChangeInput={this.convertState.bind(this)} onHandleState={this.handleState}/>
        );
    }
}

export default connect(
    (state) => ({
        loginUserNo:state.login.no,
        goto: state.commute.get("goto"),
        gooff: state.commute.get("gooff"),
        preGoTo: state.commute.get("preGoTo"),
        preGoOff: state.commute.get("preGoOff"),
        preHoliDay: state.holiday.get("preHoliDay"),
        no: state.list.get('no'),
        isGoOff: state.commute.get('isGoOff'),
        isNight: state.commute.get('isNight'),
        endTime: state.commute.get('endTime'),
        time: state.commute.get('time'),
        cnt : state.commute.get('cnt'),
        loading: state.pender.pending['commute/GET_END_TIME'],
        token: state.login.token,
        language: state.language.language,
        starttime: state.time.get('starttime'),  
        endtime: state.time.get('endtime'),
        searchFromDate: state.list.get('searchFromDate'),
        searchToDate: state.list.get('searchToDate'),
        searchState: state.list.get('searchState'),
        changeView: state.list.get('changeView'),
        activePage: state.pagination.get('activePage'),
        totalWorkTime:state.commute.get('totalWorkTime')
    }),
    (dispatch) => ({
        CommuteActions: bindActionCreators(commuteActions, dispatch),
        TimeActions: bindActionCreators(timeActions, dispatch),
        ListActions: bindActionCreators(listActions, dispatch),
        GetCalList: bindActionCreators(getCalList, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch),
        HoliDayActions: bindActionCreators(holidayActions, dispatch)
    })
)(CommuteCheckContainer);