import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WorkAttitudeModal from 'components/modal/WorkAttitudeModal';
import * as workAttitudeActions from 'store/modules/workattitude';
import * as paginationList from 'store/modules/pagination';
import * as stateActions from 'store/modules/state';

class WorkAttitudeModalContainer extends Component {

    handleChangeInput = ({name, value}) => {
        
        const { WorkAttitudeActions } = this.props;
        WorkAttitudeActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.no !== this.props.no && nextProps.no){
            this.handleGetModal(nextProps.no);
        }

        if(nextProps.visible !== this.props.visible){
            this.handleGetModal(this.props.no);
        }

        return true;
    }

    handleGetModal = async (no) => {

        const { WorkAttitudeActions } = this.props;
        const { token, language} = this.props;
        try {
            await WorkAttitudeActions.getWorkAttitudeData(no, token, language);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {
        
        let { WorkAttitudeActions, name, title, titleEn, startDay, endDay, workAttitudeList, content, contentEn, no, userNo,
            PaginationActions, activePage, language,
            searchFromDate, searchToDate, searchUserNo, searchWorkAttitude, StateActions, searchUserName } = this.props;
        const { token } = this.props;

        if(language === "en"){
            title = titleEn;
            content = contentEn;
        }

        const edit = {
            no,
            name,
            title,
            startDay,
            endDay,
            workAttitudeList,
            content,
            userNo
        };

        const search = {
            searchWorkAttitude, searchFromDate, searchToDate, searchUserNo, searchUserName
        }

        //console.log(language);

        try {
            //console.log(edit);
            await WorkAttitudeActions.editWorkAttitudeData(edit, token, language);
            await PaginationActions.getWorkAttitudeTotal(search, token);
            await WorkAttitudeActions.getWorkAttitudeList(search, activePage, token, language);
            await StateActions.getWorkAttitudeStateList(token);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { no, WorkAttitudeActions, PaginationActions, language,
            searchFromDate, searchToDate, searchUserNo, searchWorkAttitude, searchUserName, activePage } = this.props;
        const { token } = this.props;
        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        const search = {
            searchWorkAttitude, searchFromDate, searchToDate, searchUserNo, searchUserName
        }

        try {
            await WorkAttitudeActions.removeWorkAttitudeData(no, token);
            await PaginationActions.getWorkAttitudeTotal(search, token);
            await WorkAttitudeActions.getWorkAttitudeList(search, pageNumber, token, language);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, name, title, titleEn, startDay, endDay, workAttitudeList, loading, editDisabled, content, contentEn, auth, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <WorkAttitudeModal visible={visible} name={name} title={title} titleEn={titleEn} workAttitudeList={workAttitudeList}
                        startDay={startDay} endDay={endDay} content={content} contentEn={contentEn} onChangeInput={handleChangeInput}
                        onSubmit={handleSubmit} editDisabled={editDisabled} onRemove={handleRemove} auth={auth} language={language} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.workattitude.get('modal'),
        searchWorkAttitude: state.workattitude.get('searchWorkAttitude'),
        searchFromDate: state.workattitude.get('searchFromDate'),
        searchToDate: state.workattitude.get('searchToDate'),
        searchUserNo: state.workattitude.get('searchUserNo'),
        searchUserName: state.workattitude.get('searchUserName'),
        no: state.workattitude.get('no'),
        userNo: state.workattitude.get('userNo'),
        name: state.workattitude.get('name'),
        title: state.workattitude.get('title'),
        titleEn: state.workattitude.get("titleEn"),
        startDay: state.workattitude.get('startDay'),
        endDay: state.workattitude.get('endDay'),
        content: state.workattitude.get('content'),
        contentEn: state.workattitude.get("contentEn"),
        workAttitudeList: state.workattitude.get('workAttitudeList'),
        editDisabled: state.workattitude.get('editDisabled'),
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['workattitude/WORK_ATTITUDE_DATA'],
        token: state.login.token,
        auth: state.login.auth,
        language: state.language.language
    }),
    (dispatch) => ({
        WorkAttitudeActions: bindActionCreators(workAttitudeActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(WorkAttitudeModalContainer);