import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CommuteModal from 'components/modal/CommuteModal';
import * as listActions from 'store/modules/list';
import * as paginationList from 'store/modules/pagination';
import * as stateActions from 'store/modules/state';

class CommuteModalContainer extends Component {

    startTimeChange = (e) => {
        const { value } = e.target;
        const { ListActions } = this.props;
        ListActions.changeStartTimeInput(value);
    }

    endTimeChange = (e) => {
        const { value } = e.target;
        const { ListActions } = this.props;
        ListActions.changeEndTimeInput(value);
    }

    handleChangeInput = ({name, value}) => {
        
        const { ListActions } = this.props;
        ListActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.startNo !== this.props.startNo && nextProps.startNo){

            this.handleGetModal(nextProps.startNo, nextProps.endNo);

        }

        if(nextProps.userNo !== this.props.userNo){
            this.handleChangeInput({name: 'userNo', value: nextProps.userNo});
        }
        
        return true;
    }

    handleGetModal = async (startNo, endNo) => {

        const { ListActions } = this.props;
        const { token } = this.props;
        try {
            await ListActions.getCommuteBetween({startNo, endNo}, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {
        
        const { ListActions, startdate, starttime, enddate, endtime, startCommute, endCommute,
            startNo, endNo, PaginationActions, activePage, userNo, groupNo,
            searchFromDate, searchToDate, searchUserNo, searchState, StateActions, userName, select, language } = this.props;
        const { token } = this.props;
        const edit = {
            startDate: startdate,
            startTime: starttime,
            endDate: enddate,
            endTime: endtime,
            startNo,
            endNo,
            startCommute,
            endCommute,
            userNo,
            groupNo
        };

/*
        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});
*/
        const search = {
            searchFromDate,
            searchToDate,
            searchUserNo,
            searchState,
            userName,
            select
        }

        try {
            //console.log(edit);
            await ListActions.editCommuteBetween(edit, token);
            await PaginationActions.getSearchPage(search, token);
            await ListActions.getTableList(search, activePage, select, token, language);
            await StateActions.getCommuteStateList(token);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { startNo, endNo, ListActions, PaginationActions, userNo, language,
            searchFromDate, searchToDate, searchUserNo, searchState, activePage, select, userName } = this.props;
        const { token } = this.props;
        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        const search = {
            searchFromDate,
            searchToDate,
            searchUserNo,
            searchState,
            userName,
            select
        }

        try {
            await ListActions.removeCommuteBetween(startNo, endNo, token);
            await PaginationActions.getSearchPage(search, token);
            await ListActions.getTableList(search, pageNumber, select, token, language);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, startdate, starttime, enddate, endtime, loading, editDisabled, auth, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <CommuteModal visible={visible} starttime={starttime} endtime={endtime} auth={auth} language={language}
                        startdate={startdate} enddate={enddate} onChangeInput={handleChangeInput}
                        onSubmit={handleSubmit} editDisabled={editDisabled} onRemove={handleRemove} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.list.get('modal'),
        startNo: state.list.get('startNo'),
        endNo: state.list.get('endNo'),
        startdate: state.list.get('startdate'),
        starttime: state.list.get('starttime'),
        enddate: state.list.get('enddate'),
        endtime: state.list.get('endtime'),
        startCommute: state.list.get('startCommute'),
        endCommute: state.list.get('endCommute'),
        searchFromDate: state.list.get('searchFromDate'),
        searchToDate: state.list.get('searchToDate'),
        searchState: state.list.get('searchState'),
        searchUserNo: state.list.get('searchUserNo'),
        userName: state.list.get('searchUserName'),
        editDisabled: state.list.get('editDisabled'),
        userNo: state.list.get('userNo'),
        groupNo: state.list.get('groupNo'),
        select: state.list.get('select'),
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_COMMUTE_BETWEEN'],
        token: state.login.token,
        auth: state.login.auth,
        language: state.language.language
    }),
    (dispatch) => ({
        ListActions: bindActionCreators(listActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
        StateActions: bindActionCreators(stateActions, dispatch)
    })
)(CommuteModalContainer);