import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CommuteModal from 'components/modal/CalendarCommuteModal';
import * as getCalList from 'store/modules/commutecalendarlist';


class CommuteModalContainer extends Component {


    handleChangeInput = ({name, value}) => {
        
        const { GetCalList } = this.props;
        GetCalList.changeInput({name, value});
    }

    handleGetModal = async (startNo, endNo) => {

        const { GetCalList } = this.props;
        const { token } = this.props;
        try {
            await GetCalList.getCommuteBetween({startNo, endNo}, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {
        
        const { GetCalList, fromDate, startTime, toDate, endTime, startCommute, endCommute,
            startNo, endNo, PaginationActions, activePage, userNo, groupNo,
            search,select,userName } = this.props;

    
     
            //console.log(search);
            //console.log(select);
        const { token } = this.props;
        const edit = {
            startDate: fromDate,
            startTime: startTime.substring(0,8),
            endDate: toDate,
            endTime: endTime.substring(0,8),
            startNo,
            endNo,
            startCommute,
            endCommute,
            userNo,
            groupNo
        };

        //console.log(edit);

        try {
          
             await GetCalList.editCommuteBetween(edit, token);

             if(search.searchUserNo==null)
             search.searchUserNo=0;
     
        if(!select){
            search.searchUserNo = userName;
             }
            
          await GetCalList.getList(search,token,select);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { startNo, endNo ,search,select,userName,GetCalList } = this.props;
        const { token } = this.props;
        const pageNumber = 1

  

        try {
            await GetCalList.removeCommuteBetween(startNo, endNo, token);
            if(search.searchUserNo==null)
             search.searchUserNo=0;
     
        if(!select){
            search.searchUserNo = userName;
             }
            
          await GetCalList.getList(search,token,select);

        } catch (e) {
            console.log(e);
        }
    }


    render() {
        const { visible, fromDate, startTime, toDate, endTime, loading, editDisabled ,auth, language} = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <CommuteModal visible={visible} starttime={startTime} endtime={endTime} auth={auth}
                        startdate={fromDate} enddate={toDate} onChangeInput={handleChangeInput} language={language}
                        onSubmit={handleSubmit} editDisabled={editDisabled} onRemove={handleRemove} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.commutecalendarlist.get('modal'),
        startNo: state.commutecalendarlist.get('startNo'),
        endNo: state.commutecalendarlist.get('endNo'),
        fromDate: state.commutecalendarlist.get('fromDate'),
        toDate: state.commutecalendarlist.get('toDate'),
        startTime: state.commutecalendarlist.get('startTime'),
        endTime: state.commutecalendarlist.get('endTime'),
        startCommute: state.commutecalendarlist.get('startCommute'),
        endCommute: state.commutecalendarlist.get('endCommute'),
        editDisabled: state.commutecalendarlist.get('editDisabled'),
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_COMMUTE_BETWEEN'],
        token: state.login.token,
        search: state.commutecalendarlist.get('search'),
        select:state.commutecalendarlist.get('select'),
        userName:state.commutecalendarlist.get('userName'),
        auth: state.login.auth,
        language: state.language.language
    }),
    (dispatch) => ({
        GetCalList: bindActionCreators(getCalList, dispatch)

    })
)(CommuteModalContainer);