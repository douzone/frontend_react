import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import RecordModal from 'components/modal/RecordModal';
import * as recordActions from 'store/modules/record';
import * as paginationList from 'store/modules/pagination';

class RecordModalContainer extends Component {

    handleChangeInput = ({name, value}) => {
        const { RecordActions } = this.props;

        RecordActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.no !== this.props.no ){
            this.handleGetModal(nextProps.no);
        }

        return true;
    }

    handleGetModal = async (no) => {
        const { RecordActions, token } = this.props;

        if(no === ''){
            no = 0;
        }

        try {
            await RecordActions.getRecordData(no, token);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, day, actor, id, recordType, recordTypeEn, content, contentEn, read, insertUserId, loading, language } = this.props;

        if(loading) return null;

        return (
            <RecordModal visible={visible} day={day} actor={actor} id={id} recordType={recordType} recordTypeEn={recordTypeEn} content={content} contentEn={contentEn}
                         read={read} insertUserId={insertUserId} language={language} onChangeInput={this.handleChangeInput}/>
        );
    }
}

export default connect(
    (state) => ({
        visible: state.record.get('modal'),
        no: state.record.get('no'),
        day: state.record.get('day'),
        actor: state.record.get('actor'),
        id: state.record.get('id'),
        recordType: state.record.get('recordType'),
        recordTypeEn: state.record.get('recordTypeEn'),
        content: state.record.get('content'),
        contentEn: state.record.get('contentEn'),
        read: state.record.get('read'),
        insertUserId: state.record.get('insertUserId'),
        token: state.login.token,
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_RECORD_DATA'],
        language: state.language.language
    }),
    (dispatch) => ({
        RecordActions: bindActionCreators(recordActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
    })
)(RecordModalContainer);