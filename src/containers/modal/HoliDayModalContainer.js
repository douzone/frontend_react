import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import HoliDayModal from 'components/modal/HoliDayModal';
import * as holidayActions from 'store/modules/holiday';
import * as paginationList from 'store/modules/pagination';


class HoliDayModalContainer extends Component {

    startTimeChange = (e) => {
        const { value } = e.target;
        const { HoliDayActions } = this.props;
        HoliDayActions.changeStartTimeInput(value);
    }

    endTimeChange = (e) => {
        const { value } = e.target;
        const { HoliDayActions } = this.props;
        HoliDayActions.changeEndTimeInput(value);
    }

    handleChangeInput = ({name, value}) => {

        const { HoliDayActions } = this.props;
        HoliDayActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.no !== this.props.no ){
            this.handleGetModal(nextProps.no);
        }

        if(nextProps.visible !== this.props.visible){
            this.handleGetModal(this.props.no);
        }

        return true;
    }

    handleGetModal = async (no) => {

        const { HoliDayActions, token } = this.props;
        try {
            await HoliDayActions.getHoliDayData(no, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {

        let { HoliDayActions, PaginationActions, activePage, day, use, description, descriptionEn, no, token,language } = this.props;

        /*
                const pageNumber = 1

                PaginationActions.pageChange({activePage, pageNumber});
        */

        if(language === "en"){
            description = descriptionEn;
        }

        try {
            await HoliDayActions.editHoliDayData(no, day, String(use), description, token,language);
            await PaginationActions.getHoliDayTotal(token);
            await HoliDayActions.getHoliDayList(activePage, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { HoliDayActions, PaginationActions, no, activePage, token,language } = this.props;

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await HoliDayActions.removeHoliDayData(no, token,language);
            await PaginationActions.getHoliDayTotal(token);
            await HoliDayActions.getHoliDayList(pageNumber, token);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, day, use, description, descriptionEn, loading, editDisabled, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <HoliDayModal visible={visible} day={day} use={use} description={description} descriptionEn={descriptionEn} editDisabled={editDisabled} language={language}
                            onChangeInput={handleChangeInput} onSubmit={handleSubmit} onRemove={handleRemove} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.holiday.get('modal'),
        no: state.holiday.get('no'),
        day: state.holiday.get('day'),
        use: state.holiday.get('use'),
        description: state.holiday.get('description'),
        descriptionEn: state.holiday.get('descriptionEn'),
        editDisabled: state.holiday.get('editDisabled'),
        token: state.login.token,
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_BREAK_TIME_DATA'],
        language: state.language.language
    }),
    (dispatch) => ({
        HoliDayActions: bindActionCreators(holidayActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
    })
)(HoliDayModalContainer);