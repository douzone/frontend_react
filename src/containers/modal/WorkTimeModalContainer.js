import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import WorkTimeModal from 'components/modal/WorkTimeModal';
import * as worktimeActions from 'store/modules/worktime';
import * as paginationList from 'store/modules/pagination';


class WorkTimeModalContainer extends Component {

    startTimeChange = (e) => {
        const { value } = e.target;
        const { WorkTimeActions } = this.props;
        WorkTimeActions.changeStartTimeInput(value);
    }

    endTimeChange = (e) => {
        const { value } = e.target;
        const { WorkTimeActions } = this.props;
        WorkTimeActions.changeEndTimeInput(value);
    }

    handleChangeInput = ({name, value}) => {

        const { WorkTimeActions } = this.props;
        WorkTimeActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.no !== this.props.no ){
            this.handleGetModal(nextProps.no);
        }

        return true;
    }

    handleGetModal = async (no) => {

        const { WorkTimeActions, token } = this.props;
        try {
            await WorkTimeActions.getWorkTimeData(no, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {

        const { WorkTimeActions, PaginationActions, activePage, start, end, use, no, token,language } = this.props;

        /*
                const pageNumber = 1

                PaginationActions.pageChange({activePage, pageNumber});
        */

        try {
            await WorkTimeActions.editWorkTimeData(no, start, end, use, token,language);
            await PaginationActions.getWorkTimeTotal(token);
            await WorkTimeActions.getWorkTimeList(activePage, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { WorkTimeActions, PaginationActions, no, activePage, token ,language} = this.props;

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await WorkTimeActions.removeWorkTimeData(no, token,language);
            await PaginationActions.getWorkTimeTotal(token);
            await WorkTimeActions.getWorkTimeList(pageNumber, token);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, start, end, use, loading, editDisabled, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <WorkTimeModal visible={visible} start={start} end={end} use={use} editDisabled={editDisabled} language={language}
                           onChangeInput={handleChangeInput} onSubmit={handleSubmit} onRemove={handleRemove} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.worktime.get('modal'),
        no: state.worktime.get('no'),
        start: state.worktime.get('start'),
        end: state.worktime.get('end'),
        use: state.worktime.get('use'),
        editDisabled: state.worktime.get('editDisabled'),
        token: state.login.token,
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_COMMUTE_BETWEEN'],
        language: state.language.language
    }),
    (dispatch) => ({
        WorkTimeActions: bindActionCreators(worktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
    })
)(WorkTimeModalContainer);