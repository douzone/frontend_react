import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import CalendarWorkAttitudeModal from 'components/modal/CalendarWorkAttitudeModal';
import * as workattitudecalendarlist from 'store/modules/workattitudecalendarlist';
import * as workAttitudeActions from 'store/modules/workattitude';

class CalendarWorkAttitudeModalContainer extends Component {

    handleChangeInput = ({name, value}) => {
        
        const { Workattitudecalendarlist } = this.props;
        Workattitudecalendarlist.changeInput({name, value});
    }


    handleSubmit = async () => {
      
        const {  name, title, startDay, endDay, workAttitudeList, content, no,search,select,userName,WorkAttitudeActions,Workattitudecalendarlist, language} = this.props;
        const { token } = this.props;
        const edit = {
            userNo: search.searchUserNo,
            no,
            name,
            title,
            startDay,
            endDay,
            workAttitudeList,
            content
        };

        try {

            await WorkAttitudeActions.editWorkAttitudeData(edit, token, language);

            if(search.searchUserNo==null)
            search.searchUserNo=0;
    
       if(!select){
           search.searchUserNo = userName;
            }
           
         await Workattitudecalendarlist.getList(search,token,select);
          
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { no ,WorkAttitudeActions,search,select,userName,Workattitudecalendarlist} = this.props;
        const { token } = this.props;


        try {
            await WorkAttitudeActions.removeWorkAttitudeData(no, token);

            if(search.searchUserNo==null)
            search.searchUserNo=0;
    
       if(!select){
           search.searchUserNo = userName;
            }
           
         await Workattitudecalendarlist.getList(search,token,select);

        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, name, title, startDay, endDay, workAttitudeList, loading, editDisabled, content, auth, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <CalendarWorkAttitudeModal visible={visible} name={name} title={title} workAttitudeList={workAttitudeList}
                        startDay={startDay} endDay={endDay} content={content} onChangeInput={handleChangeInput} language={language}
                        onSubmit={handleSubmit} editDisabled={editDisabled} onRemove={handleRemove} auth={auth} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.workattitudecalendarlist.get('modal'),
        editDisabled: state.workattitudecalendarlist.get('editDisabled'),
        loading: state.pender.pending['workattitude/WORK_ATTITUDE_DATA'],
        token: state.login.token,
        auth: state.login.auth,
        name:state.workattitudecalendarlist.get('name'),
        title:state.workattitudecalendarlist.get('title'),
        startDay:state.workattitudecalendarlist.get('startDay'),
        endDay:state.workattitudecalendarlist.get('endDay'),
        workAttitudeList:state.workattitudecalendarlist.get('workAttitudeList'),
        content:state.workattitudecalendarlist.get('content'),
        no:state.workattitudecalendarlist.get('no'),
        search:state.workattitudecalendarlist.get('search'),
        select:state.workattitudecalendarlist.get('select'),
        userName:state.workattitudecalendarlist.get('userName'),
        language: state.language.language
    }),
    (dispatch) => ({
        WorkAttitudeActions: bindActionCreators(workAttitudeActions, dispatch),
        Workattitudecalendarlist: bindActionCreators(workattitudecalendarlist, dispatch)

    })
)(CalendarWorkAttitudeModalContainer);