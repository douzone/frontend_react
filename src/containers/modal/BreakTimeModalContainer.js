import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BreakTimeModal from 'components/modal/BreakTimeModal';
import * as breaktimeActions from 'store/modules/breaktime';
import * as paginationList from 'store/modules/pagination';


class BreakTimeModalContainer extends Component {

    startTimeChange = (e) => {
        const { value } = e.target;
        const { BreakTimeActions } = this.props;
        BreakTimeActions.changeStartTimeInput(value);
    }

    endTimeChange = (e) => {
        const { value } = e.target;
        const { BreakTimeActions } = this.props;
        BreakTimeActions.changeEndTimeInput(value);
    }

    handleChangeInput = ({name, value}) => {

        const { BreakTimeActions } = this.props;
        BreakTimeActions.changeInput({name, value});
    }

    shouldComponentUpdate(nextProps, nextState) {

        if(nextProps.no !== this.props.no ){
            this.handleGetModal(nextProps.no);
        }

        if(nextProps.visible !== this.props.visible){
            this.handleGetModal(this.props.no);
        }

        return true;
    }

    handleGetModal = async (no) => {

        const { BreakTimeActions, token } = this.props;
        try {
            await BreakTimeActions.getBreakTimeData(no, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleSubmit = async () => {

        let { BreakTimeActions, PaginationActions, activePage, start, end, use, description, descriptionEn, no, token,language } = this.props;

        /*
                const pageNumber = 1

                PaginationActions.pageChange({activePage, pageNumber});
        */

        if(language === "en"){
            description = descriptionEn;
        }

        try {
            await BreakTimeActions.editBreakTimeData(no, start, end, String(use), description, token,language);
            await PaginationActions.getBreakTimeTotal(token);
            await BreakTimeActions.getBreakTimeList(activePage, token);
        } catch (e) {
            console.log(e);
        }
    }

    handleRemove = async () => {
        const { BreakTimeActions, PaginationActions, no, activePage, token,language } = this.props;

        const pageNumber = 1

        PaginationActions.pageChange({activePage, pageNumber});

        try {
            await BreakTimeActions.removeBreakTimeData(no, token,language);
            await PaginationActions.getBreakTimeTotal(token);
            await BreakTimeActions.getBreakTimeList(pageNumber, token);
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const { visible, start, end, use, description, descriptionEn, loading, editDisabled, language } = this.props;
        const { handleChangeInput, handleSubmit, handleRemove} = this;
        if(loading) return null;

        return (
            <BreakTimeModal visible={visible} start={start} end={end} use={use} description={description} descriptionEn={descriptionEn} editDisabled={editDisabled} language={language}
                           onChangeInput={handleChangeInput} onSubmit={handleSubmit} onRemove={handleRemove} />
        );
    }
}

export default connect(
    (state) => ({
        visible: state.breaktime.get('modal'),
        no: state.breaktime.get('no'),
        start: state.breaktime.get('start'),
        end: state.breaktime.get('end'),
        use: state.breaktime.get('use'),
        description: state.breaktime.get('description'),
        descriptionEn: state.breaktime.get('descriptionEn'),
        editDisabled: state.breaktime.get('editDisabled'),
        token: state.login.token,
        activePage: state.pagination.get('activePage'),
        loading: state.pender.pending['list/GET_BREAK_TIME_DATA'],
        language: state.language.language
    }),
    (dispatch) => ({
        BreakTimeActions: bindActionCreators(breaktimeActions, dispatch),
        PaginationActions: bindActionCreators(paginationList, dispatch),
    })
)(BreakTimeModalContainer);