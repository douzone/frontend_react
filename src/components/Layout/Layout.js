import React, { Component } from 'react';
import LayoutView from './LayoutView';
import { CircularProgress } from '@material-ui/core';
import { connect } from 'react-redux';
import { getUser } from 'pages/login/LoginState';
import { getNotice } from "components/Header/Notice/NoticeListState";

/**
* @author KJS
* @date 2019. 5. 14.
* @brief Login 후 Login User 정보 가져오기
*/

export class Layout extends Component {
    constructor(props) {
      super(props)

      this.state = {
        logging: false
      }

    }

    componentDidMount() {
        const { token, getUser, isAuthenticated } = this.props;
        if(isAuthenticated && token) {
          getUser(token);
        }
    }

      render() {
        const { isLogging, isLoading, auth } = this.props;
        const Lodingbar = (
          <div style={{position:'absolute', top:'50%', left:'50%'}}>
            <CircularProgress size={26}/>
          </div>
        )
          return (
              <div>
                  {
                    (isLogging === true) ?
                    <LayoutView auth={auth} /> :
                    Lodingbar
                  }
              </div>
          )
      }
}

export default connect(
    state => ({
      token: state.login.token,
      isLogging: state.login.isLogging,
      auth: state.login.auth,
      name: state.login.name,
      isAuthenticated: state.login.isAuthenticated,
      tables: state.notice.tables,
      isInit: state.notice.isInit
    }),
    { getUser, getNotice },
)(Layout);