import React from 'react';
import { withStyles, CssBaseline } from '@material-ui/core';
import { BrowserRouter, Route, Switch, Redirect } from 'react-router-dom';
import classnames from 'classnames';

import Header from '../Header';
import Sidebar from '../Sidebar';

// pages
import ListPage from 'pages/list';
import WorkTimePage from 'pages/worktime';
import BreaktimePage from 'pages/breaktime';
import HolidayPage from 'pages/holiday';
import RecordPage from 'pages/record';
import AddPage from 'pages/workAttitude';
import WorkAttitudePage from 'pages/workAttitudeList';
import AdminMainPage from 'pages/adminMain';
import Error from 'pages/error/Error';

/**
* @author KJS
* @date 2019. 5. 14.
* @brief 권한에 따른 Route
*/


const LayoutView = ({  classes, isSidebarOpened, toggleSidebar, ...props }) => {
  
  const ServiceRoute = (
    <div>
    <Route exact path="/app/service" render={() => <Redirect to="/app/service/worktime" />} />
    <Route path="/app/service/worktime" component={ WorkTimePage } />
    <Route path="/app/service/breaktime" component={ BreaktimePage } />
    <Route path="/app/service/holiday" component={ HolidayPage } />
    </div> 
  );

  return (
  <div className={classes.root}>
    <CssBaseline />
    <BrowserRouter>
      <React.Fragment>
        <Header />
        <Sidebar/>
        <div className={classnames(classes.content, { [classes.contentShift]: isSidebarOpened })}>
          <div className={classes.fakeToolbar} />
          <Switch>
            <Route exact path="/app/commute" render={() => <Redirect to="/app/commute/list" />} />
            <Route path="/app/commute/list" component={ ListPage } />
            <Route exact path="/app/workAttitude" render={() => <Redirect to="/app/workAttitude/list" />} />
            <Route path="/app/workAttitude/list" component={ WorkAttitudePage } />
            {
               props.auth === "ROLE_ADMIN" &&
              <Route path="/app/admin/main" component={ AdminMainPage } />
            }
            {
              props.auth === "ROLE_USER"  &&
              <Route path="/app/workAttitude/new" component={ AddPage } /> 
            }
            <Route path="/app/record" component={ RecordPage } />
            {
              props.auth === "ROLE_ADMIN" && 
              <Route exact path="/app/service" render={() => <Redirect to="/app/service/worktime" />} />
            }
            {
              props.auth === "ROLE_ADMIN" && 
              <Route path="/app/service/worktime" component={ WorkTimePage } />
            }
            {
              props.auth === "ROLE_ADMIN" && 
              <Route path="/app/service/breaktime" component={ BreaktimePage } />
            }
            {
              props.auth === "ROLE_ADMIN" && 
              <Route path="/app/service/holiday" component={ HolidayPage } />
            }
            <Route component={Error} />
          </Switch>
        </div>
      </React.Fragment>
    </BrowserRouter>
  </div>
  );
};

const styles = theme => ({
  root: {
    display: 'flex',
    maxWidth: '100vw',
    overflowX: 'hidden',
    backgroundColor: 'white'
  },
  content: { // 화면 넓이 설정
    //flexGrow: 1,
    padding: theme.spacing.unit * 5,
    width: `calc(100vw - 360px)`,
    minHeight: '100vh',
  },
  contentShift: {
    width: `calc(100vw - ${240 + theme.spacing.unit * 6}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  fakeToolbar: {
    ...theme.mixins.toolbar,
  }
});

export default withStyles(styles)(LayoutView);
