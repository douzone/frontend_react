import React, { Component } from 'react';
import locale from 'locale';
import { withStyles } from "@material-ui/core";
import { Typography } from "components/Wrappers";
import { Divider } from 'antd';
import {
    Home as HomeIcon
} from "@material-ui/icons";

class Title extends Component {
    render() {
        const { language, classes } = this.props;

        return(
            <div>
                <div className={classes.pageTitleContainer}>
                    <Typography variant="h2" size="sm">
                        <strong id="commuteTitle">
                            {locale.commuteList[language]}
                        </strong>
                    </Typography>
                    <Typography className={classes.dirTypo} variant="h6" size="sm">
                        <HomeIcon /> > {locale.commuteList[language]}
                    </Typography>
                </div>
            </div>
        )
    }
}

const styles = theme => ({
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        backgroundColor: '#efeaea',
        padding: '10px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(Title);