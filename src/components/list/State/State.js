import React, {Component} from 'react';
import {Grid} from '@material-ui/core';
import Widget from 'components/Widget';
import {PieChart, Pie, ResponsiveContainer, Sector} from "recharts";
import locale from 'locale';


class State extends Component {

    render() {
        const {tables, language} = this.props;

        let normal;
        let error;
        let late;
        let early;
        let etc;
        let PieChartData;

        //console.log(tables);

        if (tables.size === 0) {
            normal = 1;
            error = 0;
            late = 0;
            early = 0;
            etc = 0;

            PieChartData = [
                {name: 0, value: normal, color: "primary"},
                {name: 0, value: late, color: "secondary"},
                {name: 0, value: early, color: "warning"},
                {name: 0, value: error, color: "success"},
                {name: 0, value: etc, color: "success"}
            ];
        } else {
            normal = tables.get("정상");
            error = tables.get("에러");
            late = tables.get("지각");
            early = tables.get("조퇴");
            etc = tables.get("미등록");

            PieChartData = [
                {name: normal, value: normal, color: "primary"},
                {name: late, value: late, color: "secondary"},
                {name: early, value: early, color: "warning"},
                {name: error, value: error, color: "success"},
                {name: etc, value: etc, color: "success"}
            ];

            if (normal === 0 && error === 0 && late === 0 && early === 0 && etc === 0) {
                normal = 1;

                PieChartData = [
                    {name: 0, value: normal, color: "primary"},
                    {name: 0, value: late, color: "secondary"},
                    {name: 0, value: early, color: "warning"},
                    {name: 0, value: error, color: "success"},
                    {name: 0, value: etc, color: "success"}
                ];
            }
        }


        const renderActiveShape = (props) => {
            const {
                cx, cy, innerRadius, outerRadius, startAngle, endAngle,
                fill, payload
            } = props;

            return (
                <g>
                    <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };

        return (
            <React.Fragment>
                <br/>
                <Grid container spacing={32}>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Normal[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={0}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#28a745'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Lateness[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={1}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#ffc107'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Early_leave[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={2}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#6c757d'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Error[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={3}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#dc3545'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Non_enregistrement[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={4}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#007bff'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                </Grid>

                <br/>
            </React.Fragment>
        )
    }
}

const pieChartLegendWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
}

export default State;