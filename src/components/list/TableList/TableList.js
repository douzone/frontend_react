import React, {Component} from 'react';
import {Grid, Table, TableRow, TableHead, TableBody, TableCell} from '@material-ui/core';
import {Badge} from 'react-bootstrap';
import {Button} from 'antd';
import locale from 'locale';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import './TableList.css';

import { withStyles } from "@material-ui/core";

class TableList extends Component {

    handleShowModal = (table) => {
        //console.log(table);
        const {handleChangeInput} = this.props;

        handleChangeInput({name: 'startNo', value: table.startNo});
        handleChangeInput({name: 'endNo', value: table.endNo});
        handleChangeInput({name: 'userNo', value: table.userNo});

        handleChangeInput({name: 'modal', value: true});
    }

    render() {
        const {tables, language, classes} = this.props;
        const {handleShowModal} = this;

        const TableList = tables.map(
            (table, index) => {
                const {name, userNo, day, dayEn, totalWorktime, fromState, fromStateEn, toState, toStateEn, startTime, endTime} = table.toJS();
                let {fromBadge, toBadge, lDay, lFromState, lToState} = '';

                if ('en' === language) {
                    lDay = dayEn;
                    lFromState = fromStateEn;
                    lToState = toStateEn;
                } else if ('ko' === language) {
                    lDay = day;
                    lFromState = fromState;
                    lToState = toState;
                }

                if (lFromState === locale.Normal[language]) fromBadge = "success";
                else if (lFromState === locale.Lateness[language]) fromBadge = "warning";
                else if (lFromState === locale.Error[language]) fromBadge = "danger";
                else if (lFromState === locale.Non_enregistrement[language]) fromBadge = "primary";

                if (lToState === locale.Normal[language]) toBadge = "success";
                else if (lToState === locale.Early_leave[language]) toBadge = "secondary";
                else if (lToState === locale.Error[language]) toBadge = "danger";
                else if (lToState === locale.Non_enregistrement[language]) toBadge = "primary";

                return (
                    <TableRow className={classes.tableBody} hover key={index} onClick={event => handleShowModal(table.toJS())}>
                        <TableCell id="userNo" className="pl-3 fw-normal">{userNo}</TableCell>
                        <TableCell id="name">{name}</TableCell>
                        <TableCell>{lDay}</TableCell>
                        <TableCell>{startTime}</TableCell>
                        <TableCell>{endTime}</TableCell>
                        <TableCell>{totalWorktime}</TableCell>
                        <TableCell><Badge variant={fromBadge}>{lFromState}</Badge> <Badge
                            variant={toBadge}>{lToState}</Badge></TableCell>
                    </TableRow>
                )
            }
        );

        return (
            <React.Fragment>
                <div style={divWrapper}>
                    <Button type="link" shape="round" icon="download" size="large">
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="css"
                            table="commuteTable"
                            filename="commuteTableXLS"
                            sheet="commuteTableXLS"
                            buttonText="Download"
                        />
                    </Button>
                </div>
                <Grid item xs={12} style={{marginTop: '10px'}}>
                    <div>
                        <Table id="commuteTable" className="mb-0">
                            <TableHead>
                                <TableRow className={classes.tableHead}>
                                    <TableCell><b>User No</b></TableCell>
                                    <TableCell><b>{locale.Name[language]}</b></TableCell>
                                    <TableCell><b>{locale.Day[language]}</b></TableCell>
                                    <TableCell><b>{locale.CheckInTime[language]}</b></TableCell>
                                    <TableCell><b>{locale.CheckOutTime[language]}</b></TableCell>
                                    <TableCell><b>{locale.TotalWorkTime[language]}</b></TableCell>
                                    <TableCell><b>{locale.State[language]}</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody id="commuteRecord">
                                {TableList}
                            </TableBody>
                        </Table>
                    </div>
                </Grid>
            </React.Fragment>
        )
    }
}

const divWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end",
}

const styles = theme => ({
    tableHead: {
        backgroundColor: '#fafafa',
        borderTop: '2px solid rgba(224, 224, 224, .5)',
        height: theme.spacing.unit * 6,
        "& th" : {
            borderBottom: '2px solid rgba(224, 224, 224, .5)',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            width: '20%'
        },
        "& th:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none',
            width: '10%'
        },
        "& th:nth-child(2)" : {
            width: '10%'
        },
        "& th:nth-child(3)" : {
            width: '5%'
        },
        "& th:nth-child(7)" : {
            width: '15%'
        }
    },
    tableBody: {
        padding: '0px 0px 0px 0px',
        height: theme.spacing.unit * 5,
        "& td" : {
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
        },
        "& td:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        }
    },
    tdContent: {
        textAlign:'left'
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(TableList);
