import React, {Component} from 'react';
import {Grid} from '@material-ui/core';
import Widget from 'components/Widget';
import {PieChart, Pie, ResponsiveContainer, Sector} from "recharts";
import locale from 'locale';


class CalendarState extends Component {

    render() {
        const {calendarTables, language} = this.props;

        //console.log(calendarTables);

        let total;
        let etc;
        let normal;
        let PieChartData = [];

        if (calendarTables.size === 0) {
            total = 1;
            etc = 0;
            normal = 0;

            PieChartData = [
                {name: 0, value: total, color: "primary"},
                {name: 0, value: etc, color: "secondary"},
                {name: 0, value: normal, color: "warning"}
            ];
        } else {
            total = calendarTables.get("총 근로일");
            etc = calendarTables.get("미처리");
            normal = calendarTables.get("정상 근로일");

            if (total === 0 && etc === 0 && normal === 0) {
                total = 1;
                //console.log("aa");

                PieChartData = [
                    {name: 0, value: total, color: "primary"},
                    {name: 0, value: etc, color: "secondary"},
                    {name: 0, value: normal, color: "warning"}
                ];
            } else {
                PieChartData = [
                    {name: total, value: total, color: "primary"},
                    {name: etc, value: etc, color: "secondary"},
                    {name: normal, value: normal, color: "warning"}
                ];
            }
        }

        const renderActiveShape = (props) => {
            const {
                cx, cy, innerRadius, outerRadius, startAngle, endAngle,
                fill, payload
            } = props;

            return (
                <g>
                    <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };

        return (
            <React.Fragment>

                <br/>

                <Grid container spacing={32}>
                    <Grid item lg={4} md={5} sm={6} xs={12}>
                        <Widget title={locale.TotalWorkDay[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144} margin={0}>
                                                <Pie
                                                    activeIndex={0}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#6c757d'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item lg={4} md={8} sm={6} xs={12}>
                        <Widget title={locale.Non_process[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144} margin={0}>
                                                <Pie
                                                    activeIndex={1}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#007bff'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item lg={4} md={8} sm={6} xs={12}>
                        <Widget title={locale.NormalWorkDay[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144} margin={0}>
                                                <Pie
                                                    activeIndex={2}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#28a745'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                </Grid>

                <br/>

            </React.Fragment>
        )
    }
}

const pieChartLegendWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
}

export default CalendarState;