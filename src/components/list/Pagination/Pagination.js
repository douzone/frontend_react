import React, {Component} from 'react';
import {Pagination} from "antd";
import "antd/dist/antd.css";

class ListPagination extends Component {

    handlePageChange = (pageNumber) => {
        const {onChange} = this.props;

        onChange(pageNumber);
    }

    render(){
        const {handlePageChange} = this;
        let {activePage, totalCount} = this.props;

        //console.log("현재 activePage : " + activePage);
        //console.log("현재 totalCount : " + totalCount);

        if(totalCount === 0) totalCount = 1;

        return(
            <div style={{display: 'flex',  justifyContent:'center', alignItems:'center', marginTop:'20px'}}>
                <Pagination defaultCurrent={activePage} current={activePage} total={totalCount} onChange={handlePageChange} pageSize={30}/>
            </div>
        );
    }
}

export default ListPagination;