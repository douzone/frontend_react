import React, { Component } from "react";
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';
import './Calendar.css';
import Modal from 'react-awesome-modal';
import CalendarCommuteModal from 'components/modal/CalendarCommuteModal';
import locale from 'locale';

// axios.defaults.headers.common['Authorization'] = 
//                                 'Bearer ' + localStorage.getItem('jwtToken');

// axios.defaults.headers.common['Authorization'] = 
// 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTU2NTAyNDE2LCJpYXQiOjE1NTU4OTc2MTZ9.hK9ujazIT2XpBxLn_kL_gzPEfssICpOurzI8I43D-zkTge0g7MwIReZ_B9y1jNe94gqw71OTnYOliT3AN2ZRbA';
class Calendar extends Component {

    constructor(props) {
      

        super(props);
        
        this.state = {
            date:new Date(),
            events:[],
            visible : false,
            modal:{
                startTime:"",
                endTime:"",
                startDate:"",
                endDate:""
            }
        }
    }
    openModal(info) {
        //console.log(info);
        const{handleChangeInput, auth} = this.props;
        
        if(auth === "ROLE_USER") {
            this.updateRead(info.startNo);
        }
        
        handleChangeInput({name:"modal",value:true})
        handleChangeInput({name:"toDate",value:info.data.endDate})
        handleChangeInput({name:"endTime",value:info.data.endTime})
        handleChangeInput({name:"fromDate",value:info.data.startDate})
        handleChangeInput({name:"startTime",value:info.data.startTime})
        handleChangeInput({name:"startNo",value:info.startNo})
        handleChangeInput({name:"endNo",value:info.endNo})
        handleChangeInput({name:"userNo",value:info.userNo})
  
    }

    // (list, calendar)리스트 클릭시 updateRead액션 발생
    // MongoDB의 key_no와 startNo가 일치하면 read true로 update
    updateRead = (no) => {
        const {token, auth, ListActions } = this.props;
        const recordType = "출퇴근"
        ListActions.updateRead(no, auth, recordType, token);
    }

    closeModal() {
        this.setState({
            visible : false
        });
    }

    render() {
        let ary=[];
       const{calList,search,language} = this.props;
       const text = '';
       if(calList!=null){
          // //console.log(calList);
        ary= calList.data.map((items,index)=>{
           // //console.log(items);
           // if(items.state=="지각조퇴")
           let color ='#DC3545';
               if(items.state === "지각")
               color = '#FFC107';
               else if(items.state === "정상")
               color ='#28A745';
               else if(items.state === "조퇴")
               color ='#999';
               else if(items.state === "지각조퇴")
               color ='#999';
               else if(items.state==null){
                   color='#007BFF';
               }
            let start=items.startDate.split(" ");
            let end;
            let setTitle;
            if(items.endDate!=null){
             end =items.endDate.split(" ");
             let setTitleState=""; 
             if(language === "ko")
             {
                 if(items.state!=null)
                setTitleState=items.state;
                else
                setTitleState="미등록";
             }
             else if(language === "en")
             {
                if(items.state === "지각")
                setTitleState="Lateness"
                else if(items.state === "정상")
                setTitleState="Normal"
                else if(items.state === "조퇴")
                setTitleState="Early leave"
                else if(items.state === "지각조퇴")
                setTitleState="Lateness & Early leave"
                else
                setTitleState="Non-enregistrement"
                
             }
             setTitle = items.startDate.substring(10,16)+"~"+items.endDate.substring(10,16)+"      "+setTitleState;
             return {
                title:setTitle,
                start:items.startDate,
                end:items.endDate,
                userNo:items.userNo,
                startNo:items.startNo,
                endNo:items.endNo,
                backgroundColor:color,
                time:items.startDate,
                data:{
                     startTime:start[1],
                     endTime:end[1],
                     startDate:start[0],
                     endDate:end[0]

                }
            }
            }
            else{
             end=null;
             let setTitleState=""; 
             if(language === "ko")
             {
                setTitleState="~ 퇴근 기록 없음";
             }
             else if(language === "en")
             {
                setTitleState="~ No record for leaving work "
                
             }
             setTitle=items.startDate.substring(10,16)+setTitleState
             return {
                title:setTitle,
                start:items.startDate,
                end:items.startDate,
                userNo:items.userNo,
                startNo:items.startNo,
                endNo:items.endNo,
                backgroundColor:color,
                time:items.startDate,
                data:{
                     startTime:start[1],
                     startDate:start[0]
                     

                }
            }
            }
              

            // return {
            //     title:setTitle,
            //     start:items.startDate,
            //     end:items.endDate,
            //     userNo:items.userNo,
            //     startNo:items.startNo,
            //     endNo:items.endNo,
            //     backgroundColor:color,
            //     data:{
            //          startTime:start[1],
            //          endTime:end[1],
            //          startDate:start[0],
            //          endDate:end[0]

            //     }
            // }
        //     let color ='#DC3545';
        //     if(items.state=="지각")
        //     color = '#FFC107';
        //     else if(items.state=="정상")
        //     color ='#28A745';
        //     else if(items.state=="조퇴")
        //     color ='#999';
            
        // return {title:items.state+" "+items.commute, start: items.day,user_no:items.user_no,commute_no:items.commute_no,name:items.name,id:items.id,commute:items.commute,time:items.day,backgroundColor:color}
    });
    }
        return (
            <div id="full-calendar">
                <FullCalendar
                    id = "your-custom-ID"
                    header = {{
                        left: 'none',
                        center: 'title',
                        right: 'none'
                    }}
                    defaultDate={search.searchFromDate === ""?this.state.date:search.searchFromDate}
                   
                    navLinks= {true} // can click day/week names to navigate views
                    editable= {false}
                    eventLimit= {true} // allow "more" link when too many events
                    titleFormat={'YYYY-MM'}
                    events = {ary}
                    timeFormat = {"HH:mm"}
                    eventClick={ (info)=>{

                        return this.openModal(info)
                        
                    }
                    }
                    droppable={false}
                    displayEventTime={false}
                    locale={language}
                />
               {/* <Modal visible={this.state.visible} width="300" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div className = "Modal"><br/>
                        <h1>{this.state.modal.title}</h1>
                        <br/>
                        <p>이름: {this.state.modal.name}</p>
                        <p>아이디: {this.state.modal.id}</p>
                        <p>시간 : {this.state.modal.time}</p>
                        <a href=" " onClick={() => this.closeModal()}>수정하기</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href=" " onClick={() => this.closeModal()}>닫기</a>
                    </div>
                </Modal> */}
                  {/* <CalendarCommuteModal visible={this.state.visible} starttime={this.state.modal.startTime} endtime={this.state.modal.endTime}
                        startdate={this.state.modal.startDate} enddate={this.state.modal.endDate} />  */}
            </div>
        );
    }

}




export default Calendar;