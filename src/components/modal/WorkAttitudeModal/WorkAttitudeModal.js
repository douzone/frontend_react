import React, { Component } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import { Alert, Input, Divider } from 'antd';
import moment from 'moment';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';

const { TextArea } = Input;

class WorkAttitudeModal extends Component {

    constructor(props, context) {
        super(props, context);
    
        this.state = {
          show: true,
          message: ''
        };
        this.startDayChange = this.startDayChange.bind(this);
        this.endDayChange = this.endDayChange.bind(this);
    }

    startDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "startDay";
        onChangeInput({name, value});
    }

    endDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "endDay";
        onChangeInput({name, value});
    }

    showModal = () => {
        //this.props.onChangeInput({name:'modal', value:true});
    }
    
    handleOk = (e) => {
        const { onSubmit } = this.props;
        onSubmit();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleEdit = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:false});
    }

    handleRemove = (e) => {
        const { onRemove } = this.props;
        onRemove();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }
    
    handleCancel = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:true});
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'no', value:0});
        this.setState({ show : true,
                        message : ''});
    }

    handleChange = (e) => {
        const { onChangeInput, language } = this.props;
        const { value, name } = e.target;

        if(language === "en"){
            onChangeInput({name: 'titleEn', value: value})
        }
        else {
            onChangeInput({name, value});
        }
    }

    handleTextArea = (e) => {
        const {onChangeInput, language} = this.props;
        const {value, name} = e.target;

        if(language === "en"){
            onChangeInput({name: 'contentEn', value: value});
        }
        else {
            onChangeInput({name, value});
        }
    }

    submitCheck = (e) => {
        
        const { startDay, endDay, title, content,language } = this.props;
        const { handleOk } = this;

        if(startDay <= 0 || endDay <= 0){

            this.setState({ show : false,
                message : locale.Empty_Period[language]});
            return;
        }

        if(startDay > endDay){
            this.setState({ show : false,
                message : locale.Time_Longer[language]});
            return;
        }

        if(title <= 0){
            this.setState({ show : false,
                message : locale.Empty_Subject[language]});
            return;
        }

        if(content <= 0){
            this.setState({ show : false,
                message : locale.Empty_Present[language]});
            return;
        }

        handleOk(e);

    }

    render() {
        let { visible, name, title, titleEn, startDay, endDay, workAttitudeList, content, contentEn, editDisabled, auth, language } = this.props;
        const { handleChange, handleEdit, submitCheck, handleCancel, handleRemove, handleTextArea,
                startDayChange, endDayChange } = this;

        if(language === "en"){
            title = titleEn;
            content = contentEn;
        }

        return (
            <div>
                <Modal show={visible} centered={true} size="lg">
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title id="modal1" style={{color: '#ffffff'}}>
                            {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                    </Modal.Body>
                </Modal>


                <Modal show={visible} centered={true} size="lg">
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title id="modal" style={{color: '#ffffff'}}>
                            {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.applicant[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={handleChange}
                                disabled={true}
                                name="name"
                            />
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.Title[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <Form.Control type="text" 
                                          onChange={handleChange} 
                                          value={title}
                                          disabled={editDisabled}
                                          name="title"/>
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.workAttitudePeriod[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <DatePickerInput
                                title={locale.Start_date[language]}
                                selected={startDay}
                                value={startDay}
                                onChange={startDayChange}
                                displayFormat='YYYY-MM-DD'
                                returnFormat='YYYY-MM-DD'
                                //defaultValue={this.state.yesterday}
                                locale={language}
                                readOnly={true}
                                disabled={editDisabled}
                            />
                        </Col>
                        ~
                        <Col xs={4}>
                            <DatePickerInput
                                title={locale.Start_date[language]}
                                selected={endDay}
                                value={endDay}
                                onChange={endDayChange}
                                displayFormat='YYYY-MM-DD'
                                returnFormat='YYYY-MM-DD'
                                //defaultValue={this.state.yesterday}
                                locale={language}
                                readOnly={true}
                                disabled={editDisabled}
                            />
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2}>
                            {locale.WorkAttitudeItem[language]} :{" "}
                        </Col>
                        <Col xs={2}>
                            <select name="workAttitudeList" type="text" disabled={editDisabled} value={workAttitudeList} onChange={handleChange}>
                                <option value="출장">{locale.Business_trip[language]}</option>
                                <option value="외근">{locale.Work_outside[language]}</option>
                                <option value="연차">{locale.Annual_leave[language]}</option>
                                <option value="오전반차">{locale.morningSemiAnnualLeave[language]}</option>
                                <option value="오후반차">{locale.afternoonSemiAnnualLeave[language]}</option>
                                <option value="교육">{locale.Education[language]}</option>
                            </select>
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2}>
                            {locale.WorkAttitudeReason[language]} :{" "}
                        </Col>
                        <Col xs={10}>
                            <TextArea name='content' rows={4} disabled={editDisabled} onChange={handleTextArea} value={content} />
                        </Col>
                    </Row>
                    <div hidden={this.state.show}>
                        <Alert message={this.state.message} type="error" />
                    </div>
                    </Modal.Body>
                    {/* 화면 width 50% 이하일 경우 버튼 깨짐*/}
                    <Modal.Footer>
                        <div>
                            <Button id="btnClose" variant="secondary" onClick={handleCancel} style={{width: 72}}>
                                {locale.close[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button id="btnDelete" hidden={editDisabled === true ? false : true}  variant="danger" onClick={handleRemove} style={{width: 72}}>
                                {locale.delete[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button id="btnModify" variant="primary" onClick={editDisabled === true ? handleEdit : submitCheck} style={{width: 128}}>
                                {editDisabled === true ? locale.modification[language] : locale.modify[language] }
                            </Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default WorkAttitudeModal;