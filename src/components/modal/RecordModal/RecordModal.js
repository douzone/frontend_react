import React, { Component } from 'react';
import {Modal, Button, Badge} from 'react-bootstrap';
import {Table, TableRow, TableHead, TableBody, TableCell} from '@material-ui/core';
import locale from 'locale';

class RecordModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            show: true,
            message: ''
        };
    }

    handleCancel = (e) => {
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name: 'no', value: 0});

        this.setState({ show : true, message: '' });
    }

    render() {
        let { visible, day, actor, id, recordType, recordTypeEn, content, contentEn, read, insertUserId, language } = this.props;
        let badge = '';

        if(language === "en"){
            recordType = recordTypeEn;
            content = contentEn;
        }

        if(read === true) badge = 'success';
        else badge = 'danger';

        return (
            <div>
                <Modal show={visible} centered={true}>
                    <Modal.Header closeButton={false}>
                        <Modal.Title>{locale.detailView[language]}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <div>
                            <Table className="mb-0">
                                <TableHead>
                                </TableHead>
                                <TableBody>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.Date[language]}</b></TableCell>
                                        <TableCell>{day}</TableCell>
                                    </TableRow>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.ActiveAgent[language]}</b></TableCell>
                                        <TableCell id="actor">{actor}</TableCell>
                                    </TableRow>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.Id[language]}</b></TableCell>
                                        <TableCell id="id">{insertUserId}</TableCell>
                                    </TableRow>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.Type[language]}</b></TableCell>
                                        <TableCell>{recordType}</TableCell>
                                    </TableRow>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.Contents[language]}</b></TableCell>
                                        <TableCell>{content}</TableCell>
                                    </TableRow>
                                    <TableRow hover>
                                        <TableCell className="pl-3 fw-normal"><b>{locale.ReadCheck[language]}</b></TableCell>
                                        <TableCell><h6><Badge variant={badge}>{String(read)}</Badge></h6></TableCell>
                                    </TableRow>
                                </TableBody>
                            </Table>
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button id="btnClose" variant="secondary" onClick={this.handleCancel}>
                            {locale.close[language]}
                        </Button>
                    </Modal.Footer>
                </Modal>
           </div>
        );
    }
}

export default RecordModal;