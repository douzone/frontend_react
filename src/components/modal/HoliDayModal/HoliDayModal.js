import React, { Component } from 'react';
import { Modal, Button, Row, Col } from 'react-bootstrap';
import {Menu, Dropdown, Button as Btn, Icon, Alert, Input, Divider} from 'antd';
import moment from 'moment';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';


class HoliDayModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            show: true,
            message: ''
        };
        this.dayChange = this.dayChange.bind(this);
    }

    dayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "day";
        onChangeInput({name, value});
    }

    showModal = () => {
        //this.props.onChangeInput({name:'modal', value:true});
    }

    handleOk = (e) => {
        const { onSubmit } = this.props;

        onSubmit();

        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleEdit = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:false});
    }

    handleRemove = (e) => {
        const { onRemove, use, language } = this.props;

        if(String(use) === 'true'){
            this.setState({ show : false,
                message : '사용하고 있는 휴일입니다.'});

            if(language === "en"){
                this.setState({
                    message: 'It\'s a holiday you\'re using'
                });
            }

            return;
        }

        onRemove();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleCancel = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:true});
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name: 'no', value: 0});
        this.setState({ show : true, message: '' });
    }

    startHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'start';
        onChangeInput({name, value});
    }

    endHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'end';
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const name = 'use';
        const value = e.key;

        onChangeInput({name, value});
    }

    DateChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }

    textAreaChange = (e) => {
        const { onChangeInput, language } = this.props;
        const { value, name } = e.target;

        if(language === "en"){
            onChangeInput({name: 'descriptionEn', value: value});
        }
        else {
            onChangeInput({name, value});
        }
    }

    submitCheck = (e) => {
        const {day, description, descriptionEn, language} = this.props;
        const { handleOk } = this;

        if(day >= 0) {
            this.setState({ show : false,
                message : '휴일 날짜가 없습니다.'});

            if(language === "en"){
                this.setState({
                    message: 'There is no holiday date'
                });
            }

            return;
        }

        if(language === "en"){
            if(descriptionEn === ''){
                this.setState({
                    show:false,
                    message: 'No holiday description'
                });

                return;
            }
        }
        else {
            if (description === '') {
                this.setState({
                    show: false,
                    message: '휴일 설명이 없습니다.'
                });

                return;
            }
        }

        handleOk(e);
    }

    render() {
        let { visible, day, use, description, descriptionEn, editDisabled, language } = this.props;
        const {TextArea} = Input;

        const menu = (
            <Menu onClick={this.handleChange}>
                <Menu.Item key="true">true</Menu.Item>
                <Menu.Item key="false">false</Menu.Item>
            </Menu>
        );

        if(language === "en"){
            description = descriptionEn;
        }

        return (
            <div>
                <Modal show={visible} centered={true} >
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title style={{color: '#ffffff'}}>
                            {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                        <Row>
                            <Col xs={8}>
                                <label>
                                    {locale.holidayDate[language]}
                                </label>
                            </Col>
                            <Col xs={4}>
                                <label>
                                    {locale.use[language]}
                                </label>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={8}>
                                <DatePickerInput
                                    title={locale.holidayDate[language]}
                                    selected={day}
                                    value={day}
                                    onChange={this.dayChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    //defaultValue={this.state.yesterday}
                                    locale={language}
                                    readOnly={true}
                                    disabled={editDisabled}
                                    id="dayHoliDayModal"
                                />
                            </Col>
                            <Col xs={4} style={{display: 'flex', alignItems: 'center'}}>
                                <Dropdown overlay={menu} disabled={editDisabled}>
                                    <Btn id="useHoliDayModal">
                                        {String(use)} <Icon type="down" />
                                    </Btn>
                                </Dropdown>
                            </Col>
                        </Row>
                        <Divider />
                        <Row>
                            <Col xs={8}>
                                <label>
                                    {locale.holidayExplanation[language]}
                                </label>
                            </Col>
                            <Col xs={12}>
                                <TextArea name='description' id="desHoliDayModal" rows={4} onChange={this.textAreaChange} value={description} disabled={editDisabled}/>
                            </Col>
                        </Row>
                        <div hidden={this.state.show}>
                            <Alert message={this.state.message} type="error" />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleCancel} style={{width: 72}}>
                            {locale.close[language]}
                        </Button>
                        <Button hidden={editDisabled === true ? false : true} id="holiDayRemoveBtn" variant="danger" onClick={this.handleRemove} style={{width: 72}}>
                            {locale.delete[language]}
                        </Button>
                        <Button variant="primary" id="holiDayModifyBtn" onClick={editDisabled === true ? this.handleEdit : this.submitCheck} style={{width: 128}}>
                            {editDisabled === true ? locale.modification[language] +
                                "" : locale.modify[language] }
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default HoliDayModal;