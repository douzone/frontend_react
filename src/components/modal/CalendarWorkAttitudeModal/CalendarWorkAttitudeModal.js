import React, { Component } from 'react';
import { Modal, Button, Row, Col, Form } from 'react-bootstrap';
import { Alert, Input, Divider } from 'antd';
import moment from 'moment';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';

const { TextArea } = Input;

class CalendarWorkAttitudeModal extends Component {

    constructor(props, context) {
        super(props, context);
    
        this.state = {
          show: true,
          message: ''
        };
        this.startDayChange = this.startDayChange.bind(this);
        this.endDayChange = this.endDayChange.bind(this);
    }

    startDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "startDay";
        onChangeInput({name, value});
    }

    endDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "endDay";
        onChangeInput({name, value});
    }

    showModal = () => {
        //this.props.onChangeInput({name:'modal', value:true});
    }
    
    handleOk = (e) => {
        const { onSubmit } = this.props;
        onSubmit();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleEdit = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:false});
    }

    handleRemove = (e) => {
        const { onRemove } = this.props;
        onRemove();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }
    
    handleCancel = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:true});
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'startNo', value:''});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }

    submitCheck = (e) => {
        
        const { startdate, enddate, starttime, endtime } = this.props;
        const { handleOk } = this;
        /*
        const format = 'HH:mm';
        const between = moment.duration(moment(starttime, format).diff(moment(endtime, format))).asMinutes();

        if(startdate >= 0)
        {
            this.setState({ show : false,
                             message : '출근날짜가 없습니다.'});
            return;
        }

        if(enddate >= 0)
        {
            this.setState({ show : false,
                            message : '퇴근날짜가 없습니다.'});
            return;
        }

        if(startdate >= enddate) { // 출근날짜가 퇴근날짜보다 크거나 같을 때
            if(startdate > enddate) { // 출근날짜가 퇴근날짜보다 클 때
                this.setState({ show : false,
                    message : '출근날짜가 퇴근날짜보다 큽니다.'});
                return;
            }
            else if(startdate === enddate && between > 0) { // 출근날짜가 퇴근날짜가 같은데 시간의 차가 없을 때
                this.setState({ show : false,
                    message : '출근날짜가 퇴근날짜보다 큽니다.'});
                return;
            }
        }
        */

        handleOk(e);

    }

    render() {
        const { visible, name, title, startDay, endDay, workAttitudeList, content, editDisabled, auth, language } = this.props;
        const { handleChange, handleEdit, submitCheck, handleCancel, handleRemove,
                startDayChange, endDayChange } = this;
        
        return (
            <div>
                <Modal show={visible} centered={true} size="lg">
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title style={{color: '#ffffff'}}>
                            {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.applicant[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <Form.Control
                                type="text"
                                value={name}
                                onChange={handleChange}
                                disabled={true}
                                name="name"
                            />
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.Title[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <Form.Control
                                id="title"
                                type="text"
                                onChange={handleChange}
                                value={title}
                                disabled={editDisabled}
                                name="title"/>
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2} style={{display: 'flex', alignItems: 'center'}}>
                            {locale.workAttitudePeriod[language]} :{" "}
                        </Col>
                        <Col xs={4}>
                            <DatePickerInput
                                title={locale.Start_date[language]}
                                selected={startDay}
                                value={startDay}
                                onChange={startDayChange}
                                displayFormat='YYYY-MM-DD'
                                returnFormat='YYYY-MM-DD'
                                //defaultValue={this.state.yesterday}
                                locale={language}
                                readOnly={true}
                                disabled={editDisabled}
                            />
                        </Col>
                        ~
                        <Col xs={4}>
                            <DatePickerInput
                                title={locale.Start_date[language]}
                                selected={endDay}
                                value={endDay}
                                onChange={endDayChange}
                                displayFormat='YYYY-MM-DD'
                                returnFormat='YYYY-MM-DD'
                                //defaultValue={this.state.yesterday}
                                locale={language}
                                readOnly={true}
                                disabled={editDisabled}
                            />
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2}>
                            {locale.WorkAttitudeItem[language]} :{" "}
                        </Col>
                        <Col xs={2}>
                            <select id="selectMenu" name="workAttitudeList" type="text" disabled={editDisabled} value={workAttitudeList} onChange={handleChange}>
                                <option value="출장">{locale.Business_trip[language]}</option>
                                <option value="외근">{locale.Work_outside[language]}</option>
                                <option id="annual" value="연차">{locale.Annual_leave[language]}</option>
                                <option value="오전반차">{locale.morningSemiAnnualLeave[language]}</option>
                                <option value="오후반차">{locale.afternoonSemiAnnualLeave[language]}</option>
                                <option value="교육">{locale.Education[language]}</option>
                            </select>
                        </Col>
                    </Row>
                    <Divider />
                    <Row>
                        <Col xs={2}>
                            {locale.WorkAttitudeReason[language]} :{" "}
                        </Col>
                        <Col xs={10}>
                            <TextArea id="content" name='content' rows={4} disabled={editDisabled} onChange={handleChange} value={content} />
                        </Col>
                    </Row>
                    <div hidden={this.state.show}>
                        <Alert message={this.state.message} type="error" />
                    </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div>
                            <Button variant="secondary" onClick={handleCancel} style={{width: 72}}>
                                {locale.close[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button id="btnDelete" hidden={editDisabled === true ? false : true}  variant="danger" onClick={handleRemove} style={{width: 72}}>
                                {locale.delete[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button id="btnModify" variant="primary" onClick={editDisabled === true ? handleEdit : submitCheck} style={{width: 128}}>
                                {editDisabled === true ? locale.modification[language] : locale.modify[language] }
                            </Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default CalendarWorkAttitudeModal;
