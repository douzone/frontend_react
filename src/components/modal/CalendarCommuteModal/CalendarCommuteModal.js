import React, { Component } from 'react';
import { Modal, Button, Col, Row } from 'react-bootstrap';
import { TimePicker, Alert, Divider } from 'antd';
import moment from 'moment';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';


class CalendarCommuteModal extends Component {

    constructor(props, context) {
        super(props, context);
    
        this.state = {
          show: true,
          message: ''
        };
        this.startDateChange = this.startDateChange.bind(this);
        this.endDateChange = this.endDateChange.bind(this);
    }

    // List 검색 시작 날짜 값 변경
    startDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "fromDate";
        onChangeInput({name, value});
    }

    // List 검색 끝 날짜 값 변경
    endDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "toDate";
        onChangeInput({name, value});
    }

    showModal = () => {
        //this.props.onChangeInput({name:'modal', value:true});
    }
    
    handleOk = (e) => {
        const { onSubmit } = this.props;
        onSubmit();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleEdit = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:false});
    }

    handleRemove = (e) => {
        const { onRemove } = this.props;
        onRemove();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }
    
    handleCancel = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:true});
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'startNo', value:''});
    }

    startTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'startTime';
        onChangeInput({name, value});
    }

    endTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'endTime';
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }

    submitCheck = (e) => {
        const { startdate, enddate, starttime, endtime } = this.props;
        const { handleOk } = this;
        const format = 'HH:mm';
        const between = moment.duration(moment(starttime, format).diff(moment(endtime, format))).asMinutes();

        if(startdate >= 0)
        {
            this.setState({ show : false,
                             message : '출근날짜가 없습니다.'});
            return;
        }

        if(enddate >= 0)
        {
            this.setState({ show : false,
                            message : '퇴근날짜가 없습니다.'});
            return;
        }

        if(startdate >= enddate) { // 출근날짜가 퇴근날짜보다 크거나 같을 때
            if(startdate > enddate) { // 출근날짜가 퇴근날짜보다 클 때
                this.setState({ show : false,
                    message : '출근날짜가 퇴근날짜보다 큽니다.'});
                return;
            }
            else if(startdate === enddate && between > 0) { // 출근날짜가 퇴근날짜가 같은데 시간의 차가 없을 때
                this.setState({ show : false,
                    message : '출근날짜가 퇴근날짜보다 큽니다.'});
                return;
            }
        }

        handleOk(e);

    }

    render() {
        const { visible, startdate, starttime, enddate, endtime, editDisabled, auth, language } = this.props;
        const { startTimeHandleChange, endTimeHandleChange, startDateChange, endDateChange,
             handleEdit, submitCheck, handleCancel, handleRemove } = this;
        const format = 'HH:mm';
        
        return (
            <div>
                <Modal show={visible} centered={true}>
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title style={{color: '#ffffff'}}>
                        {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                    <div>
                        <Row>
                            <Col xs={8}>
                                <label>{locale.Start_date[language]}</label>
                            </Col>
                            <Col xs={4}>
                                <label>{locale.startTime[language]}</label>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={8}>
                                <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={startdate}
                                    value={startdate}
                                    onChange={startDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    //defaultValue={this.state.yesterday}
                                    locale={language}
                                    readOnly={true}
                                    disabled={editDisabled}
                                />
                            </Col>
                            <Col xs={4}>
                                <TimePicker
                                    format={format}
                                    value={moment(starttime, format)}
                                    allowClear={false}
                                    onChange={startTimeHandleChange}
                                    disabled={editDisabled}
                                />
                            </Col>
                        </Row>
                    </div>
                    <Divider />
                    <div>
                        <Row>
                            <Col xs={8}>
                                <label>{locale.End_date[language]}</label>
                            </Col>
                            <Col xs={4}>
                                <label>{locale.endTime[language]}</label>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={8}>
                                <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={enddate}
                                    value={enddate}
                                    onChange={endDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    //defaultValue={this.state.yesterday}
                                    locale={language}
                                    readOnly={true}
                                    disabled={editDisabled}
                                />
                            </Col>
                            <Col xs={4}>
                                <TimePicker
                                    format={format}
                                    value={moment(endtime, format)}
                                    allowClear={false}
                                    onChange={endTimeHandleChange}
                                    disabled={editDisabled}
                                />
                            </Col>
                        </Row>
                    </div>
                    <div hidden={this.state.show}>
                        <Alert message={this.state.message} type="error" />
                    </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <div>
                            <Button variant="secondary" onClick={handleCancel} style={{width: 72}}>
                                {locale.close[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button hidden={editDisabled === true ? false : true}  variant="danger" onClick={handleRemove} style={{width: 72}}>
                                {locale.delete[language]}
                            </Button>
                        </div>
                        <div hidden={auth === 'ROLE_ADMIN' ? false : true}>
                            <Button variant="primary" onClick={editDisabled === true ? handleEdit : submitCheck} style={{width: 128}}>
                                {editDisabled === true ? locale.modification[language] : locale.modify[language] }
                            </Button>
                        </div>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default CalendarCommuteModal;