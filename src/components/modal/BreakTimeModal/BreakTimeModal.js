import React, { Component } from 'react';
import { Modal, Button, Row, Col } from 'react-bootstrap';
import {TimePicker, Menu, Dropdown, Button as Btn, Icon, Alert, Input, Divider} from 'antd';
import moment from 'moment';
import locale from 'locale';

class BreakTimeModal extends Component {

    constructor(props, context) {
        super(props, context);

        this.state = {
            show: true,
            message: ''
        };
    }

    showModal = () => {
        //this.props.onChangeInput({name:'modal', value:true});
    }

    handleOk = (e) => {
        const { onSubmit } = this.props;

        onSubmit();

        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleEdit = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:false});
    }

    handleRemove = (e) => {
        const { onRemove, use, language } = this.props;

        if(String(use) === 'true'){
            this.setState({ show : false,
                message : '사용하고 있는 휴게 시간입니다.'});

            if(language === "en"){
                this.setState({
                    message: 'This is the rest hours you\'re using'
                });
            }

            return;
        }

        onRemove();
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name:'editDisabled', value:true});
    }

    handleCancel = (e) => {
        this.props.onChangeInput({name:'editDisabled', value:true});
        this.props.onChangeInput({name:'modal', value:false});
        this.props.onChangeInput({name: 'no', value: 0});
        this.setState({ show : true, message: '' });
    }

    startHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'start';
        onChangeInput({name, value});
    }

    endHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'end';
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const name = 'use';
        const value = e.key;

        onChangeInput({name, value});
    }

    textAreaChange = (e) => {
        const { onChangeInput, language } = this.props;
        const { value, name } = e.target;

        if(language === "en"){
            onChangeInput({name: 'descriptionEn', value: value});
        }
        else {
            onChangeInput({name, value});
        }
    }

    submitCheck = (e) => {
        const { start, end, description, descriptionEn, language } = this.props;
        const { handleOk } = this;
        const format = 'HH:mm';
        const between = moment.duration(moment(start, format).diff(moment(end, format))).asMinutes();

        if(between >= 0){
            this.setState({ show : false,
                message : '시작시간과 끝시간의 간격이 없습니다.'});

            if(language === "en"){
                this.setState({
                    message: 'There is no interval between start and end times'
                });
            }

            return;
        }

        if(language === "en"){
            if(descriptionEn === ''){
                this.setState({
                   show: false,
                   message: 'No rest description'
                });

                return;
            }
        }
        else {
            if (description === '') {
                this.setState({
                    show: false,
                    message: '휴게 설명이 없습니다.'
                });

                return;
            }
        }

        handleOk(e);
    }

    render() {
        let { visible, start, end, use, description, descriptionEn, editDisabled, language } = this.props;
        const format = 'HH:mm';
        const {TextArea} = Input;

        const menu = (
            <Menu onClick={this.handleChange}>
                <Menu.Item key="true">true</Menu.Item>
                <Menu.Item key="false">false</Menu.Item>
            </Menu>
        );

        if(language === "en"){
            description = descriptionEn;
        }

        return (
            <div>
                <Modal show={visible} centered={true} >
                    <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
                        <Modal.Title style={{color: '#ffffff'}}>
                            {editDisabled === true ? locale.detailView[language] : locale.modify[language] }
                        </Modal.Title>
                    </Modal.Header>
                    <Modal.Body style={{fontWeight: 'bold'}}>
                        <Row>
                            <Col xs={4}>
                                <label>
                                    {locale.startTime[language]}
                                </label>
                            </Col>
                            <Col xs={4}>
                                <label>
                                    {locale.endTime[language]}
                                </label>
                            </Col>
                            <Col xs={4}>
                                <label>
                                    {locale.use[language]}
                                </label>
                            </Col>
                        </Row>
                        <Row>
                            <Col xs={4}>
                                <TimePicker
                                    format={format}
                                    value={moment(start, format)}
                                    allowClear={false}
                                    onChange={this.startHandleChange}
                                    disabled={editDisabled}
                                    id="fromBreakTimeModal"
                                />
                            </Col>
                            <Col xs={4}>
                                <TimePicker
                                    format={format}
                                    value={moment(end, format)}
                                    allowClear={false}
                                    onChange={this.endHandleChange}
                                    disabled={editDisabled}
                                    id="toBreakTimeModal"
                                />
                            </Col>
                            <Col xs={4}>
                                <Dropdown overlay={menu} disabled={editDisabled}>
                                    <Btn id="useBreakTimeModal">
                                        {String(use)} <Icon type="down" />
                                    </Btn>
                                </Dropdown>
                            </Col>
                        </Row>
                        <Divider />
                        <Row>
                            <Col xs={8}>
                                <label>
                                    {locale.breakTimeExplanation[language]}
                                </label>
                            </Col>
                            <Col xs={12}>
                                <TextArea name='description' id="desBreakTimeModal" rows={4} onChange={this.textAreaChange} value={description} disabled={editDisabled}/>
                            </Col>
                        </Row>
                        <div hidden={this.state.show}>
                            <Alert message={this.state.message} type="error" />
                        </div>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={this.handleCancel} style={{width: 72}}>
                            {locale.close[language]}
                        </Button>
                        <Button hidden={editDisabled === true ? false : true} id="breakTimeRemoveBtn" variant="danger" onClick={this.handleRemove} style={{width: 72}}>
                            {locale.delete[language]}
                        </Button>
                        <Button variant="primary" id="breakTimeModifyBtn" onClick={editDisabled === true ? this.handleEdit : this.submitCheck} style={{width: 128}}>
                            {editDisabled === true ? locale.modification[language] +
                                "" : locale.modify[language] }
                        </Button>
                    </Modal.Footer>
                </Modal>
            </div>
        );
    }
}

export default BreakTimeModal;