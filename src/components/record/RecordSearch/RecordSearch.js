import React, {Component} from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import {Button as Btn, Dropdown, Icon, Input, Menu,message, AutoComplete} from 'antd/lib/index';
import Modal from "react-awesome-modal";
import * as api from 'lib/api';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';
import moment from 'moment';

import { Table, TableRow, TableHead, TableCell, withStyles } from "@material-ui/core";

const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;

let a = false;
let tableUserSelect = false;
let selectNo = 0;

class RecordSearch extends Component {

    constructor(props) {
        super(props);
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        this.state = {
            yesterday,
            changeValue: "",
            arry: [],
            dataSource: [
                {
                    title: '회원리스트',
                    children: []
                }
            ],
            selectNo: "",
            select: false,
            selectName: ""
        }
        this.searchFromDateChange = this.searchFromDateChange.bind(this);
        this.searchToDateChange = this.searchToDateChange.bind(this);
    }

    // List 검색 시작 날짜 값 변경
    searchFromDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "startdate";
       onChangeInput({name, value});
    }

    // List 검색 끝 날짜 값 변경
    searchToDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "enddate";
       onChangeInput({name, value});
    }

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const {value, name} = e.target;
        onChangeInput({name, value});
    }

    handleTypeChange = (e) => {
        const {onChangeInput} = this.props;
        const name = 'newRecordType';
        const value = e.key;

        onChangeInput({name, value});
    }

    handleReadChange = (e) => {
        const {onChangeInput} = this.props;
        const name = 'newRead';
        const value = e.key;

        onChangeInput({name, value});
    }

    handleSubmit = () => {
        // const {onSubmit} = this.props;

        // onSubmit();
    }

    _onChange = (value) => {

        this.setState({
            changeValue: value
        })
        let ary = [{title: "회원리스트", children: []}];
        if (value != "") {
            this.state.arry[0].children.map((items, index) => {
                if (items.title.includes(value)) {
                    ary[0].children.push(
                        {
                            title: items.title,
                            no: items.no,
                            name: items.name
                        }
                    )
                }
            })

        }

        this.setState({
            dataSource:
            ary
        })
        if (a) {
            this.setState({
                dataSource: [
                    {
                        title: '회원리스트',
                        children: []
                    }
                ],
                selectNo: "",
                select: false,
                selectName: ""
            })
            a = false;
        }
    }

    _onChange2 =(value)=> {
        // this.setState( {
        //     dataSource : [
        //         {
        //          title: '회원리스트',
        //           children: [

        //           ]
        //         }
        //       ]})
        this.setState({
            changeValue: value
        })
        let ary = [{title: "회원리스트", children: []}];
        if (value != "") {
            this.state.arry[0].children.map((items, index) => {
                if (items.title.includes(value)) {
                    ary[0].children.push(
                        {
                            title: items.title,
                            no: items.no,
                            name: items.name
                        }
                    )
                }
            })

        }

        this.setState({
            dataSource2:
            ary
        })
        if (tableUserSelect) {
            this.setState({
                dataSource2: [
                    {
                        title: '회원리스트',
                        children: []
                    }
                ],
                selectNo: "",
                select: false,
                selectName: ""
            })
            tableUserSelect = false;
        }
    }

    componentWillMount() {
        const {token} = this.props;
        let ary = [{title: "회원리스트", children: []}];
        api.getUserListByName(token).then(response => {
            //console.log(response.data.data)
            if (response.data.data != null) {
                response.data.data.map((items, index) => {
                    ary[0].children.push(
                        {
                            title: items.name + "(" + items.username + ")",
                            no: items.no,
                            name: items.name
                        }
                    );
                })
                this.setState({
                    arry:
                    ary
                })
            }
        });
    }

    _select(value, option, onSubmit) {
        const {onChangeInput} = this.props;

        a = true;
        this.setState({
            selectNo: value,
            select: true,
            selectName: option.props.children
        });

        //console.log(value + "@@@@" + this.state.changeValue);

        onChangeInput({name: 'no', value: value});
        onChangeInput({name: 'select', value: true});
        onChangeInput({name: 'name', value: ''});

        onSubmit(value, true, this.state.changeValue);

        this._initialization();
    }

    _select2(value, option, onSubmit) {
        const {onChangeInput} = this.props;

        tableUserSelect = true;

        this.setState({
            selectNo: value,
            select: true,
            selectName: option.props.children
        })

        onChangeInput({name: 'no', value: value});
        onChangeInput({name: 'select', value: true});
        onSubmit(value, true, this.state.changeValue);
        selectNo = value;
        this._initialization();
    }

    _seachButtonClick(onSubmit) {
        const {onChangeInput,startdate,enddate,language} = this.props;
        message.config( {top: 100, duration: 2});
        if (startdate <= 0 || enddate <= 0 ) {
            message.info(locale.Empty_Period[language]);
            return;
        }
      
        if (startdate > enddate) {
            message.info(locale.Time_Longer[language]);
            return;
        }
        
        onChangeInput({name: 'select', value: false});
        onChangeInput({name: 'name', value: this.state.changeValue});
        onChangeInput({name: 'no', value: ''});

        onSubmit(this.state.selectNo, a, this.state.changeValue);

        this._initialization();
    }

    _seachButtonClick2(onSubmit) {
        const {onChangeInput,startdate,enddate,language} = this.props;
        message.config( {top: 100, duration: 2});

        if (startdate <= 0 || enddate <= 0 ) {
            message.info(locale.Empty_Period[language]);
            return;
        }
      
        if (startdate > enddate) {
            message.info(locale.Time_Longer[language]);
            return;
        }
        
        
        onChangeInput({name: 'userName', value: this.state.changeValue});
        onChangeInput({name: 'select', value: tableUserSelect});
        onSubmit(this.state.selectNo, tableUserSelect, this.state.changeValue);

        this._initialization();
    }

    _initialization() {
        this.setState({
            dataSource: [
                {
                    title: '회원리스트',
                    children: []
                }
            ],
            selectNo: "",
            select: false,
            selectName: ""
        })
    }

    render() {
        const {searchToDateChange, searchFromDateChange} = this;
        const {startdate, enddate, name, content, recordType, read, onSubmit, language, auth, classes} = this.props;
        const {Search} = Input;

        const menu = (
            <Menu onClick={this.handleTypeChange}>
                <Menu.Item key="전체">{locale.All[language]}</Menu.Item>
                <Menu.Item id="goToWork" key="출근">{locale.goToWork[language]}</Menu.Item>
                <Menu.Item key="퇴근">{locale.getOffWork[language]}</Menu.Item>
                <Menu.Item key="근태">{locale.workAttitude[language]}</Menu.Item>
            </Menu>
        );

        const readMenu = (
            <Menu onClick={this.handleReadChange}>
                <Menu.Item key="전체">{locale.All[language]}</Menu.Item>
                <Menu.Item id="true" key="true">{locale.Read[language]}</Menu.Item>
                <Menu.Item key="false">{locale.UnRead[language]}</Menu.Item>
            </Menu>
        )
        if (this.state.arry == null)
            return null;

        if (auth === "ROLE_ADMIN") {
            return (
                <div className={classes.borderline}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.ActivePeriod[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                        title={locale.Start_date[language]}
                                        selected={startdate}
                                        onChange={searchFromDateChange}
                                        displayFormat='YYYY-MM-DD'
                                        returnFormat='YYYY-MM-DD'
                                        //defaultValue={this.state.yesterday}
                                        locale={language}
                                        readOnly={true}
                                        style={{width:'20%', display:'inline-table'}}
                                    />
                                    <div className={classes.hyphen}> - </div>
                                    <DatePickerInput
                                        title={locale.End_date[language]}
                                        selected={enddate}
                                        onChange={searchToDateChange}
                                        displayFormat='YYYY-MM-DD'
                                        returnFormat='YYYY-MM-DD'
                                        locale={language}
                                        readOnly={true}
                                        style={{width:'20%', display:'inline-table'}}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Type[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu}>
                                    <Btn>
                                        {recordType==='전체'?locale.All[language]:
                                        recordType==='출근'?locale.goToWork[language]:
                                        recordType==='퇴근'?locale.getOffWork[language]:
                                        recordType==='근태'?locale.workAttitude[language]:''}
                                    <Icon type="down"/>
                                    </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.ReadCheck[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={readMenu}>
                                    <Btn>
                                        {read==='전체'?locale.All[language]:
                                        read==='true'?locale.Read[language]:
                                        read==='false'?locale.UnRead[language]:''} 
                                    <Icon type="down"/>
                                    </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Name[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <AutoComplete
                                        dropdownStyle={{width: 300}}
                                        size="large"
                                        style={{width: '45%'}}
                                        dataSource={this.state.dataSource
                                            .map(group => (
                                                <OptGroup key={"회원리스트"}>
                                                    {group.children.map(opt => (
                                                        <Option key={opt.no} value={opt.no}>
                                                            {opt.title}
                                                        </Option>
                                                    ))}
                                                </OptGroup>
                                            ))}
                                        onChange={(value) => this._onChange(value)}
                                        onSelect={(value, option) => this._select(value, option, onSubmit)}
                                    >
                                    <Search placeholder={locale.Employee_name[language]}/>
                                    </AutoComplete>
                                </TableCell>
                            </TableRow>
                            
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Contents[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Input
                                        placeholder={locale.Contents[language]}
                                        title="content"
                                        type="text"
                                        value={content}
                                        name="newContent"
                                        onChange={this.handleChange}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                        <Button style={{width:80}} onClick={() => this._seachButtonClick(onSubmit)} theme="outline">
                            {locale.Search[language]}
                        </Button>
                    </div>
                </div>
            );
        } else {
            return (
                <div className={classes.borderline}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.ActivePeriod[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                        title={locale.Start_date[language]}
                                        selected={startdate}
                                        onChange={searchFromDateChange}
                                        displayFormat='YYYY-MM-DD'
                                        returnFormat='YYYY-MM-DD'
                                        locale={language}
                                        readOnly={true}
                                        style={{width:'20%', display:'inline-table'}}
                                    />
                                    <div className={classes.hyphen}> - </div>
                                    <DatePickerInput
                                        title={locale.End_date[language]}
                                        selected={enddate}
                                        onChange={searchToDateChange}
                                        displayFormat='YYYY-MM-DD'
                                        returnFormat='YYYY-MM-DD'
                                        locale={language}
                                        readOnly={true}
                                        style={{width:'20%', display:'inline-table'}}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Type[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu}>
                                    <Btn>
                                        {recordType==='전체'?locale.All[language]:
                                        recordType==='출근'?locale.goToWork[language]:
                                        recordType==='퇴근'?locale.getOffWork[language]:
                                        recordType==='근태'?locale.workAttitude[language]:''} 
                                    <Icon type="down"/>
                                    </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.ReadCheck[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={readMenu}>
                                    <Btn>
                                        {read==='전체'?locale.All[language]:
                                        read==='true'?locale.Read[language]:
                                        read==='false'?locale.UnRead[language]:''} 
                                    <Icon type="down"/>
                                    </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Contents[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Input
                                    id="userContent"
                                    placeholder={locale.Contents[language]}
                                    title="content"
                                    type="text"
                                    value={content}
                                    name="newContent"
                                    onChange={this.handleChange}
                                    />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                    <Button style={{width:80}} id="btnUserSearch" onClick={() => this._seachButtonClick2(onSubmit)}
                            theme="outline">{locale.Search[language]}</Button>
                    </div>
                </div>
            );
        }
    }
}

const styles = theme => ({
    borderline: {
        borderTop: '2px solid rgba(224, 224, 224, .5)'
    },
    tableHead: {
        "& tr" : {
            height: '48px'
        },
        "& th:first-of-type" : {
            width:'15%', 
            backgroundColor: '#fafafa',
            padding: '0px 0px 0px 16px',
        },
        "& tr:last-of-type": {
            "& th": {
                borderBottom: '2px solid rgba(224, 224, 224, .5)'
            }
        }
    },
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        padding: '8px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    },
    hyphen: {
        width:'5%', 
        display:'inline-table', 
        textAlign: 'center'
    }
});

export default withStyles(styles)(RecordSearch);