import React, {Component} from 'react';
import {Badge} from 'react-bootstrap';
import {Grid, Table, TableRow, TableHead, TableBody, TableCell} from '@material-ui/core';
import {Button} from "antd";
import locale from 'locale';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import "../../list/TableList/TableList.css";


import { withStyles } from "@material-ui/core";

class RecordList extends Component {

    handleShowModal = (table) => {
        //console.log(table);
        const {handleChangeInput} = this.props;

        handleChangeInput({name: 'no', value: table.no});
        handleChangeInput({name: 'modal', value: true});
    }

    render() {
        const {tables, language, classes} = this.props;

        //console.log(tables);

        const RecordList = tables.map(
            (table, index) => {
                let {day, actor, insertUserId, recordType, recordTypeEn, content, contentEn, read} = table.toJS();
                let badge = '';

                if (read === true) {
                    badge = "success";
                } else {
                    badge = "danger";
                }

                if (language === "en") {
                    recordType = recordTypeEn;
                    content = contentEn;
                } else {
                    read === true ? read = '읽음' : read = '읽지않음';
                }

                return (
                    <TableRow className={classes.tableBody} hover key={index} onClick={event => this.handleShowModal(table.toJS())}>
                        <TableCell className="pl-3 fw-normal">{day}</TableCell>
                        <TableCell id="actor">{actor}</TableCell>
                        <TableCell id="id">{insertUserId}</TableCell>
                        <TableCell>{recordType}</TableCell>
                        <TableCell>{content}</TableCell>
                        <TableCell><h6><Badge variant={badge}>{String(read)}</Badge></h6></TableCell>
                    </TableRow>
                );
            }
        );

        return (
            <React.Fragment>
                <div style={divWrapper}>
                    <Button type="link" shape="round" icon="download" size="large">
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="css"
                            table="recordTable"
                            filename="recordTableXLS"
                            sheet="recordTableXLS"
                            buttonText="Download"
                        />
                    </Button>
                </div>
                <Grid item xs={12} style={{marginTop: '10px'}}>
                    <div>
                        <Table id="recordTable" className="mb-0">
                            <TableHead>
                                <TableRow className={classes.tableHead}>
                                    <TableCell><b>{locale.Date[language]}</b></TableCell>
                                    <TableCell><b>{locale.ActiveAgent[language]}</b></TableCell>
                                    <TableCell><b>{locale.Id[language]}</b></TableCell>
                                    <TableCell><b>{locale.Type[language]}</b></TableCell>
                                    <TableCell><b>{locale.Contents[language]}</b></TableCell>
                                    <TableCell><b>{locale.ReadCheck[language]}</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {RecordList}
                            </TableBody>
                        </Table>
                    </div>
                </Grid>
            </React.Fragment>
        );
    }
}

const divWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end",
}

const styles = theme => ({
    tableHead: {
        backgroundColor: '#fafafa',
        borderTop: '2px solid rgba(224, 224, 224, .5)',
        height: theme.spacing.unit * 6,
        "& th" : {
            borderBottom: '2px solid rgba(224, 224, 224, .5)',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            width: '10%'
        },
        "& th:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        },
        "& th:nth-child(4)" : {
            width: '15%'
        },
        "& th:nth-child(5)" : {
            width: '45%'
        }
    },
    tableBody: {
        padding: '0px 0px 0px 0px',
        height: theme.spacing.unit * 5,
        "& td" : {
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
        },
        "& td:nth-child(5)" : {
            paddingLeft: theme.spacing.unit * 2,
            textAlign: 'left'
        },
        "& td:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        }
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(RecordList);
