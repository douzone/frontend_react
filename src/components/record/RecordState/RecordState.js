import React, {Component} from 'react';
import {Grid} from '@material-ui/core';
import Widget from 'components/Widget';
import {PieChart, Pie, ResponsiveContainer, Sector} from "recharts";
import locale from 'locale';


class RecordState extends Component {

    render() {
        const {tables, language, auth, loginUserName, subType, subRead, subContent, subSelect, subStartdate, subEnddate} = this.props;

        let normal = tables.get("읽음");
        let error = tables.get("읽지않음");
        let recordSearchInfo = '';

        let PieChartData = [
            {name: normal, value: normal, color: "primary"},
            {name: error, value: error, color: "success"},
        ];

        if (normal === 0 && error === 0) {
            normal = 1;

            PieChartData = [
                {name: 0, value: normal, color: "primary"},
                {name: 0, value: error, color: "success"}
            ];
        }

        if(auth === 'ROLE_USER' && subSelect === true && (subStartdate !=='' || subEnddate !== '')) {
            recordSearchInfo = 
                loginUserName + "님의 " + 
                subStartdate + " ~ " + 
                subEnddate + " 활동기록(타입: " +
                subType + ", 읽음 여부: " + 
                subRead + ")";

        } else if(auth === 'ROLE_USER' && subStartdate ==='' && subEnddate === '') {
            recordSearchInfo = 
                loginUserName + "님의 활동기록(타입: " +
                subType + ", 읽음 여부: " + 
                subRead + ")";

        } else if(auth === 'ROLE_ADMIN' && subSelect === true) {
            if(subStartdate ==='' && subEnddate ==='')
                recordSearchInfo = 
                    "사원 활동기록(타입: " +
                    subType + ", 읽음 여부: " + 
                    subRead + ")";
                    
            else {
                recordSearchInfo = 
                    "사원 " + 
                    subStartdate + " ~ " + 
                    subEnddate + " 활동기록(타입: " +
                    subType + ", 읽음 여부: " + 
                    subRead + ")";
            }
        } else if(auth === 'ROLE_ADMIN'  && subStartdate ==='' && subEnddate === '') {
            recordSearchInfo = 
                "사원 활동기록(타입: " +
                subType + ", 읽음 여부: " + 
                subRead + ")";
        }

        const renderActiveShape = (props) => {
            const {
                cx, cy, innerRadius, outerRadius, startAngle, endAngle,
                fill, payload
            } = props;

            return (
                <g>
                    <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };

        return (
            <React.Fragment>
                {/*<div>
                    <strong>{recordSearchInfo}</strong>
                </div>*/}
                <br/>
                <Grid container spacing={32}>
                    <Grid item style={{width: '50%'}}>
                        <Widget title={locale.Read[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin:'0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={0}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#28a745'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '50%'}}>
                        <Widget title={locale.UnRead[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin:'0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={1}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#dc3545'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                </Grid>

                <br/>
            </React.Fragment>
        )
    }
}

const pieChartLegendWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
}

export default RecordState;