import React, {Component} from "react";
import {connect} from "react-redux";
import moment from "moment";
import * as commuteActions from "store/modules/commute";
import * as headerActions from "store/modules/Header";
import * as timeActions from "store/modules/time";
import {bindActionCreators} from "redux";
import locale from "locale";
let timer = null;
let ary= [];
class TimeList extends Component {
    state = {
        timeset: " "
    };
    getTime = () => {
        const hour = moment().format("HH");
        const minute = moment().format("mm");
        const second = moment().format("ss");
        const timeset = `${hour > 9 ? hour : `0${hour}`}:${minute > 9 ? minute : `0${minute}`}:${second > 9 ? second : `0${second}`}`;
        this.setState({
            timeset
        });
    };

    componentWillMount() {
        let cnt = 0;
        const {auth, CommuteActions, token, loginUserNo, TimeActions} = this.props;
        let name = "goto";
        let value = false;
        CommuteActions.changeInput({name, value});
        name = "gooff";
        value = false;
        CommuteActions.changeInput({name, value});
        name = "starttime"
        value = "";
        TimeActions.changeInput({name, value});
        name = "endtime"
        value = "";
        TimeActions.changeInput({name, value});

        if (auth === 'ROLE_USER') {
            timer = setInterval(() => {
                const {endtime, starttime, goto, gooff} = this.props;

                cnt++;
                if (starttime === "" && goto) {
                    TimeActions.getStartTime(loginUserNo, token);
                }
                // //console.log(goto);
                // //console.log(starttime);
                // //console.log(endtime);
                // //console.log(gooff);
                if (starttime !== "" && endtime === "" && gooff) {
                    TimeActions.getTotalWorkTime(loginUserNo, token);
                    TimeActions.getTodayCommuteEndTime(loginUserNo, token);
                    clearInterval(timer);
                }

                CommuteActions.changeInput({name: 'cnt', value: cnt});
                this.tick();
               // //console.log("인터벌~");
                if (!sessionStorage.getItem("id_token")) {

                    clearInterval(timer);

                }
            }, 1000);
        }
    }

    convertState(name, value) {
        const {CommuteActions} = this.props;

        CommuteActions.stateChange({name, value});
    }

    tick = () => {
        const {endTime, preGoTo, preGoOff, preHoliDay, language} = this.props;

        this.convertState('time', new Date());

        const now = moment(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + (new Date().getDate()) + " " + new Date().getHours() + ":" + new Date().getMinutes() + ":" + new Date().getSeconds(), 'YYYY-MM-DD HH:mm:ss');
        const end = moment(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + (new Date().getDate()) + " " + endTime, 'YYYY-MM-DD HH:mm:ss');
        const night = moment(new Date().getFullYear() + '-' + (new Date().getMonth() + 1) + '-' + (new Date().getDate() + 1) + " " + "00:00:00", 'YYYY-MM-DD HH:mm:ss')


        if (!preGoTo && preGoOff) {
            this.convertState('preGoTo', true);
        }

        if ((now.isoWeekday() === 1) || (preHoliDay === true)) {
            this.convertState('preGoTo', true);
            this.convertState('preGoOff', true);
        }

        if (now.isAfter(night)) {
            //console.log("same");
            this.convertState('isNight', true);
        }

        if (now.isAfter(end)) {
            //console.log("after");
            this.convertState('isGoOff', true);
            this.convertState('isNigiht', false);
        }

        if (now.isBefore(end)) {
            //console.log("before");
            this.convertState('isGoOff', false);
            this.convertState("isNight", false);
        }
    }

    render() {
        let state=0;
        //지각여부
        let late=false;
        let now = moment().format('HH:mm:ss');
        const {totalWorkTime, starttime, time1, endtime, goto,breakTimeList,language,CommuteActions} = this.props;
        //console.log(language);
        ////console.log(starttime);
       // //console.log(goto);
       // //console.log("랜더")
       //console.log(time1);
       let breaktimeboolean=false;
       let gotoBreakTime=false;

        let timeset;
        if (starttime === "") {
            timeset = '00:00:00';
        } else if (starttime !== "") {

            const selectstarttime = moment(time1, "HH:mm:ss");
            const pushnow = moment(now, "HH:mm:ss");
            let pushstarttime = moment(starttime, "HH:mm:ss");
            const timediff = selectstarttime.diff(pushnow, "seconds");       // 출근해야하는시간 - 현재시간    양수이면 근무시간 0 출력 음수이면 계산
            const timediff2 = selectstarttime.diff(pushstarttime, "seconds");// 출근해야하는시간 - 출근시간  양수이면 정상출근 음수이면 지각
                                                                             // 정상 출근이면   출근시간 = 출근해야하는시간 
                                                                            // 지각 이면 출근시간 = 출근한 시간
            // //console.log(timediff);
            if (timediff > 0) {
                timeset = "00:00:00";
                //console.log("@@");
            } else {

                let secondsDiff;
                //정상 출근
                if (timediff2>=0)
                {
                    late=false;
                    secondsDiff=pushnow.diff(selectstarttime, "seconds");
                }
                //지각 출근
                else
                {
                    late=true;
                    secondsDiff=pushnow.diff(pushstarttime, "seconds");
                }

                let total=0; 
                let breakTime=moment("00:00", "HH:mm:ss"); 
                


                if(breakTimeList!=="" && ary.length===0)
                breakTimeList.map((items,index)=>{
                    // console.log(items);
                     ary.push({
                         description:items.description,
                         start:moment(items.start, "HH:mm:ss"), 
                         end:moment(items.end, "HH:mm:ss"),
                         time:moment(items.end, "HH:mm:ss").diff(moment(items.start, "HH:mm:ss"),"seconds")
                     })
                })
                //console.log(ary);
                // let state=0;
                 let breakStartTime=0;
                 let gotoBreakTimeEnd=0;
                if(ary.length!==0)
                {
                    ary.map((items,index)=>{
                       const statediff = items.start.diff(pushnow, "seconds");
                       const statediff2 = items.end.diff(pushnow, "seconds");
                       
                       //출근을 휴게시간 도중에 했을때
                       if(items.start.diff(pushstarttime)<=0 && items.end.diff(pushstarttime)>=0){
                           //state=2;
                           gotoBreakTimeEnd = items.end;
                           pushstarttime=items.end;
                           gotoBreakTime=true;
                           console.log("@@@");
                       }
                       //else
                       //휴게시간 지나간
                       if(statediff2<0){
                       // console.log("지나간 휴게시간:"+items.description);
                        //state=0;
                        //출근시간 이후의 breaktime만
                        if(statediff<0 && items.start.diff(pushstarttime)>0)
                        breakTime=breakTime.add(items.time,"seconds");
                        }

                        //휴게시간
                       else if(statediff<=0 && statediff2>=0){
                      console.log("현재는 휴게시간:"+items.description);
                       state=1;
                       breakStartTime=items.start;
                       }

                        //아직 시간 안된 휴게시간
                    //     else if( statediff>0){
                    //         console.log("앞으로의 휴게시간:"+items.description);
                    //         state=2;
                           
                    //    }
                    })
                    let selectTime;
                    if(late)
                        selectTime= pushstarttime;
                    else if(late&&gotoBreakTime)
                        selectTime=gotoBreakTimeEnd;
                    else
                         selectTime= selectstarttime;
                    if(state===0){
                        console.log("state0");
                    total=pushnow.diff(selectTime, "seconds")
                    //console.log(total);
                    const latehour = Math.floor(total / 3600)
                const latemin = Math.floor((total - (latehour * 3600)) / 60)
                const latesec = (total - (latehour * 3600) - (latemin * 60))
                timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
                total=moment(timeset, "HH:mm:ss").diff(breakTime);     
                //console.log(total);
                    
                    }
                    else if(state===1){
                        console.log(gotoBreakTime);
                        console.log(late);
                        console.log("state1");
                       
                        total=breakStartTime.diff(selectTime, "seconds")
                        if(total<=0)
                        total=0;
                        else{
                    const latehour = Math.floor(total / 3600)
                    const latemin = Math.floor((total - (latehour * 3600)) / 60)
                    const latesec = (total - (latehour * 3600) - (latemin * 60))
                    timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
                    total=moment(timeset, "HH:mm:ss").diff(breakTime);     
                        }
                   
                   // console.log(total);
                    
                    }
                    else if(state===2){
                        console.log("State2")
                        total=pushnow.diff(gotoBreakTimeEnd, "seconds");
                       //// console.log(total);
                       // console.log(total);  
                        if(total<=0){
                        total=0;
                        breaktimeboolean=true;
                        }
                        else
                        {
                         const latehour = Math.floor(total / 3600)
                    const latemin = Math.floor((total - (latehour * 3600)) / 60)
                    const latesec = (total - (latehour * 3600) - (latemin * 60))
                    timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
                    total=moment(timeset, "HH:mm:ss").diff(breakTime);   
                   // console.log(total);  
                        }
                        if(total<=0)
                        total=0;
                    }
            //         if(state==2){
            //         total=pushnow.diff(pushstarttime, "seconds")
                    
                    
                    
            //         const latehour = Math.floor(total / 3600)
            //     const latemin = Math.floor((total - (latehour * 3600)) / 60)
            //     const latesec = (total - (latehour * 3600) - (latemin * 60))
            //     timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
            //     total=moment(timeset, "HH:mm:ss").diff(breakTime);     
            //     console.log(total);
            // }
           // console.log(breakTime);

                                      }
                //휴게시간이 존재x
                else{
                    total=pushnow.diff(secondsDiff, "seconds");
                }
               // console.log(total/1000);
               //if(state!==2)
                total = total /1000;
              
                const latehour = Math.floor(total / 3600)
                const latemin = Math.floor((total - (latehour * 3600)) / 60)
                const latesec = (total - (latehour * 3600) - (latemin * 60))
                timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
                
                CommuteActions.changeInput({
                    name:"totalWorkTime",
                    value:timeset
                })
            }
        }
            //     let secondsDiff;
            //   //  //console.log(timediff2);
            //     if (timediff2 > 0) {
            //         const startdt = moment(now, "HH:mm:ss");
            //         const enddt = moment(time1, "HH:mm:ss");
            //         secondsDiff = startdt.diff(enddt, "seconds");


            //     } else {
            //         const startdt = moment(now, "HH:mm:ss");
            //         const enddt = moment(starttime, "HH:mm:ss");
            //         secondsDiff = startdt.diff(enddt, "seconds");

                    
            //     }
            //     const latehour = Math.floor(secondsDiff / 3600)
            //     const latemin = Math.floor((secondsDiff - (latehour * 3600)) / 60)
            //     const latesec = (secondsDiff - (latehour * 3600) - (latemin * 60))

            //     if (secondsDiff < 0) {
            //         timeset = "00:00:00"
            //     } else {
            //         //   //console.log("@@@@");
            //         timeset = `${latehour > 9 ? latehour : `0${latehour}`}:${latemin > 9 ? latemin : `0${latemin}`}:${latesec > 9 ? latesec : `0${latesec}`}`;
        //         }
        //     }

        // }
        if (endtime === ""&&state==1||(state==2&&breaktimeboolean))
            return (<div>{timeset}<br/><div><h6>{locale.timeset[language]}</h6></div></div>);
        else if (endtime === ""&&(state==0||state==2))
            return (<div>{timeset}</div>);
        else
            return <div>{totalWorkTime}</div>;
    }
}

export default connect(
    state => ({
        loginUserNo: state.login.no,
        token: state.login.token,
        auth: state.login.auth,
        no: state.list.get("no"),
        lasttime: state.commute.get("lasttime"),
        cnt: state.commute.get('cnt'),
        preGoTo: state.commute.get("preGoTo"),
        preGoOff: state.commute.get("preGoOff"),
        preHoliDay: state.holiday.get("preHoliDay"),
        goto: state.commute.get("goto"),
        gooff: state.commute.get("gooff"),
        endTime: state.commute.get("endTime"),
        totalWorkTime: state.time.get("totalWorkTime"),
        starttime: state.time.get("starttime"),
        time: state.time.get("time"),
        start: state.time.get("start"),
        end: state.time.get("end"),
        time1: state.time.get("time1"),
        endtime: state.time.get("endtime"),
        breakTimeList:state.time.get("breakTimeList"),
        language: state.language.language
    }),
    dispatch => ({
        CommuteActions: bindActionCreators(commuteActions, dispatch),
        HeaderActions: bindActionCreators(headerActions, dispatch),
        TimeActions: bindActionCreators(timeActions, dispatch)

    })
)(TimeList);