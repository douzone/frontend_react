import { compose, withState, withHandlers, lifecycle } from 'recompose';
import { connect } from 'react-redux';

import HeaderView from './HeaderView';
import { getNotice, setNoticeStore } from "components/Header/Notice/NoticeListState";
import { signOut } from '../../pages/login/LoginState';
import { toggleSidebar } from '../Layout/LayoutState';
import { changeLanguage } from 'store/modules/language'
import { inittime } from 'store/modules/time';
import { click } from 'store/modules/alarm';

export default compose(
  connect(
    state => ({
      isSidebarOpened: state.layout.isSidebarOpened,
      no: state.login.no,
      auth: state.login.auth,
      token: state.login.token,
      name: state.login.name,
      id: state.login.id,
      tables: state.notice.tables,
      isInit: state.notice.isInit,
      isLogouting: state.login.isLogouting,
      isLogging: state.login.isLogging,
      language: state.language.language,
      isClick: state.alarm.get("isClick"),
      commuteView: state.list.get('changeView'),
      workAttitudeView: state.list.get('changeView'),

      preGoTo: state.commute.get("preGoTo"),
      preGoOff: state.commute.get("preGoOff"),
      cnt: state.commute.get("cnt"),

      totalWorkTime : state.time.get("totalWorkTime"),
      start : state.time.get("start"),
      time: state.time.get("time"),
      end : state.time.get("end"),
      starttime : state.time.get("starttime"),
      endtime:state.time.get("endtime")
    }),
    { signOut, click, inittime, toggleSidebar, getNotice, setNoticeStore, changeLanguage }),
  withState('mailMenu', 'setMailMenu', null),
  withState('isMailsUnread', 'setIsMailsUnread', true),
  withState('notificationsMenu', 'setNotificationsMenu', null),
  withState('isNotificationsUnread', 'setIsNotificationsUnread', true),
  withState('profileMenu', 'setProfileMenu', null),
  withState('isSearchOpen', 'setSearchOpen', false),
  withHandlers({
    // 다국어 열기
    openMailMenu: props => event => {
      props.setMailMenu(event.currentTarget);
      props.setIsMailsUnread(false);
    },
    // 다국어 닫기
    closeMailMenu: props => () => {
      props.setMailMenu(null);
    },
    // 언어 변경 sessionStorage로 언어 변경시에도 유지
    onChangeLanguage: props => (message) => {
      if(sessionStorage.getItem("language")) {
        sessionStorage.removeItem("language");
        if(sessionStorage.getItem("language") === null) {
          sessionStorage.setItem("language", message);
        }
      }
      props.changeLanguage(message);
      props.setMailMenu(null);
    },
    // 알림 열기 - 알림 Icon 클릭시
    openNotificationsMenu: props => event => {
      props.setNotificationsMenu(event.currentTarget);
      props.setIsNotificationsUnread(false);
    },
    // 알림 닫기
    closeNotificationsMenu: props => () => {
      props.setNotificationsMenu(null);  
    },
    toggleSearch: props => () => {
      props.setSearchOpen(!props.isSearchOpen);
    },
    openProfileMenu: props => event => {
      props.setProfileMenu(event.currentTarget);
    },
    closeProfileMenu: props => () => {
      props.setProfileMenu(null);
    },
    logouthandle: props => () => {
      props.setNoticeStore();
      props.signOut(props.token);
      props.inittime();
    }
  }),
  lifecycle({
    componentDidMount() {
      // KJS: requet가 많아서 알림 주석 처리 했습니다.

      const { getNotice, auth, no, token, isLogging} = this.props;

       if(auth === 'ROLE_ADMIN' && isLogging) {
         getNotice(auth, no, token);
    
        const repeat = setInterval(function() {
          if(sessionStorage.getItem("id_token")) {
            getNotice(auth, no, token);
          }
          if(!sessionStorage.getItem("id_token")) {
            clearInterval(repeat);
          }
        }, 2000);
       }
    
      if(auth === 'ROLE_USER' && isLogging) {
         getNotice(auth, no, token);
    
        const repeat = setInterval(function() {
          if(sessionStorage.getItem("id_token")) {
            getNotice(auth, no, token);
          }
          if(!sessionStorage.getItem("id_token")) {
            clearInterval(repeat);
          }
        }, 2000);
       }
    }
  })
)(HeaderView);