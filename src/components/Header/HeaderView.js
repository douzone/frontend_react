import React, { Component } from "react";
import {
  AppBar,
  Toolbar,
  IconButton,
  Menu,
  MenuItem,
  withStyles
} from "@material-ui/core";
import {
  ExitToApp as ExitToAppIcon,
  NotificationsNone as NotificationsIcon,
  Language as LanguageIcon,
} from "@material-ui/icons";
import { fade } from "@material-ui/core/styles/colorManipulator";
import classNames from "classnames";
import { Badge, Typography } from "../Wrappers";
import Notice from "components/Header/Notice/Notice";
import Time from "components/Header/Time/Time";
import UserAvatar from "../UserAvatar";
import locale from 'locale';
import logo from './logo.PNG';
const worlds = [
  {
    id: 0,
    language: "한국어",
    name: "ko"
  },
  {
    id: 1,
    language: "English",
    name: "en",
  }
];

const Header = ({ auth, isClick, language, tables, isInit, id, name, classes, isSidebarOpened, toggleSidebar, ...props }) => (
  <AppBar position="fixed" className={classes.appBar}>
    <Toolbar className={classes.toolbar}>
    <Typography variant="h6" weight="medium" className={classes.logotype} style={{marginLeft: 16}}>
      <img src={logo} style={{width:240, height:40}} alt="logo"/>
    </Typography>       

      {/* 알림 Icon Button */}
      <div className={classes.grow}>
        <div className={classes.alarm}>
        <Typography variant="h6" weight="medium" id="headerName" className={classNames(classes.logotype, classes.avatar)}>
          <strong>{name}</strong>
        </Typography>
        <IconButton
          color="inherit"
          aria-haspopup="true"
          aria-controls="mail-menu"
          onClick={props.openNotificationsMenu}
          className={classes.headerMenuButton}
        >
          <Badge
            badgeContent={
              sessionStorage.getItem("alarmnum") !== null ? sessionStorage.getItem("alarmnum") : null
            }
            color="warning"
          >
            <NotificationsIcon classes={{ root: classes.headerIcon }} />
          </Badge>
        </IconButton>
        </div>
      </div>

      {/* 다국어 Icon Button */}
      <IconButton
        color="inherit"
        aria-haspopup="true"
        aria-controls="mail-menu"
        onClick={props.openMailMenu}
        className={classes.headerMenuButton}
      >
        <LanguageIcon classes={{ root: classes.headerIcon }} />
      </IconButton>

      {/* 사용자 Icon Button -> 로그아웃 Icon으로 변경 예정  */}
      <IconButton
        aria-haspopup="true"
        color="inherit"
        className={classes.headerMenuButton}
        aria-controls="mail-menu"
        onClick={props.logouthandle}
      >
        <ExitToAppIcon classes={{ root: classes.headerIcon }} />
      </IconButton>
      
      {/* 다국어 메뉴 */}
      <Menu
        id="language-menu"
        open={Boolean(props.mailMenu)}
        anchorEl={props.mailMenu}
        onClose={props.closeMailMenu}
        MenuListProps={{ className: classes.headerMenuList }}
        className={classes.headerMenu}
        classes={{ paper: classes.profileMenu }}
        disableAutoFocusItem
      >
        {worlds.map(world => (
          <div key={world.id}>
          <MenuItem 
          key={world.id} 
          className={classes.messageNotification}
          onClick={() => props.onChangeLanguage(world.name)}>
            <div className={classes.messageNotificationSide}>
              <UserAvatar name={world.name} />
            </div>
            <div
              className={classNames(
                classes.messageNotificationSide,
                classes.messageNotificationBodySide
              )}
            >
              <Typography weight="medium" gutterBottom>
                {world.language}
              </Typography>
            </div>
          </MenuItem>
          </div>
        ))}
      </Menu>

      {/* 알림 메뉴 */}
      <Menu
        id="notifications-menu"
        open={Boolean(props.notificationsMenu)}
        anchorEl={props.notificationsMenu}
        onClose={props.closeNotificationsMenu}
        MenuListProps={{ className: classes.headerMenuList }}
        className={classes.headerMenu}
        classes={{ paper: classes.profileMenu }}
        disableAutoFocusItem
      >
        <div className={classes.profileMenuUser} >
          <Typography variant="h6" weight="medium">
            New Notice
          </Typography>
          <Typography
            className={classes.profileMenuLink}
            component="a"
            color="secondary"
          >
            {locale.alarmText1[language]}
            {sessionStorage.getItem("alarmnum") !== null ? 
              sessionStorage.getItem("alarmnum") : 0} 
            {locale.alarmText2[language]}
          </Typography>
        </div>
        <Notice/>
      </Menu>
    </Toolbar>
  </AppBar>
);

const styles = theme => ({
  logotype: {
    color: "white",
    //marginLeft: theme.spacing.unit * 2.5,
    //marginRight: theme.spacing.unit * 2.5,
    fontWeight: 500,
    fontSize: 18,
    whiteSpace: "nowrap",
    [theme.breakpoints.down("xs")]: {
      display: "none"
    },
    overflow: 'hidden'
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(["margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen
    })
  },
  toolbar: {
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2
  },
  hide: {
    display: "none"
  },
  grow: {
    flexGrow: 1
  },
  alarm: {
    width: "100%",
    position: "relative",
    display: "flex",
    alignItems: "center",
    justifyContent: "flex-end"
  },
  messageContent: {
    display: "flex",
    flexDirection: "column"
  },
  headerMenu: {
    marginTop: theme.spacing.unit * 7
  },
  headerMenuList: {
    display: "flex",
    flexDirection: "column"
  },
  headerMenuItem: {
    "&:hover, &:focus": {
      backgroundColor: theme.palette.primary.main,
      color: "white"
    }
  },
  headerMenuButton: {
    marginLeft: theme.spacing.unit * 2,
    padding: theme.spacing.unit / 2
  },
  headerMenuButtonCollapse: {
    marginRight: theme.spacing.unit * 2
  },
  headerIcon: {
    fontSize: 28,
    color: "rgba(255, 255, 255, 0.35)"
  },
  headerIconCollapse: {
    color: "white"
  },
  profileMenu: {
    minWidth: 200
  },
  profileMenuUser: {
    display: "flex",
    flexDirection: "column",
    padding: theme.spacing.unit * 2
  },
  profileMenuItem: {
    color: theme.palette.text.hint
  },
  profileMenuIcon: {
    marginRight: theme.spacing.unit * 2,
    color: theme.palette.text.hint
  },
  profileMenuLink: {
    fontSize: 16,
    textDecoration: "none",
    "&:hover": {
      cursor: "pointer"
    }
  },
  messageNotification: {
    height: "auto",
    display: "flex",
    alignItems: "center",
    "&:hover, &:focus": {
      backgroundColor: theme.palette.background.light
    }
  },
  messageNotificationSide: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginRight: theme.spacing.unit * 2
  },
  messageNotificationBodySide: {
    alignItems: "flex-start",
    marginRight: 0
  },
  sendMessageButton: {
    margin: theme.spacing.unit * 4,
    marginTop: theme.spacing.unit * 2,
    marginBottom: theme.spacing.unit * 2,
    textTransform: "none"
  },
  sendButtonIcon: {
    marginLeft: theme.spacing.unit * 2
  },
  avatar: {
    width: 64,
    height: 40,
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#687fff",
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15
  }
});

export default withStyles(styles)(Header);
