import Axios from 'axios';
import { List, fromJS } from 'immutable';

export const initialState = {
    keyNo: false,
    isInit: false,
    isLoading: false,
    tables: false,
    error: null
};

export const START_NOTICE = "notice/START_NOTICE";
export const NOTICE_SUCCESS = "notice/NOTICE_SUCCESS";
export const NOTICE_FAILURE = "notice/NOTICE_FAILURE";
export const INIT_NOTICE_STORE = "notice/INIT_NOTICE_STORE";
export const SET_KEYNO = "notice/SET_KEYNO";
export const INIT_KEYNO = "notice/INIT_KEYNO";
export const UPDATE_READ = "notice/UPDATE_READ";

export const startNotice = () => ({
    type: START_NOTICE
});

export const noticeSuccess = (data) => ({
    type: NOTICE_SUCCESS,
    payload : {
        data : data
    }
});

export const noticeFailure = () => ({
    type: NOTICE_FAILURE
});

export const initNoticeStore = () => ({
    type: INIT_NOTICE_STORE
});

export const setKeyNo = (keyNo) => ({
    type: SET_KEYNO,
    payload : {
        keyNo : keyNo
    }
});

export const initKeyNo = () => ({
    type: INIT_KEYNO
});

export const updateRead = () => ({
    type: UPDATE_READ
});

export const noticeupdateRead = (key_no, auth, recordType, token) => {
    return async (dispatch) => {
        await Axios.get('http://218.39.221.104:8080/smartchecker/record/alarm/update/' + key_no + "/" + auth + "/" + recordType, {
            headers : {
                'Authorization' : 'Bearer ' + token
            }
        })
        .then((response) => {
            //console.log(response);
        }).catch((error) => {
            //console.log(error);
        })
    }
}

export const setNoticeKeyNo = (keyNo) => {
    return (dispatch) => {
        dispatch(setKeyNo(keyNo));
    }
} 

export const initNoticeKeyNo = () => {
    return (dispatch) => {
        dispatch(initKeyNo());
    }
}

export const setNoticeStore = () => {
    return (dispatch) => {
        dispatch(initNoticeStore());
    }
};

export const getNotice = (auth, user_no, token) => {
    return async (dispatch) => {
        dispatch(startNotice());
        if(token !== false) {
            await Axios.get('http://218.39.221.104:8080/smartchecker/record/alarm/' + auth + '?user_no=' + user_no, {
                headers : {
                    'Authorization' : 'Bearer ' + token
                }
            })
            .then((response) => {
                if(sessionStorage.getItem("alarmnum") === null) {
                    sessionStorage.setItem("alarmnum", response.data.data.length);
                } else if(sessionStorage.getItem("alarmnum") !== response.data.data.length && sessionStorage.getItem("alarmnum") !== null) {
                    sessionStorage.removeItem("alarmnum");

                    if(sessionStorage.getItem("alarmnum") === null) {
                        sessionStorage.setItem("alarmnum", response.data.data.length);
                    }
                }
                dispatch(noticeSuccess(fromJS(response.data.data)));
            })
            .catch((error) => {
                //console.log(error);
            })
        }
    }
};

export default function NoticeReducer(state = initialState, { type, payload}) {
    switch (type) {
        case START_NOTICE:
        return {
            ...state,
            isLoading: true
        };
        case NOTICE_SUCCESS:
        return {
            ...state,
            isLoading: false,
            isInit: true,
            tables: payload.data,
            error: null
        };
        case NOTICE_FAILURE:
        return {
            ...state,
            isInit: false,
            error: true
        };
        case INIT_NOTICE_STORE:
        return {
            ...state,
            keyNo: false,
            isInit: false,
            tables: false,
            error: null
        };
        case SET_KEYNO:
        return {
            ...state,
            keyNo: payload.keyNo
        };
        case INIT_KEYNO:
        return {
            ...state,
            keyNo: false
        };
        case UPDATE_READ:
        return {
            ...state
        };
        default:
        return state;
    }
}