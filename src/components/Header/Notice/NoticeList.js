import React, { Component } from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { noticeupdateRead } from "./NoticeListState";
import { CircularProgress, MenuItem } from "@material-ui/core";
import { Typography } from 'components/Wrappers';
import classNames from "classnames";
import UserAvatar from 'components/UserAvatar';
import { Divider } from 'antd';
import { click } from 'store/modules/alarm';

class NoticeList extends Component {
    constructor(props) {
        super(props);
        this.handleClick = this.handleClick.bind(this);
    }

    // 클릭 시 현재 데이터 storage에 저장
    handleClick(e, keyNo, day, actor, recordType) {
        const { noticeupdateRead, token, auth, click } = this.props;
        if(sessionStorage.getItem("listkeyNo") && sessionStorage.getItem("statekeyNo") && sessionStorage.getItem("pagekeyNo")){
            sessionStorage.removeItem("listkeyNo");
            sessionStorage.removeItem("statekeyNo");
            sessionStorage.removeItem("pagekeyNo");
            if(!sessionStorage.getItem("listkeyNo") && !sessionStorage.getItem("statekeyNo") && !sessionStorage.getItem("pagekeyNo"))
            {
                sessionStorage.setItem("listkeyNo", keyNo);
                sessionStorage.setItem("statekeyNo", keyNo);
                sessionStorage.setItem("pagekeyNo", keyNo);
                noticeupdateRead(keyNo, auth, recordType, token);
            }
        }
    }

   render() {
    const { tables, isInit, isLoading, language } = this.props;
    
    const Notice = tables.map(
        (table, index) => {
            const{ keyNo, day, actor, content, contentEn, recordType } = table.toJS();
            
            return (
                <div key={index} >
                    <Divider style={{margin: '8px 0'}} />
                    <NavLink
                     onClick={(e) => {this.handleClick(e, keyNo, day, actor, recordType)}} 
                        to={
                        (recordType.includes("근태 신청") || recordType.includes("근태 수정")) ?
                        "/app/workAttitude" : recordType.includes("출퇴근 수정") ?
                        "/app/commute" : '#'
                        }>
                        <MenuItem key={index} className={styles.messageNotification} >
                            <div className={styles.messageNotificationSide} style={{width: 48}}>
                            <UserAvatar color={"success"} name={recordType} />
                                <Typography size="sm" color="textSecondary">
                                    {day.substr(5,10)}
                                </Typography>
                            </div>
                            <div
                                className={classNames(
                                    styles.messageNotificationSide,
                                    styles.messageNotificationBodySide
                                )}>
                                    <Typography weight="medium" gutterBottom>
                                        {actor}
                                    </Typography>
                                    <Typography color="textSecondary">
                                        {language === "en" ? contentEn : content}
                                    </Typography>     
                            </div>
                        </MenuItem>
                    </NavLink>    
                </div>
            );
        }
    );
        return (
            <div>
                {
                    //통신중이며 store 값이 초기화되지 않았으면, 로딩바생성
                    (isInit === false && isLoading === true) ? 
                    <CircularProgress size={26} /> :
                        //통신 성공했으며, 값이 초기화되면, 알람리스트 개수 비교
                        sessionStorage.getItem("alarmnum") !== tables.size &&
                            //알림리스트 개수가 변경되지 않았을 경우 
                            sessionStorage.getItem("alarmnum") !== '0' ?
                                //알림 리스트 개수가 0이 아니면 알림 list 출력
                                Notice :
                                // 알림 리스트 개수가 0개
                                null
                    
                }
            </div>
        );
    }
}

export default connect(
    state => ({
        isInit: state.notice.isInit,
        isLoading: state.notice.isLoading,
        tables: state.notice.tables,
        error: state.notice.error,
        token: state.login.token,
        isReading: state.notice.isReading,
        language: state.language.language,
        auth: state.login.auth
    }),
    { noticeupdateRead, click }
)(NoticeList);

const styles = theme => ({
    messageNotification: {
        height: "auto",
        display: "flex",
        alignItems: "center",
        "&:hover, &:focus": {
          backgroundColor: theme.palette.background.light
        }
    },
    messageNotificationSide: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      marginRight: theme.spacing.unit * 2
    },
    messageNotificationBodySide: {
      alignItems: "flex-start",
      marginRight: 0
    }
});