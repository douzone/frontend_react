import React, {Component} from 'react';
import {Badge} from 'react-bootstrap';
import {Grid, Table, TableRow, TableHead, TableBody, TableCell} from '@material-ui/core';
import {Button} from "antd";
import locale from 'locale';
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import "../../list/TableList/TableList.css";
import { withStyles } from "@material-ui/core";


class WorkAttitudeList extends Component {

    handleShowModal = (table) => {
        //console.log(table);
        const {handleChangeInput} = this.props;
        //console.log(table.no);
        handleChangeInput({name: 'no', value: table.no});
        handleChangeInput({name: 'userNo', value: table.userNo});

        handleChangeInput({name: 'modal', value: true});
    }

    render() {
        const {tables, language, classes} = this.props;
        const {handleShowModal} = this;
        const TableList = tables.map(
            (table, index) => {
                const {
                    userNo, workAttitudeList, name, applicationDate, day, startDay, endDay,
                    dayEn, workAttitudeListEn, stateEn, content, contentEn, state
                } = table.toJS();
                let {badge, lContent, lWorkAttitudeList, lState, lDay} = '';

                if ('en' === language) {
                    lContent = contentEn;
                    lDay = dayEn;
                    lWorkAttitudeList = workAttitudeListEn;
                    lState = stateEn;
                } else if ('ko' === language) {
                    lContent = content;
                    lDay = day;
                    lWorkAttitudeList = workAttitudeList;
                    lState = state;
                }

                if (lState === locale.Normal[language]) {
                    badge = 'success';
                } else {
                    badge = 'danger';
                }

                return (
                    <TableRow className={classes.tableBody} key={index}hover onClick={event => handleShowModal(table.toJS())}>
                        <TableCell id="userNo" className="pl-3 fw-normal">{userNo}</TableCell>
                        <TableCell>{lWorkAttitudeList}</TableCell>
                        <TableCell id="name">{name}</TableCell>
                        <TableCell>{applicationDate}({lDay})</TableCell>
                        <TableCell>{startDay}<span> ~ </span>{endDay}</TableCell>
                        <TableCell>{lContent}</TableCell>
                        <TableCell><h6><Badge variant={badge}>{lState}</Badge></h6></TableCell>
                    </TableRow>
                )
            }
        );
        return (
            <React.Fragment>
                <div style={divWrapper}>
                    <Button type="link" shape="round" icon="download" size="large">
                        <ReactHTMLTableToExcel
                            id="test-table-xls-button"
                            className="css"
                            table="workAttitudeTable"
                            filename="workAttitudeTableXLS"
                            sheet="workAttitudeTableXLS"
                            buttonText="Download"
                        />
                    </Button>
                </div>
                <Grid item xs={12} style={{marginTop: '10px'}}>
                    <div>
                        <Table id="workAttitudeTable" className="mb-0">
                            <TableHead>
                                <TableRow className={classes.tableHead} >
                                    <TableCell><b>User No</b></TableCell>
                                    <TableCell><b>{locale.WorkAttitudeItem[language]}</b></TableCell>
                                    <TableCell><b>{locale.Name[language]}</b></TableCell>
                                    <TableCell><b>{locale.Apply_period[language]}</b></TableCell>
                                    <TableCell><b>{locale.workAttitudePeriod[language]}</b></TableCell>
                                    <TableCell><b>{locale.WorkAttitudeContents[language]}</b></TableCell>
                                    <TableCell><b>{locale.State[language]}</b></TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {TableList}
                            </TableBody>
                        </Table>
                    </div>
                </Grid>
            </React.Fragment>
        )
    }
}

const divWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end",
}

const styles = theme => ({
    tableHead: {
        backgroundColor: '#fafafa',
        borderTop: '2px solid rgba(224, 224, 224, .5)',
        height: theme.spacing.unit * 6,
        "& th" : {
            borderBottom: '2px solid rgba(224, 224, 224, .5)',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
        },
        "& th:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none',
            width: '10%'
        },
        "& th:nth-child(2)" : {
            width: '10%'
        },
        "& th:nth-child(3)" : {
            width: '5%'
        },
        "& th:nth-child(4)" : {
            width: '15%'
        },
        "& th:nth-child(5)" : {
            width: '20%'
        },
        "& th:nth-child(6)" : {
            width: '30%'
        },
        "& th:nth-child(7)" : {
            width: '10%'
        }
    },
    tableBody: {
        padding: '0px 0px 0px 0px',
        height: theme.spacing.unit * 5,
        "& td" : {
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
            "& #content" : {
                textAlign: 'left'
            }
        },
        "& td:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        },
        "& td:nth-child(6)" : {
            textAlign: 'left',
            paddingLeft: theme.spacing.unit * 3
        }
    },
    tdContent: {
        textAlign:'left'
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(WorkAttitudeList);
