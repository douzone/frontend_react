import React, {Component} from 'react';
import {Button, Row, Col} from 'react-bootstrap';
import * as api from 'lib/api';
import {Button as Btn, Dropdown, Icon, Input, Menu, AutoComplete,message} from 'antd';
import './WorkAttitudeSearch.css';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';
import moment from 'moment';

import { Table, TableRow, TableHead, TableCell, withStyles } from "@material-ui/core";
import classNames from "classnames";


const Option = AutoComplete.Option;
const OptGroup = AutoComplete.OptGroup;

let calendarUserSelect=false;
let tableUserSelect=false;
let selectNo=0;
let onSelect = false;
let select = false;
class WorkAttitudeSeacrch extends Component {

    constructor(props) {
        super(props);
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        this.state = {
            yesterday,
            changeValue:"",
            sWorkAttitude: "",
            arry:[],
            dataSource : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ],
              dataSource2 : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ],
              selectNo:"",
              select:false,
              selectName:""
        }
        this.searchFromDateChange = this.searchFromDateChange.bind(this);
        this.searchToDateChange = this.searchToDateChange.bind(this);
        this.searchCalendarChange = this.searchCalendarChange.bind(this);
    }

    // List 검색 시작 날짜 값 변경
    searchFromDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "searchFromDate";
        onChangeInput({name, value});
    }

    // List 검색 끝 날짜 값 변경
    searchToDateChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "searchToDate";
        onChangeInput({name, value});
    }

    // Calendar 검색 날짜 값 변경
    searchCalendarChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM');
        let name = "searchFromDate";
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const {onChangeInput} = this.props;
        const {value, name} = e.target;
        onChangeInput({name, value});
    }

    handleDropdownChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'searchWorkAttitude';
        const value = e.key;
        //console.log("------->> ", e.item.props.children);
        this.setState({sWorkAttitude: e.item.props.children});
        onChangeInput({name, value});
    }

    handleSubmit = () => {
        const {onChangeInput,onSubmit,auth,language} = this.props;
        if(!select&&auth==="ROLE_ADMIN")
        {
            message.config({top: 100, duration: 2});
            message.info(locale.nameInputValidation[language]);
            return;
        }
        onSubmit(selectNo,true,this.state.changeValue);
        onChangeInput({name: 'searchUserNo', value: this.state.selectNo});
        onChangeInput({name: 'searchUserName', value: this.state.changeValue});

        this._initialization();
    }
    handleSubmit2 = () => {
        const {onSubmit,onChangeInput,searchFromDate,searchToDate,language} = this.props;
        message.config( {top: 100, duration: 2});

        if (searchFromDate <= 0 || searchToDate <= 0 ) {
            message.info(locale.Empty_Period[language]);
            return;
          }
      
          if (searchFromDate > searchToDate) {
            message.info(locale.Time_Longer[language]);
            return;
          }
        
        onSubmit(this.state.selectNo,tableUserSelect,this.state.changeValue);
        onChangeInput({name: 'searchUserNo', value: this.state.selectNo});
        onChangeInput({name: 'searchUserName', value: this.state.changeValue});

        this._initialization();
    }

    componentWillMount(){
        
        const {token} = this.props;
        let ary =[{title:"회원리스트",children:[]}];
        api.getUserListByName(token).then(response => {
            if(response.data.data != null){
            response.data.data.map((items,index)=>{
                  ary[0].children.push( 
                      {
                        title: items.name+"(" +items.username+")",
                        no:items.no,
                        name:items.name
                      }

                  );                             
            })
            this.setState({
                arry:
                    ary               
            })
        }
        });
    }
    _onChange =(value)=>{
        // this.setState( {
        //     dataSource2 : [
        //         {
        //          title: '회원리스트',
        //           children: [
                   
        //           ]
        //         }
        //       ]})
        //console.log("change;")
        this.setState({
            changeValue : value
        })
        let ary =[{title:"회원리스트",children:[]}];
        if(value != ""&& this.state.arry[0] != null){
           this.state.arry[0].children.map((items,index)=>{
                if(items.title.includes(value)){
                    ary[0].children.push( 
                        {
                          title: items.title,
                          no:items.no,
                          name:items.name      
                        }
                    )
                }
           })

        }

        this.setState({
            dataSource:
                ary   
        })
        if(calendarUserSelect){
        this.setState({
            dataSource : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ],
            selectNo:"",
            select:false,
            selectName:""
        })
        calendarUserSelect=false;
       
        select=true;
    }
    else
    select=false;
  

    }
    _onChange2 =(value)=>{
        // this.setState( {
        //     dataSource : [
        //         {
        //          title: '회원리스트',
        //           children: [
                   
        //           ]
        //         }
        //       ]})
        this.setState({
            changeValue : value
        })
        let ary =[{title:"회원리스트",children:[]}];
        if(value != ""&& this.state.arry[0] != null){
           this.state.arry[0].children.map((items,index)=>{
                if(items.title.includes(value)){
                    ary[0].children.push( 
                        {
                          title: items.title,
                          no:items.no,
                          name:items.name      
                        }
                    )
                }
           })

        }

        this.setState({
            dataSource2:
                ary   
        })
        if(tableUserSelect){
        this.setState({
            dataSource : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ]
        })
        if(onSelect){
            onSelect=false;
            }
            else{
                tableUserSelect=false;
            }
    }
  

    }
    _select(value,option,onSubmit){
        calendarUserSelect=true;
        selectNo=value;
        this.setState({
            selectNo:value,
            select:true,
            selectName:option.props.children
        })
        onSubmit(value,true,this.state.changeValue);
        this._initialization();
    }

    _select2(value,option,onSubmit){
        onSelect = true;
        tableUserSelect=true;
        this.setState({
            selectNo:value,
            select:true,
            selectName:option.props.children
        })
        onSubmit(value,true,this.state.changeValue);
        this._initialization();
    }
    _initialization(){
        this.setState( {
            dataSource : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ],
              dataSource2 : [
                {
                 title: '회원리스트',
                  children: [
                   
                  ]
                }
              ]
        })
    }

    render() {
        const { handleDropdownChange, searchToDateChange, searchFromDateChange, searchCalendarChange } = this;
        const {language, searchFromDate, searchToDate, searchWorkAttitude, onSubmit, changeView, auth, classes} = this.props;
        const {Search} = Input;

        const menu = (
            <Menu onClick={handleDropdownChange}>
                <Menu.Item key="전체">{locale.All[language]}</Menu.Item>
                <Menu.Item key="출장">{locale.Business_trip[language]}</Menu.Item>
                <Menu.Item key="외근">{locale.Work_outside[language]}</Menu.Item>
                <Menu.Item key="연차">{locale.Annual_leave[language]}</Menu.Item>
                <Menu.Item key="반차">{locale.Semi_annual_leave[language]}</Menu.Item>
                <Menu.Item id="seminar" key="교육">{locale.Education[language]}</Menu.Item>
            </Menu>
        );
        if(auth==="ROLE_ADMIN")
        return (
            <div>
                <div className={classNames(
                changeView==='calendar'?'hide':'show',
                classes.borderline)}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Date[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={searchFromDate}
                                    onChange={searchFromDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    locale={language}
                                    readOnly={true}
                                    style={{width:'20%', display:'inline-table'}}
                                    />
                                    <div className={classes.hyphen}> - </div>
                                    <DatePickerInput
                                    title={locale.End_date[language]}
                                    selected={searchToDate}
                                    onChange={searchToDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    locale={language}
                                    readOnly={true}
                                    style={{width:'20%', display:'inline-table'}}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Division[language]}</b>
                                </TableCell>
                                <TableCell>
                                <Dropdown overlay={menu} >
                                    <Btn >
                                        {searchWorkAttitude==='전체'?locale.All[language]:
                                        searchWorkAttitude==='출장'?locale.Business_trip[language]:
                                        searchWorkAttitude==='외근'?locale.Work_outside[language]:
                                        searchWorkAttitude==='연차'?locale.Annual_leave[language]:
                                        searchWorkAttitude==='반차'?locale.Semi_annual_leave[language]:
                                        searchWorkAttitude==='교육'?locale.Education[language]:''} <Icon type="down" />
                                    </Btn>
                                </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Name[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <AutoComplete
                                    dropdownStyle={{ width: 300 }}
                                    size="large"
                                    style={{ width: '45%' }}
                                    dataSource={this.state.dataSource2
                                        .map(group => (
                                        <OptGroup key={"회원리스트"}>
                                            {group.children.map(opt => (
                                            <Option id="employee" key={opt.no} value={opt.no}>
                                                {opt.title}
                                            </Option>
                                            ))}
                                        </OptGroup>
                                        ))}
                                    onChange={(value)=>this._onChange2(value)}
                                    onSelect={(value,option)=>this._select2(value,option,onSubmit)}
                                    >
                                    <Search id="listSearch" placeholder={locale.Employee_name[language]} />
                                    </AutoComplete>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                        <Button style={{width:80}} id="btnListSearch" onClick={this.handleSubmit2} theme="outline">
                            {locale.Search[language]}
                        </Button>
                    </div>
                </div>

                <div className={classNames(
                changeView==='calendar'?'show':'hide',
                classes.borderline)}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Date[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={searchFromDate}
                                    onChange={searchCalendarChange}
                                    displayFormat='YYYY-MM'
                                    returnFormat='YYYY-MM'
                                    locale={language}
                                    startMode='month'
                                    fixedMode={true}
                                    style={{width:'20%'}}
                                    defaultValue={new Date()}
                                    readOnly={true}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Division[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu} >
                                        <Btn>
                                            {searchWorkAttitude==='전체'?locale.All[language]:
                                            searchWorkAttitude==='출장'?locale.Business_trip[language]:
                                            searchWorkAttitude==='외근'?locale.Work_outside[language]:
                                            searchWorkAttitude==='연차'?locale.Annual_leave[language]:
                                            searchWorkAttitude==='반차'?locale.Semi_annual_leave[language]:
                                            searchWorkAttitude==='교육'?locale.Education[language]:''} <Icon type="down" />
                                        </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Name[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <AutoComplete
                                    dropdownStyle={{ width: 300 }}
                                    size="large"
                                    style={{ width: '45%' }}
                                    dataSource={this.state.dataSource
                                        .map(group => (
                                        <OptGroup key={"회원리스트"}>
                                            {group.children.map(opt => (
                                            <Option id="calendarEmployee" key={opt.no} value={opt.no}>
                                                {opt.title}
                                            </Option>
                                            ))}
                                        </OptGroup>
                                        ))}
                                    onChange={(value)=>this._onChange(value)}
                                    onSelect={(value,option)=>this._select(value,option,onSubmit)}
                                    >
                                    <Search id="calendarSearch" placeholder={locale.Employee_name[language]} />
                                    </AutoComplete>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                    <Button style={{width:80}} onClick={this.handleSubmit} theme="outline">
                        {locale.Search[language]}
                    </Button>
                    </div>
                </div>
            </div>
          );
          else
          return (
            <div>
                <div className={classNames(
                changeView==='table'?'show':'hide',
                classes.borderline)}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Date[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={searchFromDate}
                                    onChange={searchFromDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    locale={language}
                                    readOnly={true}
                                    style={{width:'20%', display:'inline-table'}}
                                    />
                                    <div className={classes.hyphen}> - </div>
                                    <DatePickerInput
                                    title={locale.End_date[language]}
                                    selected={searchToDate}
                                    onChange={searchToDateChange}
                                    displayFormat='YYYY-MM-DD'
                                    returnFormat='YYYY-MM-DD'
                                    locale={language}
                                    readOnly={true}
                                    style={{width:'20%', display:'inline-table'}}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Division[language]}</b>
                                </TableCell>
                                <TableCell>
                                <Dropdown overlay={menu} >
                                    <Btn>
                                        {searchWorkAttitude==='전체'?locale.All[language]:
                                        searchWorkAttitude==='출장'?locale.Business_trip[language]:
                                        searchWorkAttitude==='외근'?locale.Work_outside[language]:
                                        searchWorkAttitude==='연차'?locale.Annual_leave[language]:
                                        searchWorkAttitude==='반차'?locale.Semi_annual_leave[language]:
                                        searchWorkAttitude==='교육'?locale.Education[language]:''} <Icon type="down" />
                                    </Btn>
                                </Dropdown>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                        <Button style={{width:80}} id="btnSearch" onClick={this.handleSubmit2} theme="outline">
                            {locale.Search[language]}
                        </Button>
                    </div>
                </div>

                <div className={classNames(
                changeView==='calendar'?'show':'hide',
                classes.borderline)}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Date[language]}<b style={{color: 'red'}}>*</b></b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                    title={locale.Start_date[language]}
                                    selected={searchFromDate}
                                    onChange={searchCalendarChange}
                                    displayFormat='YYYY-MM'
                                    returnFormat='YYYY-MM'
                                    locale={language}
                                    startMode='month'
                                    fixedMode={true}
                                    style={{width:'20%'}}
                                    defaultValue={new Date()}
                                    readOnly={true}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.Division[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu} >
                                        <Btn>
                                            {searchWorkAttitude==='전체'?locale.All[language]:
                                            searchWorkAttitude==='출장'?locale.Business_trip[language]:
                                            searchWorkAttitude==='외근'?locale.Work_outside[language]:
                                            searchWorkAttitude==='연차'?locale.Annual_leave[language]:
                                            searchWorkAttitude==='반차'?locale.Semi_annual_leave[language]:
                                            searchWorkAttitude==='교육'?locale.Education[language]:''} <Icon type="down" />
                                        </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                    <div style={{textAlign:'-webkit-right', marginTop:8}}>
                        <Button id="btnUserCalendarSearch" style={{width:80}} onClick={this.handleSubmit} theme="outline">
                            {locale.Search[language]}
                        </Button>
                    </div>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    borderline: {
        borderTop: '2px solid rgba(224, 224, 224, .5)'
    },
    tableHead: {
        "& tr" : {
            height: '48px'
        },
        "& th:first-of-type" : {
            width:'10%', 
            backgroundColor: '#fafafa',
            padding: '0px 0px 0px 16px',
        },
        "& tr:last-of-type": {
            "& th": {
                borderBottom: '2px solid rgba(224, 224, 224, .5)'
            }
        }
    },
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        padding: '8px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    },
    hyphen: {
        width:'5%', 
        display:'inline-table', 
        textAlign: 'center'
    }
});

export default withStyles(styles)(WorkAttitudeSeacrch);