import React, {Component} from 'react';
import {Grid} from '@material-ui/core';
import Widget from 'components/Widget';
import {PieChart, Pie, ResponsiveContainer, Sector} from "recharts";
import { connect } from 'react-redux';
import locale from 'locale';


class WorkAttitudeState extends Component {

    render() {
        const {tables} = this.props;

        let normal;
        let error;
        let PieChartData;

        //console.log(tables);

        if(tables.size === 0) {
            normal = 1;
            error = 0;

            PieChartData = [
                {name: 0, value: normal, color: "primary"},
                {name: 0, value: error, color: "success"}
            ];
        }
        else{
            normal = tables.get("정상");
            error = tables.get("에러");

            PieChartData = [
                {name: normal, value: normal, color: "primary"},
                {name: error, value: error, color: "success"}
            ];

            if(normal === 0 && error === 0){
                normal = 1;

                PieChartData = [
                    {name: 0, value: normal, color: "primary"},
                    {name: 0, value: error, color: "success"}
                ];
            }
        }

        const renderActiveShape = (props) => {
            const {
                cx, cy, innerRadius, outerRadius, startAngle, endAngle,
                fill, payload
            } = props;

            return (
                <g>
                    <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };
        const {language } = this.props;

        return (
            <React.Fragment>
                <br/>

                <Grid container spacing={32}>
                    <Grid item lg={6} md={5} sm={6} xs={12}>
                        <Widget title={locale.Normal[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144} style={{margin:'0 auto'}}>
                                <Grid container spacing={16} style={{margin:'0 auto', width:'100%'}}>
                                    <Grid item sm={6} style={{margin:'0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144} style={{margin:'0 auto'}}>
                                                <Pie
                                                    activeIndex={0}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#28a745'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item lg={6} md={4} sm={6} xs={12}>
                        <Widget title={locale.Error[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144} style={{margin:'0 auto'}}>
                                <Grid container spacing={16} style={{margin:'0 auto', width:'100%'}}>
                                    <Grid item sm={6} style={{margin:'0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144} style={{margin:'0 auto'}}>
                                                <Pie
                                                    activeIndex={1}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#dc3545'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                </Grid>

                <br/>
            </React.Fragment>
        )
    }
}

const pieChartLegendWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end"
}

export default connect(
    (state) => ({
        language: state.language.language
    }),
)(WorkAttitudeState);