import React, { Component } from 'react';
import locale from 'locale';
import { withStyles } from "@material-ui/core";
import { Typography } from "components/Wrappers";
import {
    Home as HomeIcon
} from "@material-ui/icons";

class Title extends Component {
    
    render() {
        const { language, classes, auth } = this.props;
        let title, path = null;
        if(window.location.href.includes('list')) {
            title = locale.workAttitudeList[language];
            path = locale.workAttitudeManage[language] + " > " + locale.workAttitudeList[language];
        } else {
            title = locale.workAttitudeApply[language];
            path = locale.workAttitudeManage[language] + " > " + locale.workAttitudeApply[language];
        }
        return(
            <div>
                <div className={classes.pageTitleContainer}>
                    <Typography variant="h2" size="sm">
                        <strong id="workAttitudeTitle">{title}</strong>
                    </Typography>
                    <Typography className={classes.dirTypo} variant="h6" size="sm">
                        <HomeIcon />
                        <span> > </span>
                        {auth==="ROLE_USER" ? path : locale.workAttitudeList[language]}
                    </Typography>
                </div>
            </div>
        )
    }
}

const styles = theme => ({
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        backgroundColor: '#efeaea',
        padding: '10px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(Title);