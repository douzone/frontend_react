import React, {Component} from 'react';
import {Grid} from '@material-ui/core';
import Widget from 'components/Widget';
import {PieChart, Pie, ResponsiveContainer, Sector} from "recharts";
import locale from 'locale';


class WorkAttitudeCalendarState extends Component {

    render() {
        const {calendarTables, language} = this.props;

        //console.log(calendarTables);

        let total;
        let trip;
        let outSide;
        let annual;
        let education;
        let PieChartData = [];

        if (calendarTables.size === 0) {
            total = 1;
            trip = 0;
            outSide = 0;
            annual = 0;
            education = 0;

            PieChartData = [
                {name: 0, value: total, color: "primary"},
                {name: 0, value: trip, color: "secondary"},
                {name: 0, value: outSide, color: "secondary"},
                {name: 0, value: annual, color: "secondary"},
                {name: 0, value: education, color: "secondary"}
            ];
        } else {
            total = calendarTables.get("전체");
            trip = calendarTables.get("출장");
            outSide = calendarTables.get("외근");
            annual = calendarTables.get("연차");
            education = calendarTables.get("교육");

            PieChartData = [
                {name: total, value: total, color: "primary"},
                {name: trip, value: trip, color: "secondary"},
                {name: outSide, value: outSide, color: "secondary"},
                {name: annual, value: annual, color: "secondary"},
                {name: education, value: education, color: "secondary"}
            ];

            if (total === 0 && trip === 0 && outSide === 0 && annual === 0 && education === 0) {
                total = 1;

                PieChartData = [
                    {name: 0, value: total, color: "primary"},
                    {name: 0, value: trip, color: "secondary"},
                    {name: 0, value: outSide, color: "secondary"},
                    {name: 0, value: annual, color: "secondary"},
                    {name: 0, value: education, color: "secondary"}
                ];
            }
        }


        if (calendarTables.size !== 0) {

        }

        const renderActiveShape = (props) => {
            const {
                cx, cy, innerRadius, outerRadius, startAngle, endAngle,
                fill, payload
            } = props;

            return (
                <g>
                    <text x={cx} y={cy} dy={8} textAnchor="middle" fill={fill}>{payload.name}</text>
                    <Sector
                        cx={cx}
                        cy={cy}
                        innerRadius={innerRadius}
                        outerRadius={outerRadius}
                        startAngle={startAngle}
                        endAngle={endAngle}
                        fill={fill}
                    />
                </g>
            );
        };

        return (
            <React.Fragment>

                <br/>

                <Grid container spacing={32}>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.All[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={0}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#6c757d'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Business_trip[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={1}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#28a745'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Work_outside[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={2}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#ffc107'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Annual_leave[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={3}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#dc3545'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                    <Grid item style={{width: '20%'}}>
                        <Widget title={locale.Education[language]} noBodyPadding upperTitle>
                            <ResponsiveContainer width="100%" height={144}>
                                <Grid container spacing={16}>
                                    <Grid item sm={6} style={{margin: '0 auto'}}>
                                        <div style={pieChartLegendWrapper}>
                                            <PieChart width={160} height={144}>
                                                <Pie
                                                    activeIndex={4}
                                                    activeShape={renderActiveShape}
                                                    data={PieChartData}
                                                    innerRadius={45}
                                                    outerRadius={60}
                                                    fill={'#007bff'}
                                                    dataKey="value"
                                                />
                                            </PieChart>
                                        </div>
                                    </Grid>
                                </Grid>
                            </ResponsiveContainer>
                        </Widget>
                    </Grid>
                </Grid>

                <br/>

            </React.Fragment>
        )
    }
}

const pieChartLegendWrapper = {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
}

export default WorkAttitudeCalendarState;