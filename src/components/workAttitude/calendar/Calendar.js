import React, { Component } from "react";
import FullCalendar from 'fullcalendar-reactwrapper';
import 'fullcalendar/dist/fullcalendar.css';
import './Calendar.css';
import Modal from 'react-awesome-modal';

// axios.defaults.headers.common['Authorization'] = 
//                                 'Bearer ' + localStorage.getItem('jwtToken');

// axios.defaults.headers.common['Authorization'] = 
// 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJ1c2VyIiwiZXhwIjoxNTU2NTAyNDE2LCJpYXQiOjE1NTU4OTc2MTZ9.hK9ujazIT2XpBxLn_kL_gzPEfssICpOurzI8I43D-zkTge0g7MwIReZ_B9y1jNe94gqw71OTnYOliT3AN2ZRbA';
class Calendar extends Component {

    constructor(props) {
      

        super(props);
        
        this.state = {
            date:new Date(),
            events:[]
        }
    }
    openModal(info) {
        const { handleChangeInput } = this.props;
        this.updateRead(info.data.no);
        handleChangeInput({name:"no",value:info.data.no})
        handleChangeInput({name:"modal",value:true})
        handleChangeInput({name:"name",value:info.data.name})
        handleChangeInput({name:"title",value:info.data.title})
        handleChangeInput({name:"startDay",value:info.data.startDay.substring(0,10)})
        handleChangeInput({name:"endDay",value:info.data.endDay.substring(0,10)})
        handleChangeInput({name:"workAttitudeList",value:info.data.workAttitudeList})
        handleChangeInput({name:"content",value:info.data.content})
    }

    updateRead = (no) => {
        //console.log("클릭");
        const {token, auth, workattitude } = this.props;
        const recordType = "근태";
        workattitude.updateRead(no, auth, recordType, token);
    }

    render() {
        let ary=[];
       const{calList,search,language} = this.props;
       if(calList!=null){
           //console.log(search);
        ary= calList.data.map((items,index)=>{
            let color;
             if(items.state === "에러")
            color ='#DC3545';
            else{
                if(items.workAttitudeList === "출장"){
                    color ='#28A745';
                }
                else if(items.workAttitudeList === "외근"){
                    color ='#FFC107';
                }
                else if(items.workAttitudeList === "연차"){
                    color ='#DC3545';
                }
                else if(items.workAttitudeList === "교육"){
                    color ='#007BFF';
                }else if(items.workAttitudeList === "오전반차"){
                    color ='#DC3545';
                }
                else if(items.workAttitudeList === "오후반차"){
                    color ='#DC3545';
                }
                
            }
            if(language === "ko")
        return {
            title:items.workAttitudeList, 
            start: items.startDay,
            end:items.endDay,
            backgroundColor:color,
            data:{
                no:items.no,
                workAttitudeList:items.workAttitudeList,
                title:items.title,
                state:items.state,
                content:items.content,
                name:items.name,
                startDay: items.startDay,
                endDay:items.endDay,
            }}
            else if(language === "en"){
                let titleEn="";
                if(items.workAttitudeList === "출장")
                {
                    titleEn="Business trip"
                }
                else if(items.workAttitudeList === "외근")
                {
                    titleEn="Work outside"
                }
                else if(items.workAttitudeList === "연차")
                {
                    titleEn="Annual leave"
                }
                else if(items.workAttitudeList === "반차")
                {
                    titleEn="Semi-annal leave"
                }
                else if(items.workAttitudeList === "교육")
                {
                    titleEn="Education"
                }
                return {
                    title:titleEn, 
                    start: items.startDay,
                    end:items.endDay,
                    backgroundColor:color,
                    data:{
                        no:items.no,
                        workAttitudeList:titleEn,
                        title:titleEn,
                        state:items.stateEn,
                        content:items.contentEn,
                        name:items.name,
                        startDay: items.startDay,
                        endDay:items.endDay,
                    }}
            }
        //user_no:items.user_no,commute_no:items.commute_no,name:items.name,id:items.id,commute:items.commute,time:items.day,backgroundColor:color
    });
    }
        return (
            <div id="example-component">
                <FullCalendar
                    id = "your-custom-ID"
                    header = {{
                        left: 'none',
                        center: 'title',
                        right: 'none'
                    }}
                    defaultDate={search.searchFromDate === ""?this.state.date:search.searchFromDate}
                    locale={language}
                    navLinks= {true} // can click day/week names to navigate views
                    editable= {false}
                    eventLimit= {true} // allow "more" link when too many events
                    titleFormat={'YYYY-MM'}
                    events = {ary}
                    timeFormat = {"HH:mm"}
                    eventClick={ (info)=>{

                        return this.openModal(info)
                        
                    }
                    
                    }
                    displayEventTime={false}
                    
                />
               {/* <Modal visible={this.state.visible} width="300" height="300" effect="fadeInUp" onClickAway={() => this.closeModal()}>
                    <div className = "Modal"><br/>
                        <h1>{this.state.modal.title}</h1>
                        <br/>
                        <p>이름: {this.state.modal.name}</p>
                        <p>아이디: {this.state.modal.id}</p>
                        <p>시간 : {this.state.modal.time}</p>
                        <a href=" " onClick={() => this.closeModal()}>수정하기</a>
                        &nbsp;&nbsp;&nbsp;
                        <a href=" " onClick={() => this.closeModal()}>닫기</a>
                    </div>
                </Modal> */}
            </div>
        );
    }

}




export default Calendar;