import React, { Component } from 'react';
import { Radio, Divider } from 'antd';
import locale from 'locale';

const RadioButton = Radio.Button;
const RadioGroup = Radio.Group;

class CalendarTableSelect extends Component {

  handleChange = (e) => {

    //console.log(e.target.value);
    const { onChangeInput } = this.props;
    onChangeInput({name: 'changeView', value: e.target.value});
  }

  render() {
    const {changeView, language} = this.props;
    const {handleChange} = this;

    return (
      <div>
      <Divider orientation="left">
        <RadioGroup onChange={handleChange} value={changeView}>
          <RadioButton value="table">{locale.Table[language]}</RadioButton>
          <RadioButton value="calendar">{locale.Calendar[language]}</RadioButton>
        </RadioGroup>
      </Divider>        
      </div>
    );
  }
}

export default CalendarTableSelect;