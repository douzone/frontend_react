import React, { Component } from "react";
import { Button, Row, Col, Form,Modal } from "react-bootstrap";
import { Button as Btn ,Dropdown,Input, message,Icon,Menu } from "antd";
import moment from "moment";
import locale from "locale";
import { DatePickerInput } from "rc-datepicker";
import "rc-datepicker/lib/style.css";
import "moment/locale/en-ca";
import "moment/locale/ko";

import { Table, TableRow, TableHead, TableCell, withStyles, Typography } from "@material-ui/core";
import classNames from "classnames";

let lan ="";
let a = true;
class WorkAttitudeAdd extends Component {
    constructor(props) {
        super(props);
        const yesterday = new Date();
        yesterday.setDate(yesterday.getDate() - 1);
        this.state = {
            yesterday
        };
        this.startDayChange = this.startDayChange.bind(this);
        this.endDayChange = this.endDayChange.bind(this);
    }

    // 신청 시작 날짜 값 변경
    startDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format("YYYY-MM-DD");
        let name = "startDay";
        onChangeInput({ name, value });
        a=false;
    }

    // 신청 끝 날짜 값 변경
    endDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format("YYYY-MM-DD");
        let name = "endDay";
        onChangeInput({ name, value });
        a=false;
    }

    handleChange = e => {
        const { onChangeInput,language,LanActions } = this.props;
        const { value, name } = e.target;
        onChangeInput({ name, value });
        a=false;
        //console.log(lan);
        if(language=="ko"){
            LanActions.changeLanguage("ko");
            lan="ko"
        }
        else if(language=="en"){
            LanActions.changeLanguage("en");
            lan="en"
        }
    };


    submitCheck = () => {
        const {
            startDay,
            endDay,
            title,
            content,
            language,
            onSubmit,
            loginName,
            onChangeInput
        } = this.props;
        message.config( {top: 100, duration: 2});
        if (startDay <= 0 || endDay <= 0) {
            message.info(locale.Empty_Period[language]);
            return;
        }

        if (startDay > endDay) {
            message.info(locale.Time_Longer[language]);
            return;
        }

        if (title <= 0) {
            message.info(locale.Empty_Subject[language]);
            return;
        }

        if (content <= 0) {
            message.info(locale.Empty_Present[language]);
            return;
        }

        onChangeInput({ name: "name", value: loginName });
        onSubmit();
    };

    openModal = () => {
        const { onChangeInput } = this.props;
        let name= "workAttitudeAddModal";
        let value =true;
        onChangeInput({name, value});

    };
    //취소버튼 눌렀을때
    cancelButtonClick= () => {
        const { onChangeInput,LanActions,language } = this.props;
        let name= "workAttitudeAddModal";
        let value =false;
        onChangeInput({name, value});

        if(language=="ko"){
            LanActions.changeLanguage("en");
            lan="en"
        }
        else if(language=="en"){
            LanActions.changeLanguage("ko");
            lan="ko"
        }

        a=false;


    };
    //확인버튼 눌렀을때
    okButtonClick= () => {
        const { onChangeInput} = this.props;
        let name= "workAttitudeAddModal";
        let value =false;
        onChangeInput({name, value});

        name="title";
        value="";
        onChangeInput({name, value});

        name="content";
        value="";
        onChangeInput({name, value});

    };


    handleDropdownChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'add';
        const value = e.key;
        //console.log("------->> ", e.item.props.children);
        // this.setState({sWorkAttitude: e.item.props.children});
         onChangeInput({name, value});
    }
    render() {

    const { handleChange, submitCheck,okButtonClick, startDayChange, endDayChange,openModal,cancelButtonClick } = this;
    const { TextArea } = Input;

    const {
      startDay,
      endDay,
      title,
      gubun,
      content,
      loginName,
      language,
      workAttitudeAddModal,
      classes,
      add
    } = this.props;

    const menu = (
      <Menu onClick={this.handleDropdownChange}>
          <Menu.Item key="출장">{locale.Business_trip[language]}</Menu.Item>
          <Menu.Item key="외근">{locale.Work_outside[language]}</Menu.Item>
          <Menu.Item key="연차">{locale.Annual_leave[language]}</Menu.Item>
          <Menu.Item key="오전반차">{locale.morningSemiAnnualLeave[language]}</Menu.Item>
          <Menu.Item key="오후반차">{locale.afternoonSemiAnnualLeave[language]}</Menu.Item>  
          <Menu.Item id="seminar" key="교육">{locale.Education[language]}</Menu.Item>
      </Menu>
  );

   // //console.log("lan")
    if(language == "ko" && lan != language && (title!="" || content !="") &&a)
    {
        openModal();
        lan = language;
       // //console.log(lan);
    }
    else if(language == "en" && lan != language && (title!="" || content !="")&& a)
    {
        openModal();
        lan = language;
      //  //console.log(lan);
    }
    a=true;



    return (
      <div className={classes.borderline}>
        <Table className="mb-0">
          <TableHead className={classes.tableHead}>
            <TableRow>
              <TableCell>
                <b>{locale.Name[language]}</b>
              </TableCell>
              <TableCell>
                <Form.Control
                placeholder="홍길동"
                type="text"
                value={loginName}
                disabled={true} 
                onChange={handleChange}
                name="name"
                style={{width:"45%"}}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <b>{locale.Period[language]}<b style={{color: 'red'}}>*</b></b>
              </TableCell>
              <TableCell>
                <DatePickerInput
                title={locale.Start_date[language]}
                value={startDay}
                selected={startDay}
                onChange={startDayChange}
                displayFormat="YYYY-MM-DD"
                returnFormat="YYYY-MM-DD"
                locale={language}
                readOnly={true}
                style={{width:'20%', display:'inline-table'}}
                />
                <div className={classes.hyphen}> - </div>
                <DatePickerInput
                title={locale.End_date[language]}
                selected={endDay}
                value={endDay}
                onChange={endDayChange}
                displayFormat="YYYY-MM-DD"
                returnFormat="YYYY-MM-DD"
                locale={language}
                readOnly={true}
                style={{width:'20%', display:'inline-table'}}
              />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <b>{locale.Title[language]}<b style={{color: 'red'}}>*</b></b>
              </TableCell>
              <TableCell>
                <Form.Control
                id="title"
                type="text"
                value={title}
                onChange={handleChange}
                name="title"
                style={{width:"45%"}}
                />
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <b>{locale.Division[language]}<b style={{color: 'red'}}>*</b></b>
              </TableCell>
              <TableCell>
                {/* <select
                id="gubun"
                name="gubun"
                type="text"
                value={gubun}
                onChange={handleChange}
                >
                  <option value="출장">{locale.Business_trip[language]}</option>
                  <option value="외근">{locale.Work_outside[language]}</option>
                  <option value="연차">{locale.Annual_leave[language]}</option>
                  <option value="오전반차">
                    {locale.morningSemiAnnualLeave[language]}
                  </option>
                  <option value="오후반차">
                    {locale.afternoonSemiAnnualLeave[language]}
                  </option>
                  <option id="seminar" value="교육">
                    {locale.Education[language]}
                  </option>
                </select> */}
                <Dropdown overlay={menu} >
                                    <Btn >
                                        {
                                        add==='출장'?locale.Business_trip[language]:
                                        add==='외근'?locale.Work_outside[language]:
                                        add==='연차'?locale.Annual_leave[language]:
                                        add==='오전반차'?locale.morningSemiAnnualLeave[language]:
                                        add==='오후반차'?locale.afternoonSemiAnnualLeave[language]:
                                        add==='교육'?locale.Education[language]:''
                                        } <Icon type="down" />
                                    </Btn>
                                </Dropdown>
              </TableCell>
            </TableRow>
            <TableRow>
              <TableCell>
                <b>{locale.WorkAttitudeReason[language]}<b style={{color: 'red'}}>*</b></b>
              </TableCell>
              <TableCell>
                <TextArea
                id="content"
                name="content"
                rows={4}
                onChange={handleChange}
                value={content}
                />
              </TableCell>
            </TableRow>
          </TableHead>
        </Table>
        <div style={{textAlign:'-webkit-right', marginTop:8}}>
          <Button style={{width:'80px'}} id="btnSubmit" onClick={submitCheck} theme="outline">
            {locale.Submit[language]}
          </Button>
        </div>
        <div>
          <Modal show={workAttitudeAddModal} centered={true}>
            <Modal.Header closeButton={false} style={{backgroundColor: '#536DFE'}}>
              <Modal.Title id="modal1" style={{color: '#ffffff'}}>
                {locale.Detail[language]}
              </Modal.Title>
            </Modal.Header> 
            <Modal.Body style={{fontWeight: 'bold'}}>
              {locale.Initialized[language]}
            </Modal.Body>
            <Modal.Footer style={{justifyContent: 'unset', display: 'unset'}}>
              <Button variant="primary" onClick={okButtonClick} style={{width: 128}}>
                {locale.Confirm[language]}
              </Button>
              <Button variant="primary" onClick={cancelButtonClick} style={{width: 128}}>
                {locale.Cancel[language]}
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      </div>
    );
  }
}

const styles = theme => ({
    borderline: {
        borderTop: '2px solid rgba(224, 224, 224, .5)'
    },
    tableHead: {
        "& tr" : {
            height: '48px'
        },
        "& th:first-of-type" : {
            width:'10%',
            backgroundColor: '#fafafa',
            padding: '0px 0px 0px 16px',
        },
        "& tr:last-of-type": {
            "& th": {
                borderBottom: '2px solid rgba(224, 224, 224, .5)'
            }
        }
    },
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
            boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        padding: '8px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    },
    hyphen: {
        width:'5%',
        display:'inline-table',
        textAlign: 'center'
    }
});

export default withStyles(styles)(WorkAttitudeAdd);