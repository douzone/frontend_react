import React from "react";
import './AdminMain.css';
import { Progress }  from 'antd';
import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';
import moment from 'moment';
import {
  Grid,
  LinearProgress,
  Select,
  OutlinedInput,
  MenuItem,
  withStyles,
} from "@material-ui/core";
import locale from "locale";
import {
  ResponsiveContainer,
  ComposedChart,
  AreaChart,
  LineChart,
  Line,
  Area,
  PieChart,
  Pie,
  Cell,
  YAxis,
  XAxis,
  CartesianGrid,
  Legend,
  Tooltip
} from "recharts";

import Widget from "../../components/Widget";
import PageTitle from "../../components/PageTitle";
import { Typography } from "../../components/Wrappers";
import Dot from "../../components/Sidebar/components/Dot";



let input={
  product: "Light Blue",
  total: {
    monthly: 4232,
    weekly: 1465,
    daily: 199,
    percent: { value: 3.7, profit: false }
  },
  color: "primary",
  registrations: {
    monthly: { value: 830, profit: false },
    weekly: { value: 215, profit: true },
    daily: { value: 33, profit: true }
  },
  bounce: {
    monthly: { value: 4.5, profit: false },
    weekly: { value: 3, profit: true },
    daily: { value: 3.25, profit: true }
  }
}

const getRandomData = (length, min, max, multiplier = 10, maxDiff = 10) => {
  const array = new Array(length).fill();
  let lastValue;

  return array.map((item, index) => {
    let randomValue = Math.floor(Math.random() * multiplier + 1);

    while (
      randomValue <= min ||
      randomValue >= max ||
      (lastValue && randomValue - lastValue > maxDiff)
    ) {
      randomValue = Math.floor(Math.random() * multiplier + 1);
    }

    lastValue = randomValue;

    return { value: randomValue };
  });
};

const getMainChartData = () => {
  const resultArray = [];
  const tablet = getRandomData(31, 0, 100, 100, 1000);
  const mobile = getRandomData(31, 0, 100, 100, 1500);

  for (let i = 0; i < tablet.length; i++) {
    resultArray.push({
      tablet: tablet[i].value,
      mobile: mobile[i].value
    });
  }

  return resultArray;
};

const mainChartData = getMainChartData();

const PieChartData = [
  { name: "Group A", value: 400, color: "primary" },
  { name: "Group B", value: 300, color: "secondary" },
  { name: "Group C", value: 300, color: "warning" },
  { name: "Group D", value: 200, color: "success" }
];





const AdminMain = ({ classes, theme, ...props }) => {
  
 // //console.log(props);
  const {totalUserCount,gotoWorkCount,tardyCount,chartList,businessTripCount,workOutsideCount,annualLeaveCount,educationCount,searchDate} = props.parm;
  const{language,changeInput} = props;
 
  const searchDateChange=(date)=>{
     let value = moment(date).format('YYYY-MM-DD');
     let name = "searchFromDate";
     changeInput(name, value);
  }
 
 
 
  let gotoWorkRatio;
  if(gotoWorkCount==0 && totalUserCount==0)
   gotoWorkRatio = 100;
   else if(gotoWorkCount==0)
   gotoWorkRatio = 0;
   else {
    gotoWorkRatio=Math.round(gotoWorkCount/totalUserCount*100);
   
  }

 

   let tardyRatio;
   if(tardyCount===0)
    tardyRatio = 0
    else {
      tardyRatio =Math.round(tardyCount/gotoWorkCount*100);
    }

    let businessTripRatio=0;
    let workOutsideRatio=0;
    let AnnualLeaveRatio=0;
    let EducationRatio=0;

    if(businessTripCount===0 && totalUserCount===0)
    businessTripRatio = 100;
    else if(businessTripCount===0)
    businessTripRatio = 0;
    else {
      businessTripRatio=Math.round(businessTripCount/totalUserCount*100);
   }
   if(workOutsideCount===0 && totalUserCount===0)
   workOutsideRatio = 100;
   else if(workOutsideCount===0)
   workOutsideRatio = 0;
   else {
    workOutsideRatio=Math.round(workOutsideCount/totalUserCount*100);
  }

  if(annualLeaveCount===0 && totalUserCount===0)
  AnnualLeaveRatio = 100;
  else if(annualLeaveCount===0)
  AnnualLeaveRatio = 0;
  else {
    AnnualLeaveRatio=Math.round(annualLeaveCount/totalUserCount*100);
 }

 if(educationCount===0 && totalUserCount===0)
 EducationRatio = 100;
 else if(educationCount===0)
 EducationRatio = 0;
 else {
  EducationRatio=Math.round(educationCount/totalUserCount*100);
}




    let lineChartData=[];
    if(chartList!=null)
    {
     // //console.log(chartList);
      chartList.map((item,index)=>{
       // //console.log(item);

 


        let gotoWorkRatioItem;
        let tardyRatioItem;
        if(item.gotoWorkCount===0 && item.totalUserCount===0)
        gotoWorkRatioItem = 100;
        else if(item.gotoWorkCount===0)
        gotoWorkRatioItem = 0;
        else {
          gotoWorkRatioItem=Math.round(item.gotoWorkCount/item.totalUserCount*100);
       }

       if(item.tardyCount==0)
       tardyRatioItem = 0
       else {
        tardyRatioItem =Math.round(item.tardyCount/item.gotoWorkCount*100);
       }
       let ary ;
        if(language == "ko")
         ary = {
          name:item.date,
          지각:tardyRatioItem,
          출근:gotoWorkRatioItem
        }
        else if(language =="en"){
          ary = {
            name:item.date,
            Lateness:tardyRatioItem,
            'Clock-in':gotoWorkRatioItem
          }
        }
        lineChartData.push(ary);
      })
    }

    lineChartData.reverse();

  return (

    <React.Fragment>
      <div className="date">
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <DatePickerInput
                            title={locale.Start_date[language]}
                             //selected={}
                             onChange={searchDateChange}
                            displayFormat='YYYY-MM-DD'
                            returnFormat='YYYY-MM-DD'
                            defaultValue={new Date()}
                            locale={language}
                            //startMode='month'
                            fixedMode={true}
                            readOnly={true}
                        />
          </Grid>
          </div>
      
      <Grid container spacing={32}>
        <Grid item lg={4} md={4} sm={6} xs={12}>
      
            
          <Widget
            title={locale.DashBoard_Today_Commute[language]}
            upperTitle
            bodyClass={classes.fullHeightBody}
            className={classes.card}
          >
            <div> </div>
              <div className="today-commute">
            <div className={classes.visitsNumberContainer}>
              <Typography size="xl" weight="medium">
                
              </Typography>
              <div className="today-progress-tardy">
              {locale.goToWork[language]}<br/>
              <Progress type="circle" percent={gotoWorkRatio} strokeColor="#007BFF" status="normal"/>
              </div>
              <div className="today-progress-tardy">
              {locale.Lateness[language]}<br/>
              <Progress type="circle" percent={tardyRatio} strokeColor="red" status="normal"/>
            </div>
            </div>
            <Grid
              container
              direction="row"
              justify="space-between"
              alignItems="center"
            >
              <Grid item>
                <Typography color="textSecondary">{locale.DashBoard_Employees[language]}</Typography>
                <Typography size="md">{totalUserCount}</Typography>
              </Grid>
              <Grid item>
                <Typography color="textSecondary">{locale.DashBoard_Attendance[language]}</Typography>
                <Typography size="md">{gotoWorkCount}</Typography>
              </Grid>
              <Grid item>
                <Typography color="textSecondary">{locale.DashBoard_Tectonic[language]}</Typography>
                <Typography size="md">{tardyCount}</Typography>
              </Grid>
            </Grid>
            </div>
          </Widget>
        </Grid>

        <Grid item lg={8} md={4} sm={6} xs={12}>
      
        
      <Widget
        title={locale.DashBoard_Today_Workattitude[language]}
        upperTitle
        bodyClass={classes.fullHeightBody}
        className={classes.card}
      >

      
       
        


          <div className="today-workattitude">
        <div className={classes.visitsNumberContainer}>
                
         
           
          <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >     
          <div className="today-progress">
            {locale.Business_trip[language]}
          <br/>
          <Progress type="circle" percent={businessTripRatio} strokeColor="#28A745" status="normal"/>
          </div>
          <div className="today-progress">
          {locale.Work_outside[language]}<br/>
          <Progress type="circle" percent={workOutsideRatio} strokeColor="#FFC107" status="normal"/>
        </div>
        <div className="today-progress">
        {locale.Annual_leave[language]}<br/>
          <Progress type="circle" percent={AnnualLeaveRatio} strokeColor="red" status="normal"/>
        </div>
        <div className="today-progress">
        {locale.Education[language]}<br/>
          <Progress type="circle" percent={EducationRatio} strokeColor="#007BFF" status="normal"/>
        </div>
        </Grid>
        </div>
        
        <Grid
          container
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <Grid item>
            <Typography color="textSecondary">{locale.DashBoard_Employees[language]}</Typography>
            <Typography size="md">{totalUserCount}</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">{locale.DashBoard_Business_trip[language]}</Typography>
            <Typography size="md">{businessTripCount}</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">{locale.DashBoard_Work_outside[language]}</Typography>
            <Typography size="md">{workOutsideCount}</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">{locale.DashBoard_Annual_leave[language]}</Typography>
            <Typography size="md">{annualLeaveCount}</Typography>
          </Grid>
          <Grid item>
            <Typography color="textSecondary">{locale.DashBoard_Education[language]}</Typography>
            <Typography size="md">{educationCount}</Typography>
          </Grid>
        </Grid>
        </div>
      </Widget>
    </Grid>
        
        {/* <Grid item lg={4} md={8} sm={6} xs={12}>
          <Widget
            title="App Performance"
            upperTitle
            className={classes.card}
            bodyClass={classes.fullHeightBody}
          >
            <div className={classes.performanceLegendWrapper}>
              <div className={classes.legendElement}>
                <Dot color="warning" />
                <Typography
                  color="textSecondary"
                  className={classes.legendElementText}
                >
                  Integration
                </Typography>
              </div>
              <div className={classes.legendElement}>
                <Dot color="primary" />
                <Typography
                  color="textSecondary"
                  className={classes.legendElementText}
                >
                  SDK
                </Typography>
              </div>
            </div>
            <div className={classes.progressSection}>
              <Typography
                size="md"
                color="textSecondary"
                className={classes.progressSectionTitle}
              >
                Integration
              </Typography>
              <LinearProgress
                variant="determinate"
                value={30}
                classes={{ barColorPrimary: classes.progressBar }}
                className={classes.progress}
              />
            </div>
            <div>
              <Typography
                size="md"
                color="textSecondary"
                className={classes.progressSectionTitle}
              >
                SDK
              </Typography>
              <LinearProgress
                variant="determinate"
                value={55}
                classes={{ barColorPrimary: classes.progressBar }}
                className={classes.progress}
              />
            </div>
          </Widget>
        </Grid>
        
        <Grid item lg={4} md={4} sm={6} xs={12}>
          <Widget title="Revenue Breakdown" upperTitle className={classes.card}>
            <Grid container spacing={16}>
              <Grid item xs={6}>
                <ResponsiveContainer width="100%" height={144}>
                  <PieChart
                    margin={{ left: theme.spacing.unit * 2 }}
                  >
                    <Pie
                      data={PieChartData}
                      innerRadius={45}
                      outerRadius={60}
                      dataKey="value"
                    >
                      {PieChartData.map((entry, index) => (
                        <Cell
                          key={`cell-${index}`}
                          fill={theme.palette[entry.color].main}
                        />
                      ))}
                    </Pie>
                  </PieChart>
                  </ResponsiveContainer>
              </Grid>
              <Grid item xs={6}>
                <div className={classes.pieChartLegendWrapper}>
                  {PieChartData.map(({ name, value, color }, index) => (
                    <div key={color} className={classes.legendItemContainer}>
                      <Dot color={color} />
                      <Typography style={{ whiteSpace: 'nowrap' }}>&nbsp;{name}&nbsp;</Typography>
                      <Typography color="textSecondary">
                        &nbsp;{value}
                      </Typography>
                    </div>
                  ))}
                </div>
              </Grid>
            </Grid>
          </Widget>
        </Grid> */}
        <Grid item xs={12}>
        <Grid item xs={12} md={8} lg={12}>
          <Widget title={locale.DashBoard_attendance[language]} noBodyPadding upperTitle>
              <ResponsiveContainer width="90%" height={350}>
                  <LineChart
                      width={500}
                      height={300}
                      data={lineChartData}
                      margin={{
                          top: 5, right: 30, left: 20, bottom: 5,
                      }}
                  >
                      <CartesianGrid strokeDasharray="3 3" />
                      <XAxis dataKey="name" />
                      <YAxis />
                      <Tooltip />
                      <Legend />
                      <Line type="monotone" dataKey={locale.goToWork[language]} stroke={theme.palette.primary.main} />
                      <Line type="monotone" dataKey={locale.Lateness[language]} stroke={theme.palette.secondary.main} />
                  </LineChart>
              </ResponsiveContainer>
          </Widget>
        </Grid>
        </Grid>
       
      </Grid>
    </React.Fragment>
  );
};

const styles = theme => ({
  card: {
    minHeight: "100%",
    display: "flex",
    flexDirection: "column"
  },
  visitsNumberContainer: {
    display: "flex",
    alignItems: "center",
    flexGrow: 1,
    paddingBottom: theme.spacing.unit
  },
  progressSection: {
    marginBottom: theme.spacing.unit
  },
  progressTitle: {
    marginBottom: theme.spacing.unit * 2
  },
  progress: {
    marginBottom: theme.spacing.unit,
    backgroundColor: theme.palette.primary.main
  },
  pieChartLegendWrapper: {
    height: "100%",
    display: "flex",
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "flex-end",
    marginRight: theme.spacing.unit
  },
  legendItemContainer: {
    display: "flex",
    alignItems: "center",
    marginBottom: theme.spacing.unit
  },
  fullHeightBody: {
    display: "flex",
    flexGrow: 1,
    flexDirection: "column",
    justifyContent: "space-between"
  },
  tableWidget: {
    overflowX: "auto"
  },
  progressBar: {
    backgroundColor: theme.palette.warning.main
  },
  performanceLegendWrapper: {
    display: "flex",
    flexGrow: 1,
    alignItems: "center",
    marginBottom: theme.spacing.unit
  },
  legendElement: {
    display: "flex",
    alignItems: "center",
    marginRight: theme.spacing.unit * 2,
  },
  legendElementText: {
    marginLeft: theme.spacing.unit
  },
  serverOverviewElement: {
    display: "flex",
    alignItems: "center",
    maxWidth: "100%"
  },
  serverOverviewElementText: {
    minWidth: 145,
    paddingRight: theme.spacing.unit * 2
  },
  serverOverviewElementChartWrapper: {
    width: "100%"
  },
  mainChartBody: {
    overflowX: 'auto',
  },
  mainChartHeader: {
    width: "100%",
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    [theme.breakpoints.only("xs")]: {
      flexWrap: 'wrap',
    }
  },
  mainChartHeaderLabels: {
    display: "flex",
    alignItems: "center",
    [theme.breakpoints.only("xs")]: {
      order: 3,
      width: '100%',
      justifyContent: 'center',
      marginTop: theme.spacing.unit * 3,
      marginBottom: theme.spacing.unit * 2,
    }
  },
  mainChartHeaderLabel: {
    display: "flex",
    alignItems: "center",
    marginLeft: theme.spacing.unit * 3,
  },
  mainChartSelectRoot: {
    borderColor: theme.palette.text.hint + '80 !important',
  },
  mainChartSelect: {
    padding: 10,
    paddingRight: 25
  },
  mainChartLegentElement: {
    fontSize: '18px !important',
    marginLeft: theme.spacing.unit,
  }
});

export default withStyles(styles, { withTheme: true })(AdminMain);