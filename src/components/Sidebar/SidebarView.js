import React from 'react';
import {
  Drawer,
  IconButton,
  List,
  withStyles } from "@material-ui/core";
import {
  FilterNone as UIElementsIcon,
  WorkOutline as WorkAttitudeIcon,
  ViewList as ListIcon,
  Settings as SettingIcon,
  EventAvailable as HolidayIcon,
  AccessTime as WorktimeIcon,
  LocalHotel as BreaktimeIcon,
  Person as AdminIcon,
  Dashboard as DashboardIcon
} from "@material-ui/icons";
import classNames from 'classnames';
import Clock from 'react-live-clock';
import SidebarLink from './components/SidebarLink/SidebarLinkContainer';
import locale from 'locale';
import Time from "components/Header/Time/Time";
import { Button } from 'react-bootstrap';
import CommuteCheckContainer from 'containers/commute/CommuteCheckContainer';;

/**
* @author KJS
* @date 2019. 5. 14.
* @brief 권한에 따른 Menu
*/

const SidebarView = ({ starttime, endtime, language, auth, classes, theme, toggleSidebar, isSidebarOpened, isPermanent, location, ...props }) => {

  const userNav = [
    { id: 0, type: 'divider' },
    { id: 1, type: 'title', label: 'Commute' },
    { 
      id: 2, 
      label: locale.commuteList[language],
      link: '/app/commute',
      icon: <ListIcon />
    },
    { id: 3, type: 'divider' },
    { id: 4, type: 'title', label: 'Attendance' },
    { 
      id: 5, 
      label: locale.workAttitudeManage[language],
      link: '/app/workAttitude',
      icon: <UIElementsIcon />,
      children: [
        { type: 'sub', label: locale.workAttitudeList[language], link: '/app/workAttitude/list', icon: <ListIcon /> },
        { type: 'sub', label: locale.workAttitudeApply[language], link: '/app/workAttitude/new', icon: <WorkAttitudeIcon /> },
      ]
    },  
  ];
  
  const adminNav = [
    //{ id: 0, type: 'divider' },
    { id: 1, type: 'title', label: 'Commute' },
    { 
      id: 2, 
      label: locale.commuteList[language],
      link: '/app/commute/list',
      icon: <ListIcon />
    },
    { id: 3, type: 'divider' },
    { id: 4, type: 'title', label: "Attendance" },
    { 
      id: 5, 
      label: locale.workAttitudeManage[language],
      link: '/app/workAttitude/list',
      icon: <ListIcon />
    },    
  ]
  
  const activityNav = [
    { id: 6, type: 'divider' },
    { id: 7, type: 'title', label: 'Activity' },
    { id: 8, label: locale.activeRecord[language], link: '/app/record', icon: <ListIcon /> },
  ];
  
  const serviceNav = [
    { id: 9, type: 'divider' },
    { id: 10, type: 'title', label: 'Service' },
    { 
      id: 11, 
      label: locale.serviceSetting[language],
      link: '/app/service',
      icon: <SettingIcon />,
      children: [
        { type: 'sub', label: locale.workTimeSetting[language], link: '/app/service/worktime', icon: <WorktimeIcon /> },
        { type: 'sub', label: locale.breakTimeSetting[language], link: '/app/service/breaktime', icon: <BreaktimeIcon /> },
        { type: 'sub', label: locale.holidaySetting[language], link: '/app/service/holiday', icon: <HolidayIcon /> },
      ]
    },
  ];
  const dashboardNav = [
    { id: 12, type: 'divider' },
    { id: 13, type: 'title', label: 'DashBoard' },
    { 
      id: 14, 
      label:locale.DashBoard[language],
      link: '/app/admin/main',
      icon: <DashboardIcon />
    },
  ];


  const date = new Date();

  const userCommuteTime = (
    <div style={{margin:'16px 16px 16px 16px'}}>
        <Clock format={'YYYY.MM.DD'} /> 
        <div style={{marginBottom: '16px', textAlign: 'center', fontSize: '-webkit-xxx-large'}}>
          <Time />
        </div>
        <div style={{paddingBottom:8, textAlign:'-webkit-center'}}>
          <span style={{paddingRight:8}}>
            {locale.goToWork[language]}: <strong>{starttime ? starttime.substring(0, 5) : '--:--'}</strong>
          </span>
          <span style={{paddingLeft:8}}>
            {locale.getOffWork[language]}: <strong>{endtime ? endtime.substring(0, 5) : '--:--'}</strong>
          </span>
        </div>
        <CommuteCheckContainer />
      </div>
  );

  const admin = (
      <div style={{margin:'16px 16px 16px 16px', textAlign: 'center'}}>
        <AdminIcon style={{fontSize:'128'}} />
        <div style={{textAlign:'-webkit-center'}}>
        <span>{locale.administrator[language]}</span>
        </div>
      </div>
  );

  return (
    <Drawer
      variant={isPermanent ? 'permanent' : 'temporary'}
      className={classNames(classes.drawer, {
        [classes.drawerOpen]: isSidebarOpened,
        [classes.drawerClose]: !isSidebarOpened,
      })}
      classes={{
        paper: classNames({
          [classes.drawerOpen]: isSidebarOpened,
          [classes.drawerClose]: !isSidebarOpened,
        }),
      }}
      open={isSidebarOpened}
    >
      <div className={classes.toolbar} />
      
      {/* 출퇴근 버튼 들어갈 레이아웃 */}
      {
        auth === "ROLE_USER" && userCommuteTime
      }

      <List>
        {
          auth === "ROLE_USER" &&
          userNav.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)
        }
        {
          auth === "ROLE_ADMIN" && 
          adminNav.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)
        }
        {
          activityNav.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)
        }
        {
          auth === "ROLE_ADMIN" &&
          serviceNav.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)
        }
        {
          auth === "ROLE_ADMIN" &&
          dashboardNav.map(link => <SidebarLink key={link.id} location={location} isSidebarOpened={isSidebarOpened} {...link} />)
        }
      </List>
    </Drawer>
  );
}

// 한국 240 영어 280
const drawerWidth = 280;

const styles = theme => ({
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap'
  },
  drawerOpen: {
    width: drawerWidth,
    backgroundColor: '#fafafa',
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),









  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 40,
    [theme.breakpoints.down("sm")]: {
      width: drawerWidth,
    }
  },
  toolbar: {
    ...theme.mixins.toolbar,
    backgroundColor: '#fafafa',
    [theme.breakpoints.down("sm")]: {
      display: 'none',
    }
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  sidebarList: {
    marginTop: theme.spacing.unit * 6,
  },
  mobileBackButton: {
    marginTop: theme.spacing.unit * .5,
    marginLeft: theme.spacing.unit * 3,
    [theme.breakpoints.only("sm")]: {
      marginTop: theme.spacing.unit * .625,
    },
    [theme.breakpoints.up("md")]: {
      display: 'none',
    }
  }
});

export default withStyles(styles, { withTheme: true })(SidebarView);
