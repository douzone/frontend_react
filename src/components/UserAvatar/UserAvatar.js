import React from "react";
import { withStyles } from "@material-ui/core";
import apply from './img/apply.png';
import del from './img/delete.png';
import modify from './img/modify.png';
import korea from './img/korea.png';
import usa from './img/usa.png';

const UserAvatar = ({ classes, theme, color = 'primary', ...props }) => {
  var img;
  if(props.name.includes("수정")) {
    img = modify;
  } else if(props.name.includes("신청")) {
    img = apply;
  } else if(props.name.includes("삭제")) {
    img = del;
  } else if(props.name.includes("ko")) {
    img = korea;
  } else if(props.name.includes("en")) {
    img = usa;
  }

  return (
    <div className={classes.avatar} style={{ backgroundImage: "url(" + img + ")"}} />
  );
};

const styles = () => ({
  avatar: {
    width: 30,
    height: 30,
    display: "flex",
    alignItems: "center",
    justifyContent: "center"
  },
  text: {
    color: 'white',
  }
});

export default withStyles(styles, { withTheme: true })(UserAvatar);
