import React, {Component} from 'react';
import {Grid, Table, TableRow, TableHead, TableBody, TableCell} from '@material-ui/core';
import {Badge} from 'react-bootstrap';
import {Button} from 'antd';
import locale from 'locale';
import {Typography} from "components/Wrappers";
import {withStyles} from "@material-ui/core";
import ReactHTMLTableToExcel from 'react-html-table-to-excel';
import "../../list/TableList/TableList.css";


class HoliDayList extends Component {

    handleShowModal = (table) => {
        //console.log(table);
        const {handleChangeInput} = this.props;

        handleChangeInput({name: 'no', value: table.no});
        handleChangeInput({name: 'modal', value: true});
    }

    render() {
        const {tables, language, classes} = this.props;

        const HoliDayList = tables.map(
            (table, index) => {
                let {day, description, descriptionEn, use, updateTime} = table.toJS();
                let badge = '';

                if (use === true) {
                    badge = 'success';
                } else {
                    badge = 'danger';
                }

                if (language === "en") {
                    description = descriptionEn;
                } else {
                    use === true ? use = '사용' : use = '사용안함';
                }

                return (
                    <TableRow className={classes.tableBody} hover key={index} onClick={event => this.handleShowModal(table.toJS())}>
                        <TableCell>{day}</TableCell>
                        <TableCell>{description}</TableCell>
                        <TableCell>{updateTime}</TableCell>
                        <TableCell><Badge variant={badge}>{String(use)}</Badge></TableCell>
                    </TableRow>
                )
            }
        );

        return (
            <React.Fragment>
                <Grid item xs={12} style={{marginTop: '8px'}}>
                    <div className={classes.tableHeader}>
                        <Typography className={classes.subTitle} variant="h4" size="sm">
                            <strong>{locale.holidayList[language]}</strong>
                        </Typography>
                        <Button type="link" shape="round" icon="download" size="large">
                            <ReactHTMLTableToExcel
                                id="test-table-xls-button"
                                className="css"
                                table="workTimeTable"
                                filename="workTimeTableXLS"
                                sheet="workTimeTableXLS"
                                buttonText="Download"
                            />
                        </Button>
                    </div>

                    <Table id="holidayTable" className="mb-0">
                        <TableHead>
                            <TableRow className={classes.tableHead}>
                                <TableCell><b>{locale.holidayDate[language]}</b></TableCell>
                                <TableCell><b>{locale.holidayExplanation[language]}</b></TableCell>
                                <TableCell><b>{locale.updateTime[language]}</b></TableCell>
                                <TableCell><b>{locale.use[language]}</b></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {HoliDayList}
                        </TableBody>
                    </Table>
                </Grid>
            </React.Fragment>
        );
    }
}

const styles = theme => ({
    tableHead: {
        backgroundColor: '#fafafa',
        borderTop: '2px solid rgba(224, 224, 224, .5)',
        height: theme.spacing.unit * 6,
        "& th" : {
            borderBottom: '2px solid rgba(224, 224, 224, .5)',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            width: '10%'
        },
        "& th:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        },
        "& th:nth-child(2)" : {
            width: '70%',
        },
        "& p" : {
            marginBottom: '0px'
        }
    },
    tableBody: {
        padding: '0px 0px 0px 0px',
        height: theme.spacing.unit * 5,
        "& td" : {
            textAlign: 'center',
            padding: '0px 0px 0px 0px',
            borderLeft: '1px solid rgba(224, 224, 224, .5)',
        },
        "& td:first-of-type" : {
            paddingLeft: theme.spacing.unit * 2,
            borderLeft: 'none'
        },
        "& td:nth-child(2)" : {
            textAlign: 'left',
            paddingLeft: theme.spacing.unit * 3
        }
    },
    tableHeader: {
        padding: '8px',
        display: 'flex',
        justifyContent: 'space-between',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    },
    subTitle: {
        marginBlockStart: 'auto',
    }
});

export default withStyles(styles)(HoliDayList);