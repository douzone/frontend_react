import React, { Component } from 'react';
import locale from 'locale';
import { withStyles } from "@material-ui/core";
import { Typography } from "components/Wrappers";
import { Divider } from 'antd';
import {
    Home as HomeIcon
} from "@material-ui/icons";

class Title extends Component {
    render() {
        const { language, classes } = this.props;
        let title, path, descryption = null;
        
        if(window.location.href.includes('worktime')) {
            title = locale.workTimeSetting[language];
            path = locale.serviceSetting[language] + " > " + locale.workTimeSetting[language];
            descryption = locale.worktimeDescryption[language];
        } else if(window.location.href.includes('breaktime')) {
            path = locale.serviceSetting[language] + " > " + locale.breakTimeSetting[language];
            title = locale.breakTimeSetting[language];
            descryption = locale.breaktimeDescryption[language];
        } else {
            path = locale.serviceSetting[language] + " > " + locale.holidaySetting[language];
            title = locale.holidaySetting[language];
            descryption = locale.holidayDescryption[language];
        }

        
        return(
            <div>
                <div className={classes.pageTitleContainer}>
                    <Typography variant="h2" size="sm">
                        <strong>{title}</strong>
                    </Typography>
                    <Typography className={classes.dirTypo} variant="h5" size="sm">
                        <HomeIcon />
                            <span> > </span>
                            {path}
                    </Typography>
                </div>
                <div style={{marginBottom: 24, border: '1px solid #e8e8e8'}}>
                    {descryption.map((desc, i) => {
                        return (
                        <div className={classes.typo} variant="h6" size="sm">
                            {"! " + desc}
                        </div>)
                    })}
                </div>
            </div>
        )
    }
}

const styles = theme => ({
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        backgroundColor: '#fafafa',
        padding: '8px'
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        backgroundColor: '#efeaea',
        padding: '10px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(Title);