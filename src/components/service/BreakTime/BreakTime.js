import React, { Component } from 'react';
import { TimePicker, Button as Btn, Menu, Icon, Dropdown, message, Input } from 'antd';
import {Button} from 'react-bootstrap';
import moment from 'moment';
import locale from 'locale';
import { withStyles } from "@material-ui/core";
import {Table, TableRow, TableHead, TableCell} from '@material-ui/core';
import { Typography } from "components/Wrappers";

class BreakTime extends Component {

    startTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'newStart';
        onChangeInput({name, value});
    }

    endTimeHandleChange = (time, value) => {
        const { onChangeInput } = this.props;
        const name = 'newEnd';
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'newUse';
        const value = e.key;
        onChangeInput({name, value});
    }

    textAreaChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }
    
    submitCheck = () => {
        const { newStart, newEnd, newDescription, onSubmit } = this.props;
        const format = 'HH:mm';
        const between = moment.duration(moment(newStart, format).diff(moment(newEnd, format))).asMinutes();
        message.config( {top: 100, duration: 2});
        if(between >= 0){
            message.info('시작시간과 끝시간의 간격이 없습니다.');
            return;
        }

        if(newDescription <= 0){
            message.info('휴게설명에 내용이 없습니다.');
            return;
        }

        onSubmit();

    }

    render() {
        const { startTimeHandleChange, endTimeHandleChange, handleChange, submitCheck, textAreaChange } = this;
        const { TextArea } = Input;
        const { start, end, use, description, language, classes } = this.props;
        const format = 'HH:mm';

        const menu = (
            <Menu onClick={handleChange}>
              <Menu.Item key="true">true</Menu.Item>
              <Menu.Item key="false">false</Menu.Item>
            </Menu>
          );
        
        return (
            <div style={{marginBottom: 24}}>
                <p className={classes.tableHeader}>
                    <Typography variant="h4" size="sm">
                        <strong>{locale.breakTimeregister[language]}</strong>
                    </Typography>
                </p>
                <div className={classes.borderline}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.startTime[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <TimePicker
                                        format={format}
                                        value={moment(start, format)}
                                        allowClear={false}
                                        onChange={startTimeHandleChange}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.endTime[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <TimePicker
                                        format={format}
                                        value={moment(end, format)}
                                        allowClear={false}
                                        onChange={endTimeHandleChange}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.use[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu} >
                                        <Btn>
                                            {use} <Icon type="down" />
                                        </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.breakTimeExplanation[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <TextArea name='newDescription' id="newDesBreakTime" rows={4} onChange={textAreaChange} value={description} />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                </div>
                <div style={{textAlign:'-webkit-right', marginTop:8}}>
                    <Button style={{width:'80px'}} id="breakTimeNewBtn" onClick={submitCheck} theme="outline">
                        {locale.Submit[language]}
                    </Button>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    borderline: {
        borderTop: '2px solid rgba(224, 224, 224, .5)'
    },
    tableHead: {
        "& tr" : {
            height: '48px'
        },
        "& th:first-of-type" : {
            width:'20%', 
            backgroundColor: '#fafafa',
            padding: '0px 0px 0px 16px',
        },
        "& tr:last-of-type": {
            "& th": {
                borderBottom: '2px solid rgba(224, 224, 224, .5)'
            }
        }
    },
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        padding: '8px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(BreakTime);
