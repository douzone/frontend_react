import React, { Component } from 'react';
import { Button as Btn, Menu, Icon, Dropdown, message, Input } from 'antd';
import {Button} from 'react-bootstrap';
import locale from 'locale';

import { DatePickerInput } from 'rc-datepicker';
import moment from 'moment';
import 'rc-datepicker/lib/style.css';
import 'moment/locale/en-ca';
import 'moment/locale/ko';

import { withStyles } from "@material-ui/core";
import {Table, TableRow, TableHead, TableCell} from '@material-ui/core';
import { Typography } from "components/Wrappers";

class HoliDay extends Component {

    constructor(props) {
        super(props);
        this.newDayChange = this.newDayChange.bind(this);
    }

    newDayChange(date) {
        const { onChangeInput } = this.props;
        let value = moment(date).format('YYYY-MM-DD');
        let name = "newDay";
        onChangeInput({name, value});
    }

    UseChange = (e) => {
        const { onChangeInput } = this.props;
        const name = 'newUse';
        const value = e.key;
        onChangeInput({name, value});
    }

    handleChange = (e) => {
        const { onChangeInput } = this.props;
        const { value, name } = e.target;
        onChangeInput({name, value});
    }
    
    submitCheck = () => {
        const { day, description, onSubmit, language } = this.props;
        message.config( {top: 100, duration: 2});
        if(day <= 0){
            message.info(locale.holidayDateValidation[language]);
            return;
        }

        if(description <= 0){
            message.info(locale.holidayTextValidation[language]);
            return;
        }

        onSubmit();
    }

    render() {
        const { UseChange, handleChange, submitCheck, newDayChange } = this;
        const { TextArea } = Input;
        const { day, use, description, language, classes } = this.props;

        const menu = (
            <Menu onClick={UseChange}>
              <Menu.Item key="true">true</Menu.Item>
              <Menu.Item key="false">false</Menu.Item>
            </Menu>
          );
        
        return (
            <div style={{marginBottom: 24}}>
                <p className={classes.tableHeader}>
                    <Typography variant="h4" size="sm">
                        <strong>{locale.holidayregister[language]}</strong>
                    </Typography>
                </p>    
                <div className={classes.borderline}>
                    <Table className="mb-0">
                        <TableHead className={classes.tableHead}>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.holidayDate[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <DatePickerInput
                                        title={locale.holidayDate[language]}
                                        selected={day}
                                        value={day}
                                        onChange={newDayChange}
                                        displayFormat='YYYY-MM-DD'
                                        returnFormat='YYYY-MM-DD'
                                        //defaultValue={this.state.yesterday}
                                        locale={language}
                                        readOnly={true}
                                        style={{width:'20%'}}
                                    />
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.use[language]}</b>
                                </TableCell>
                                <TableCell>
                                    <Dropdown overlay={menu} >
                                        <Btn>
                                            {use} <Icon type="down" />
                                        </Btn>
                                    </Dropdown>
                                </TableCell>
                            </TableRow>
                            <TableRow>
                                <TableCell>
                                    <b>{locale.holidayExplanation[language]}</b>
                                </TableCell>
                                <TableCell>
                                <TextArea name='newDescription'  id="newDesHoliDay" rows={4} onChange={handleChange} value={description} />
                                </TableCell>
                            </TableRow>
                        </TableHead>
                    </Table>
                </div>
                <div style={{textAlign:'-webkit-right', marginTop:8}}>
                    <Button style={{width:'80px'}} id="holiDayNewBtn" onClick={submitCheck} theme="outline">
                        {locale.Submit[language]}
                    </Button>
                </div>
            </div>
        );
    }
}

const styles = theme => ({
    borderline: {
        borderTop: '2px solid rgba(224, 224, 224, .5)'
    },
    tableHead: {
        "& tr" : {
            height: '48px'
        },
        "& th:first-of-type" : {
            width:'20%', 
            backgroundColor: '#fafafa',
            padding: '0px 0px 0px 16px',
        },
        "& tr:last-of-type": {
            "& th": {
                borderBottom: '2px solid rgba(224, 224, 224, .5)'
            }
        }
    },
    pageTitleContainer: {
        display: "flex",
        justifyContent: "space-between",
        marginBottom: theme.spacing.unit * 1,
        marginTop: theme.spacing.unit * 5
    },
    typo: {
        color: theme.palette.text.hint,
    },
    dirTypo: {
        marginBlockStart:'auto'
    },
    button: {
        boxShadow: theme.customShadows.widget,
        textTransform: 'none',
        '&:active' : {
          boxShadow: theme.customShadows.widgetWide,
        },
    },
    tableHeader: {
        padding: '8px',
        marginBottom: 0
    },
    settingGrid: {
        backgroundColor: 'antiquewhite'
    }
});

export default withStyles(styles)(HoliDay);
