import React, {Component} from 'react';
import {Button} from 'react-bootstrap';
import locale from 'locale';


class Check extends Component {

    constructor(props, context) {
        super(props, context);
    
        this.state = {
            seconds:new Date().getSeconds(),
            minutes:new Date().getMinutes(),
            hours:new Date().getHours()
        };
    }

    render() {
        const {language, goto, gooff, onGoTo, onGoOff, endTime, isGoOff, preGoTo, preGoOff, preHoliDay, onChangeInput, time, isNight, onHandleState} = this.props;

        // //console.log("time : " + time);
        // //console.log("isGoOff : " + isGoOff);
        // //console.log("isNight : " + isNight);
        // //console.log("goto : " + goto);
        // //console.log("gooff : " + gooff);
        // //console.log("preGoTo : " + preGoTo);
        // //console.log("preGoOff : " + preGoOff);
        // //console.log("preHoliDay : " + preHoliDay);

        let text = '';
        let goOffStyle = {display: 'none'};
        let goToStyle = {display: 'none'};
        let goOffTextStyle = {display: 'block', marginTop: '10px', marginBottom: '15px'};

        // if(goto){ // 출근 - 퇴근버튼 활성화
        //     goOffStyle = {visibility: 'visible'}
        //     goToStyle = {visibility: 'hidden'}
        // }
        // else if(gooff){ // 퇴근 - 출근버튼 활성화
        //     goOffStyle = {visibility: 'hidden'}
        //     goToStyle = {visibility: 'visible'}
        // }
        let showtime;
        showtime = time.toLocaleString();
        //console.log(showtime);
        if(language === "en"){
            showtime = time.toLocaleString("en-US");
            //console.log(showtime);
        }

        if((goto && gooff && !isNight && preGoTo && preGoOff) || (!isNight && goto && gooff && preGoTo && !preGoOff)) { // 출근 후 퇴근 후 자정 전 - 둘 다 비활성
            text = locale.goOffApplySucces[language];
            //goOffTextStyle = {display: 'block', marginTop: '10px', marginBottom: '15px'};

            //console.log("자정 후 출근 버튼 활성화");
        }
        else if((isNight && !gooff && goto && preGoTo && !preGoOff) || (!isNight && !gooff && isGoOff && goto) || (preGoTo && !preGoOff && !goto)){ // 자정 + 노퇴근 or 퇴근시간 지나고 자정 전인데 노퇴근
            goOffStyle = {display: 'unset', width:'230px'}
            text = locale.goOffApplyPossibility[language];

            //console.log("퇴근 안찍을 경우");
        }
        else if(isNight && gooff){ // 퇴근 후 자정 - 출근버튼 활성화
            goToStyle = {display: 'unset', width:'230px'}
            text = locale.goToApplyPossibility[language];

            //console.log("퇴근 후 자정");

            onHandleState(preGoTo, preGoOff);
        }
        else if(!isGoOff && goto && !gooff){ // 출근 후 퇴근시간 전 - 둘 다 비활성화
            text = locale.goToSuccess[language];

            //console.log("출근 후 퇴근시간 전");
        }
        else if((isGoOff && !goto && !preGoTo && !gooff) || !isGoOff || (preGoTo && preGoOff) || (goto && preGoTo)){ // 출근 안하고 퇴근시간 전후 - 출근버튼 활성화 (출근 안하면 출근버튼 활성화)
            goToStyle = {display: 'unset', width:'230px'}
            text = locale.goToApplyPossibility[language];

            //console.log("출근 안하고 퇴근시간 전후");
        }
        const commuteText = (
            <span id="commuteText" style={goOffTextStyle}>
                <strong>{text}</strong>
            </span>
        );
        
        return (
            
            <div style={{textAlign: 'center'}}>
                {commuteText}

                <Button id="btnGoTo" style={goToStyle} variant="primary" onClick={() => {
                    if(preGoTo){
                        onGoTo(preGoTo);
                    }
                    else{
                        onChangeInput('preGoTo', true);
                        onGoTo(preGoTo);
                    }
                    }}>
                    {locale.goToWork[language]}
                </Button>

                
                <Button id="btnGoOff" style={goOffStyle} variant="primary" onClick={() => {
                    if((preGoTo && preGoOff) || (goto && preGoTo)){
                        onGoOff(preGoTo);
                    }
                    else{
                        onChangeInput('preGoOff', true);
                        onGoOff(preGoOff);
                    }
                    }}>
                    {locale.getOffWork[language]}
                </Button>
            </div>
        );
    }
}



export default Check;